<?php
	/**
	 * contains the QDataGrid class
	 *
	 * @package Controls
	 * @filesource
	 */


	/**
	 * QDataGrid can help generate tables automatically with pagination. It can also be used to
	 * render data directly from database by using a 'DataSource'. The code-generated search pages you get for
	 * every table in your database are all QDataGrids
	 *
	 * @package Controls
	 */
	class QDataGrid extends QDataGridBase  {
	         /** @var int $intCellSpacing Set the space between the cells */
		protected $intCellSpacing = 0;
                 /** @type int $intCellPadding Set the space between the cell wall and the cell content */
		protected $intCellPadding = 0;

		///////////////////////////
		// DataGrid Preferences
		///////////////////////////

		// Feel free to specify global display preferences/defaults for all QDataGrid controls

		/**
		 * QDataGrid::__construct()
		 *
		 * @param mixed  $objParentObject The Datagrid's parent
		 * @param string $strControlId    Control ID
		 *
		 * @throws QCallerException
		 * @return \QDataGrid
		 */
		public function __construct($objParentObject, $strControlId = null) {
			try {
				parent::__construct($objParentObject, $strControlId);
			} catch (QCallerException  $objExc) {
				$objExc->IncrementOffset();
				throw $objExc;
			}

			// For example... let's default the CssClass to datagrid
			$this->strCssClass = 'table table-bordered table-striped dataTable';
		}

        public function MetaDataBinderCustom($ObjectClass, QQCondition $objCondition = null, $objOptionalClauses = null) {
            // Setup input parameters to default values if none passed in
            if (!$objCondition) $objCondition = QQ::All();
            $objClauses = ($objOptionalClauses) ? $objOptionalClauses : array();

            // We need to first set the TotalItemCount, which will affect the calcuation of LimitClause below
            if ($this->Paginator) $this->TotalItemCount = $ObjectClass::QueryCount($objCondition, $objClauses);

            // If a column is selected to be sorted, and if that column has a OrderByClause set on it, then let's add
            // the OrderByClause to the $objClauses array
            if ($objClause = $this->OrderByClause) array_push($objClauses, $objClause);

            // Add the LimitClause information, as well
            if ($objClause = $this->LimitClause) array_push($objClauses, $objClause);

            // Set the DataSource to be a Query result from MasterOpsi, given the clauses above
            $this->DataSource = $ObjectClass::QueryArray($objCondition, $objClauses);
        }

        public function searchText($strFormId, $strControlId, $strParameter) {
            $this->strSearch = $this->txtSearch->Text;
            $this->Refresh();
        }

        public function __get($strName) {
            switch ($strName) {
                case 'SearchText' :
                    return $this->strSearch;
                //return ($this->blnShowSearch AND $this->txtSearch) ? $this->txtSearch->Text : null;

                case 'CurrentNumber' :
                    return (($this->PageNumber - 1)*$this->ItemsPerPage) + $this->intCurrentRowIndex + 1;

                case 'TotalData' :
                    return @count($this->objDataSource);
                    // return 1000;
                default:
                    try {
                        return parent::__get($strName);
                    } catch (QCallerException $objExc) {
                        $objExc->IncrementOffset();
                        throw $objExc;
                    }
            }
        }
	}
?>