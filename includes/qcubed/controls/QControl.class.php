<?php
	/**
	 * Contains the QControl Class - one of the most important classes in the framework
	 * @package Controls
	 * @filesource
	 */
	/**
	 * QControl is the user overridable Base-Class for all Controls.
	 *
	 * This class is intended to be modified. If you need to modify the class (looks or behavior), then place any
	 * custom modifications to QControl in the file.
	 *
	 * <b>Please note</b>: All custom render methods should start with a RenderHelper call and end with a RenderOutput call.
	 * Also, read the class file to learn about the wrappers (the HTML elements with the '_ctl' at the end of their
	 * 'id' attribute) and how to disable them. 
	 *
	 * @package Controls
	 */
	abstract class QControl extends QControlBase {

		/**
		 * By default, wrappers are turned on for all controls. Wrappers create an extra <div> tag around
		 * QControls, and were historically used to help manipulate QControls, and to group a name and error
		 * message with a control. However, they can at times get in the way. Now that we are using jQuery to
		 * manipulate controls, they are not needed as much, but they are still useful for grouping names and
		 * error messages with a control. If you want to turn global wrappers off and rather set a wrapper for
		 * individual controls, uncomment the line below.
		 */
		//protected $blnUseWrapper = false;

        public function Hide()
        {
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_grp\').hide()');
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_lbl\').hide()');
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_ctl\').hide()');
        }

        public function UnHide()
        {
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_grp\').show()');
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_lbl\').show()');
            QApplication::ExecuteJavaScript('$(\'#'.$this->strControlId.'_ctl\').show()');
        }

        public function RenderWithBootstrap($lblWidth=4,$inputWidth=8,$blnDisplayOutput = true)
        {
//            if (($this->blnRendered) || ($this->blnRendering)) return;
//            if (!$this->blnVisible) return;
            $strToOutput = '<div class="form-group" id="'.$this->strControlId.'_grp">';
            $strToOutput .= $this->RenderWithBootstrapInGroup($lblWidth,$inputWidth,false);
            $strToOutput .= '</div>';
            if($blnDisplayOutput) print $strToOutput;
            else return $strToOutput;
        }

        public function RenderWithBootstrapInGroup($lblWidth=4,$inputWidth=8,$blnDisplayOutput = true)
        {
            $strClass = get_class($this);
            $valign = ($strClass == 'QDataGrid') ? 'valign="top"' : null;
            $strToOutput = null;
            $strToOutput.= '<label ';
            $strToOutput.= 'id="'.$this->strControlId.'_lbl" ';
            $strToOutput .= 'class="col-sm-'.$lblWidth;
            if($this->blnRequired) $strToOutput .= ' requiredCell';
            if(!$this->blnVisible) $strToOutput .= ' hidden';
            $strToOutput.= '">'.$this->strName.'</label>';
            $strToOutput.= '<div class="col-sm-'.$inputWidth;
            if(!$this->blnVisible) $strToOutput .= ' hidden';
            $strToOutput.= '">';
            $strToOutput .= sprintf('%s %s',$this->Render(false),$this->txtAddOnText);
            $strToOutput.= '</div>';
            if($blnDisplayOutput) print $strToOutput;
            else return $strToOutput;
        }
	}

?>
