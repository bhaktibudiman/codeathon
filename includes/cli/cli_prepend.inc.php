<?php 
	// First, let's make sure that path_to_prepend.inc.php is exist
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';
    $counter = 0;
    while($counter < 10){
        if($found=is_dir($dir.$search)){
            $prePath=realpath($dir.$search);
            break;}
        $counter++;$search = '/..'.$search;
    }

	$strPathToPrepend = $prePath;

    $_SERVER['SHELL'] = true;

	if (!is_dir($strPathToPrepend))
		exit("The Path to prepand.inc.php is Wrong");

	// If it exists, require() it -- otherwise, report the error
	if (file_exists($strPathToPrepend . '/prepend.inc.php'))
		require($strPathToPrepend . '/prepend.inc.php');
	else
		exit("The prepend.inc.php file was not found at $strPathToPrepend.\r\nPlease be sre to specify the correct absolute path to prepend.inc.php in the ./path_to_prepend.txt file!\r\n");

	// Finally, turn off output buffering
	ob_end_flush();
?>