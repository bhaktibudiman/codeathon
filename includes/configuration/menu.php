<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');
require_once($prePath.'/pdo_db.php');

    $tipeUser = $_SESSION[__USER_TIPE__];

    $list = DynamicMenu::LoadArrayByRoleUserAsRoleMenu($tipeUser);

    echo create_list_treeview($list,$tipeUser);
    $class = 'treeview';
    $html = '';
    $db = new core_db();
    $conn = $db->connect();
    $query = "select * from dynamic_menu inner join role_menu_assn on role_menu_assn.menu_id=dynamic_menu.id
    where dynamic_menu.is_parent=false and parent_id is null and dynamic_menu.url != 'home.php'
    and role_menu_assn.role_id=".$tipeUser;
    $result = pg_query($conn, $query);
    while ($row = pg_fetch_array($result, null, PGSQL_ASSOC)) {
        $html .= "<li class='". $class ."'>\n";
        $html .= '    <a href="'.QApplication::UrlCreator($row['url']). '">
                            <i class="'.$row['icon'].'"></i>
                            <span> '.$row['title'].'</span>
                          </a>
                      </li>';
    }
    echo $html;

    function create_list($arr)
    {
        $class = 'treeview';
        $html = "\n<ul class='sidebar-menu'>\n";
        $html .="\n<li class='header'>MAIN MENU</li>\n";

        foreach ($arr as $v){
            $href = QApplication::UrlCreator($v->Url);

            $html .= "<li class='". $class ."'>\n";
            $html .= '    <a href="'. $href. '">
                            <i class="'.$v->Icon.'"></i>
                            <span>'.$v->Title.'</span>
                          </a>
                      </li>';
        }

        return $html;
    }

    function create_list_treeview($arr,$tipeUser)
    {
        $class = 'treeview';
        $html = "\n<ul class='sidebar-menu'>\n";
        $html .="\n<li class='header'>MAIN MENU</li>\n";

        $html .= "<li class='". $class ."'>\n";
        $html .= '    <a href="'.QApplication::UrlCreator('home.php'). '">
                            <i class="fa fa-home"></i>
                            <span> Home</span>
                          </a>
                      </li>';

        foreach ($arr as $v){
            $href = QApplication::UrlCreator($v->Url);
            if($v->IsParent == TRUE){
                $menuID = $v->Id;
                $html .= "<li class='". $class ."'>\n";
                $html .= '    <a href="'. $href. '">
                                <i class="'.$v->Icon.'"></i>
                                <span>'.$v->Title.'</span> <i class="fa fa-angle-left pull-right"></i>
                              </a>
                              <ul style="display: none;" class="treeview-menu">';
                $objChildMenu = DynamicMenu::LoadByParentIdRoleIdArray($menuID,$tipeUser);
                foreach($objChildMenu as $child){
                    $hrefChild = QApplication::UrlCreator($child->Url);
                    $iconChild = $child->Icon;
                    $titleChild = $child->Title;
                    $html .= '<li class=""><a href="'.$hrefChild.'"><i class="'.$iconChild.'"></i> '.$titleChild.'</a></li>';
                }
                $html .= '   </ul>
                          </li>';
            }/*else{
                $html .= "<li class='". $class ."'>\n";
                $html .= '    <a href="'.QApplication::UrlCreator($v->Url). '">
                            <i class="'.$v->Icon.'"></i>
                            <span> '.$v->Title.'</span>
                          </a>
                      </li>';
            }*/
        }



        return $html;
    }
?>