<?php
    if (!defined('__PREPEND_INCLUDED__')) {
        define('__PREPEND_INCLUDED__', 1);

        if (!file_exists(dirname(__FILE__) . '/configuration.inc.php'))
            exit('error: configuration.inc.php missing from includes/configuration/ directory');
        require(dirname(__FILE__) . '/configuration.inc.php');

        if (realpath(__FILE__) != realpath(__CONFIGURATION__ . '/prepend.inc.php'))
            exit('error: update includes/configuration.inc.php with the correct settings');

        require(__QCUBED_CORE__ . '/framework/DisableMagicQuotes.inc.php');
        require(__QCUBED_CORE__ . '/qcubed.inc.php');

        // Register the autoloader
        spl_autoload_register(array('QApplication', 'Autoload'));

        // TODO: Define any custom global functions (if any) here...

        // TODO: Include any other include files (if any) here...
        require('System.class.php');

        ///////////////////////
        // Setup Error Handling
        ///////////////////////
        /*
         * Set Error/Exception Handling to the default
         * QCubed HandleError and HandlException functions
         * (Only in non CLI mode)
         *
         * Feel free to change, if needed, to your own
         * custom error handling script(s).
         */
        if (array_key_exists('SERVER_PROTOCOL', $_SERVER)) {
            set_error_handler('QcodoHandleError', error_reporting());
            set_exception_handler('QcodoHandleException');
            register_shutdown_function('QCubedShutdown');
        }


        ////////////////////////////////////////////////
        // Initialize the Application and DB Connections
        ////////////////////////////////////////////////
        QApplication::Initialize();
        QApplication::InitializeDatabaseConnections();

        // Check if we are going to override PHP's default session handler
        QApplication::SessionOverride();


        /////////////////////////////
        // Start Session Handler (if required)
        /////////////////////////////
        session_start();


        //////////////////////////////////////////////
        // Setup Internationalization and Localization (if applicable)
        // Note, this is where you would implement code to do Language Setting discovery, as well, for example:
        // * Checking against $_GET['language_code']
        // * checking against session (example provided below)
        // * Checking the URL
        // * etc.
        // TODO: options to do this are left to the developer
        //////////////////////////////////////////////
        if (isset($_SESSION)) {
            if (array_key_exists('country_code', $_SESSION))
                QApplication::$CountryCode = $_SESSION['country_code'];
            if (array_key_exists('language_code', $_SESSION))
                QApplication::$LanguageCode = $_SESSION['language_code'];
        }

        // Initialize I18n if QApplication::$LanguageCode is set
        if (QApplication::$LanguageCode)
            QI18n::Initialize();
        else {
            // QApplication::$CountryCode = 'us';
            // QApplication::$LanguageCode = 'en';
            // QI18n::Initialize();
        }
    }
?>