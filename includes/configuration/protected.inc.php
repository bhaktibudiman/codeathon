<?php
require_once("prepend.inc.php");
if(isset($_SESSION[__USER_LOGIN__])){
    $objUser = Users::Load($_SESSION[__USER_LOGIN__]);

    if($objUser->LockedSession == null){
        QApplication::Redirect(__SOURCE__ . '/login/form.php');
    }
}else{
    QApplication::Redirect(__SOURCE__ . '/login/form.php');
}
?>