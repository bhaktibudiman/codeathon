<?php
    define('__APPID__','apuppt');
    define('__USER_LOGIN__',__APPID__.'_LOGIN');
    define('__ROLE_CONFIGURATION__',__APPID__.'_CONFIGURATION');
    define('__GLOBAL_DELIMITER__',__APPID__.'_CONFIGURATION');
    define('__USER_TIPE__',__APPID__.'_ACTOR');
    define('__USER_ROLE__',__APPID__.'_USERROLE');
    define('__ROLE_NAME__',__APPID__.'_ROLENAME');
    define('__USERNAME_LOGIN__',__APPID__.'_USERNAME');
    define('__USER_BRANCH__',__APPID__.'_ACTOR_BRANCH');
    define('__DOWNLOAD_REPORT__',__APPID__.'_FILE');
    define('___REPORT_ID__',__APPID__.'_RID');
    define('__START_DATE__',__APPID__.'_STARTDATE');
    define('__END_DATE__',__APPID__.'_ENDDATE');
    define('__BRANCH_ID__',__APPID__.'_BRANCHID');
    define('__BRANCH_NAME__',__APPID__.'_BRANCHNAME');
    define('__CUSTOMER_NAME__',__APPID__.'_FIRSTNAME');
    define('__CUSTOMER_TYPE__',__APPID__.'_CUSTOMERTYPE');
    define('__CUSTOMER_ID__',__APPID__.'_CUSTOMERID');
    define('__PARAMERTER_DESC__',__APPID__.'_DESCRIPTION');
    define('__PARAM_TYPE__',__APPID__.'_PARAMTYPE');
    define('__CTR_FLAG__',__APPID__.'_CTRFLAG');
    define('__STR_FLAG__',__APPID__.'_STRFLAG');

    define('__CTR__',__APPID__.'_CTR');
    define('__STR__',__APPID__.'_STR');
    define('__BELUM__',__APPID__.'_BELUM');
    define('__KECUALI__',__APPID__.'_KECUALI');
    define('__LAPOR__',__APPID__.'_LAPOR');


    define('__HIERARCHY_TYPE__',__APPID__.'_HIERARCHYTYPE');
    define('__STAGING__',__APPID__.'_STAGING');

	define('SERVER_INSTANCE', 'dev');

	switch (SERVER_INSTANCE) {
		case 'dev':
            define('DB_CONNECTION_1', serialize(array(
                'adapter' => 'PostgreSql',
                'server' => '156.67.217.190',
                'port' => null,
                'database' => 'codeathon',
                'username' => 'postgres',
                'password' => 'postgres',
                'caching' => false,
                'profiling' => false)));
            break;
		case 'test':
		case 'stage':
		case 'prod':
			break;
	}
    define('ALLOW_REMOTE_ADMIN', true);

    include_once dirname(__FILE__).'/path_setting.php';

    define ('__DOCROOT__',$pathSettingDocRoot);
    define ('__SUBDIRECTORY__',$pathSettingSubDirectory);
    define ('__VIRTUAL_DIRECTORY__', '');


    define ('__INCLUDES__', __DOCROOT__ . __SUBDIRECTORY__ . '/includes');
    define('__CONFIGURATION__', __INCLUDES__ . '/configuration');
    define ('__EXTERNAL_LIBRARIES__', __INCLUDES__ . '/external_libraries');

    define ('__URL_REWRITE__', 'none');

    define ('__QCUBED__', __INCLUDES__ . '/qcubed');
    define ('__PLUGINS__', __QCUBED__ . '/plugins');

    define ('__CACHE__', __INCLUDES__ . '/tmp/cache');

    define ('__QCUBED_CORE__', __INCLUDES__ . '/qcubed/_core');

    define ('__MODEL__', __INCLUDES__ . '/model' );
    define ('__MODEL_GEN__', __MODEL__ . '/generated' );
    define ('__META_CONTROLS__', __INCLUDES__ . '/meta_controls' );
    define ('__META_CONTROLS_GEN__', __META_CONTROLS__ . '/generated' );

    define ('__FORM_DRAFTS__', __SUBDIRECTORY__ . '/drafts');
    define ('__PANEL_DRAFTS__', __SUBDIRECTORY__ . '/drafts/panels');
    define ('__FORMBASE_CLASSES__', __INCLUDES__ . '/formbase_classes_generated');
    define ('__FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__', 10);
    define ('__FORM_DRAFTS_PANEL_LIST_ITEMS_PER_PAGE__', 8);
    define ('__SOURCE__', __SUBDIRECTORY__ . '/src');

    define ('__JS_ASSETS__', __SUBDIRECTORY__ . '/assets/_core/js');
    define ('__CSS_ASSETS__', __SUBDIRECTORY__ . '/assets/_core/css');
    define ('__IMAGE_ASSETS__', __SUBDIRECTORY__ . '/assets/_core/images');
    define ('__PHP_ASSETS__', __SUBDIRECTORY__ . '/assets/_core/php');
    define ('__PLUGIN_ASSETS__', __SUBDIRECTORY__ . '/assets/plugins');

    define ('__APP_JS_ASSETS__', __SUBDIRECTORY__ . '/assets/js');
    define ('__APP_CSS_ASSETS__', __SUBDIRECTORY__ . '/assets/css');
    define ('__APP_IMAGE_ASSETS__', __SUBDIRECTORY__ . '/assets/images');
    define ('__APP_PHP_ASSETS__', __SUBDIRECTORY__ . '/assets/php');
    define ('__APP_FONT_ASSETS__', __SUBDIRECTORY__ . '/assets/fonts');
    define ('__APP_JQUERY_ASSETS__', __SUBDIRECTORY__ . '/assets/jquery');
    define ('__APP_IMAGE_UPLOAD__', __SUBDIRECTORY__ . '/assets/images/upload');

    define ('__JQUERY_BASE__', __APP_JQUERY_ASSETS__ . '/jQuery-2.1.4.min.js');
    define ('__JQUERY_EFFECTS__',   __APP_JQUERY_ASSETS__ . '/jquery-ui.min.js');

    define ('__QCUBED_JS_CORE__',  'qcubed.js');
    define ('__JQUERY_CSS__', 'jquery-ui-themes/ui-qcubed/jquery-ui.custom.css');

    define ('__DEVTOOLS__', __PHP_ASSETS__ . '/_devtools');
    define ('__EXAMPLES__', __PHP_ASSETS__ . '/examples');
    define ('__QI18N_PO_PATH__', __QCUBED__ . '/i18n');

    define ('MAX_DB_CONNECTION_INDEX', 9);

    define('__QAPPLICATION_ENCODING_TYPE__', 'UTF-8');

    if ((function_exists('date_default_timezone_set')) && (!ini_get('date.timezone')))
        date_default_timezone_set('Asia/Jakarta');

    define("CACHE_PROVIDER_CLASS", null);
    define ('CACHE_PROVIDER_OPTIONS' , serialize(
        array(
            array('host' => '127.0.0.1', 'port' => 11211, ),
        )
    ) );

    define('__FORM_STATE_HANDLER__', 'QSessionFormStateHandler');
    define('__FILE_FORM_STATE_HANDLER_PATH__', __INCLUDES__ . '/tmp');
    define('__DB_BACKED_FORM_STATE_HANDLER_DB_INDEX__', 1);
    define('__DB_BACKED_FORM_STATE_HANDLER_TABLE_NAME__', 'qc_formstate');

    define("DB_BACKED_SESSION_HANDLER_DB_INDEX", 0);
    define("DB_BACKED_SESSION_HANDLER_TABLE_NAME", "qc_session");
    define('ERROR_PAGE_PATH', __PHP_ASSETS__ . '/error_page.php');
//    define('ERROR_PAGE_PATH', __PHP_ASSETS__ . '/error_page_apuppt.php');
    define('ERROR_LOG_PATH', __INCLUDES__ . '/error_log');
?>
