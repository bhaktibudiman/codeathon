<?php

class System {
    public static function GetMs()
    {
        list($usec, $sec) = explode(" ", microtime());
        list($man,$exp) = explode('.',$usec);
        return substr($exp,2,4);
    }

    public static function GenerateNumber()
    {
        return date('YmdHis').System::GetMs();
    }

    public static function GetId($intNumber=null)
    {
        return number_format(microtime(true)*1,0,'','');
    }

    public static function getHash($strSource)
    {
        return md5(sha1(md5($strSource)));
    }

    public static function DateView($objDate)
    {
        if(!$objDate) return null;
        if($objDate->PhpDate('dmY') == date('dmY')) return $objDate->PhpDate('H:i');
        return $objDate->PhpDate('d-m-Y');
    }

    /**
     * Setting metada data of record
     * @param object $objData
     * @param string $strUserLogin
     * @param mixed $params
     * @return object $objData
     *
     * */
    public static function UpdateMetaData($objData,$strUserId=null,$forceInsert=false)
    {
        $time = QDateTime::Now();

        $blInsert = $forceInsert OR !$objData->__Restored;
        if(!$strUserId) $strUserId = $_SESSION[__USER_LOGIN__];

        if($blInsert)
        {
            $objData->InsertUser = $strUserId;
            $objData->InsertTime = $time;
        }
        else
        {
            $objData->ModifyUser = $strUserId;
            $objData->ModifyTime = $time;
            $objData->ModifyCount = $objData->ModifyCount ? $objData->ModifyCount+1 : 1;
        }
    }

    public static function NavigationControl($prevControl,$nextControl)
    {
        $actionDown = new QJavaScriptAction('$(\'#'.$nextControl->ControlId.'\').focus()');
        $actionUp = new QJavaScriptAction('$(\'#'.$prevControl->ControlId.'\').focus()');
        $prevControl->AddAction(new QEnterKeyEvent(),$actionDown);
        $nextControl->AddAction(new QEscapeKeyEvent(),$actionUp);
    }
} 