<?php
    /**
     * The Application class is an abstract class that statically provides
     * information and global utilities for the entire web application.
     *
     * Custom constants for this webapp, as well as global variables and global
     * methods should be declared in this abstract class (declared statically).
     *
     * This Application class should extend from the ApplicationBase class in
     * the framework.
     */
    abstract class QApplication extends QApplicationBase {
        /**
         * This is called by the PHP5 Autoloader.  This method overrides the
         * one in ApplicationBase.
         *
         * @return void
         */
        public static function Autoload($strClassName) {
            // First use the QCubed Autoloader
            if (!parent::Autoload($strClassName)) {
                // TODO: Run any custom autoloading functionality (if any) here...
            }
        }

        ////////////////////////////
        // Additional Static Methods
        ////////////////////////////
        // TODO: Define any other custom global WebApplication functions (if any) here...

        public static function UrlCreator($strUrl)
        {
            $root = __SUBDIRECTORY__;
            /*if(stristr($_SERVER['REQUEST_URI'],'.php')) $root .= '/index.php';*/
            return $root.'/'.$strUrl;
        }
    }
?>