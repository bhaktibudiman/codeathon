<?php
include_once('static_path.php');

if(!isset($_SERVER['SHELL']))
{
    if(!$pathSettingDocRoot OR $pathSettingDocRoot != $_SERVER['DOCUMENT_ROOT'])
    {
        reGeneratePath($pathSettingDocRoot,$pathSettingSubDirectory);
    }
}

function reGeneratePath(&$pathSettingDocRoot,&$pathSettingSubDirectory)
{
    $pathSettingDocRoot = $_SERVER['DOCUMENT_ROOT'];
    $pathSettingSubDirectory = (dirname($_SERVER['SCRIPT_NAME']) != '/' ? dirname($_SERVER['SCRIPT_NAME']) :'');

    $path = $pathSettingDocRoot . $pathSettingSubDirectory.'/includes/configuration/static_path.php';
    $path = preg_replace('~/+~', '/', $path);
    $fp = fopen($path, 'w+');
    $strToWrite = '<?php ';
    $strToWrite .= "\r\n".'$pathSettingDocRoot = \''.$pathSettingDocRoot.'\';';
    $strToWrite .= "\r\n".'$pathSettingSubDirectory = \''.$pathSettingSubDirectory.'\';';
    $strToWrite .= "\r\n".' ?>';
    fwrite($fp, $strToWrite);
    fclose($fp);

}
?>