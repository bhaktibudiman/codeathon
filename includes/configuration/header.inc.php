<?php
require('protected.inc.php');
$objParam = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM', 'SESSION_TIMEOUT');
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > (int)$objParam->ParamValue)) {
    $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
    $objUser->IsLocked = false;
    $objUser->LockedSession = null;
    $objUser->IsLoggedIn = false;
    $objUser->LastLogin = QDateTime::Now();
    $objUser->Save();
    
    session_unset();
    session_destroy();

}
$_SESSION['LAST_ACTIVITY'] = time();
?>

<!DOCTYPE html>
<html>
<head>
		<meta charset="<?php _p(QApplication::$EncodingType); ?>" />
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="description" content="Regulatory Solution">
        <title>Regulatory Solution</title>
        <link rel="shortcut icon" href="<?php _p(__APP_IMAGE_ASSETS__); ?>/logo-title2.png">

        <!--Stylesheet-->
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/app.min.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/ionicons.min.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/_all-skins.min.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/style.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/bootstrap.min.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/font-awesome.min.css");</style>

        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/iCheck/flat/blue.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/morris/morris.css");</style>
        <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/datatables/dataTables.bootstrap.css");</style>

        <!-- JQUERY -->
        <script type="text/javascript" src="<?php _p(__APP_JQUERY_ASSETS__)?>/jQuery-2.1.4.min.js"></script>
        <script type="text/javascript" src="<?php _p(__APP_JQUERY_ASSETS__)?>/custom.js"></script>

        <!-- JavaScript-->
        <script type="text/javascript" src="<?php _p(__APP_JS_ASSETS__)?>/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php _p(__APP_JS_ASSETS__)?>/app.min.js"></script>
        <script type="text/javascript" src="<?php _p(__APP_JS_ASSETS__)?>/highcharts.js"></script>

       <!--  <script language="javascript">
            document.onmousedown=disableclick;
            function disableclick(event)
            {
                if(event.button==2)
                {
                    return false;
                }
            }
        </script> -->

        <script type="text/javascript">
            $(document).ajaxStart(function() {
                alert('sdfds');
            });
            
            
        </script>

</head>

<body class="skin-blue sidebar-mini" oncontextmenu="return false">
    <div class="wrapper">
        <header class="main-header">
            <a href="#" class="logo" style="text-decoration: none">
                <span class="logo-mini"><img src="<?php _p(__APP_IMAGE_ASSETS__) ?>/logo-header2.png" /></span>

                <span class="logo-lg" style="font-size: 18px">
                    <img src="<?php _p(__APP_IMAGE_ASSETS__) ?>/logo-header2.png" style="margin-left: -10px" />&nbsp; &nbsp;Regulatory Solution
                </span>
            </a>

            <nav class="navbar navbar-static-top" role="navigation">
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                </a>
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                    <span>
                                    <?php
                                    if (isset($_SESSION[__USER_LOGIN__])) {
                                        echo strtoupper($objUser->Username);
                                    }else{
                                        QApplication::Redirect(__SOURCE__ . '/login/form.php');
                                    }
                                    ?>
                                        <i class="caret"></i>
                                    </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="user-header">
                                    <img src="<?php _P(__APP_IMAGE_UPLOAD__ . '/' . $objUser->LinkImage)  ?>" class="img-circle" alt="User Image" />
                                    <p><?php echo $objUser->Username; ?><small><?php echo("Last Login " . date_format($objUser->LastLogin, "d M Y h:m:s")) ?></small></p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php _P(__SUBDIRECTORY__ . "/src/user/user/view.php/".$objUser->UserId);?>"
                                           class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo(__SOURCE__ . "/login/logout.php"); ?>" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="main-sidebar">
            <section class="sidebar">
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php _P(__APP_IMAGE_UPLOAD__ . '/' . $objUser->LinkImage) ?>" class="img-circle" alt="User Image" />
                    </div>
                    <div class="pull-left info">
                        <p><?php echo strtoupper($objUser->Username); ?></p>
                        <?php $branch = BankBranch::Load($objUser->BranchCode); ?>
                        <!--<a href="#"><i class="fa fa-circle text-success"></i> </a>-->
                        <a href="#"><?php echo ($branch)?$branch->BranchName:'-'; ?></a>
                    </div>
                </div>
                <?php
                if(isset($_SESSION[__USER_LOGIN__])){
                    include "menu.php";
                }else{
                    QApplication::DisplayAlert('Anda Belum Login');
                    QApplication::Redirect('/apupptjiarsi/index.php');
                }
                ?>
            </section>
        </aside>

        <div class="content-wrapper">
            <section class="content">
