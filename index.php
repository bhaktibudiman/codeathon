<?php
include('includes/configuration/protected.inc.php');

if(!isset($_SESSION[__USER_LOGIN__]))
{
    QApplication::Redirect(__SOURCE__ . '/login/form.php');
}else{
    QApplication::Redirect('home.php');
}
?>
