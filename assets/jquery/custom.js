$(function(){
	// function untuk angka, huruf, spasi, dan backspace saja
	$.fn.alphanumericOnly = function () {
		this.keypress(function (e) {
			var keycode = e.charCode;
		    var regex = new RegExp("^[a-zA-Z0-9@\/\-]+$");
		    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		    if (regex.test(str) || e.charCode == 0 || e.charCode == 32) {
		        return true;	
		    }
		    e.preventDefault();
	    	return false;
		});
	}
});