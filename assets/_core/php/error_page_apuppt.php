<!DOCTYPE html>
<html>
<head>
    <title>Regulatory Solution</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php _p(__APP_IMAGE_ASSETS__); ?>/logo-title2.png">
    <meta charset="UTF-8" />
    <meta name="description" content="Regulatory Solution">

    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/app.min.css");</style>
    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/font-awesome.min.css");</style>
    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/bootstrap.min.css");</style>
    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/error-style.css");</style>
</head>

<body>
    <div id="outline-border-error">
        <div id="inline-border-error">
            <center><img style="margin-top: 20px" src="<?php _p(__APP_IMAGE_ASSETS__) ?>/login-logo.png" /></center>

            <center>
                <p><h1>Sorry, content not found !</h1></p>
                <p>
                    <button class="btn btn-primary" onclick="history.go(-1);"><span class="fa fa-hand-o-left">&nbsp;Go Back</span></button>
                    <button class="btn btn-info" onclick="goHomePage()"><span class="fa fa-home">&nbsp;HomePage</span></button>
                </p>
            </center>
        </div>
    </div>
</body>

<script>
    function goHomePage() {
        window.location.assign("<?php echo __SUBDIRECTORY__.'/home.php'; ?>")
    }
</script>

</html>