<?php require('includes/configuration/header.inc.php'); ?>
<?php
$objParamExpireCount = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM','EXPIRED_PASSWORD');
$objParamExpireWarning = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM','EXPIRED_PWD_WARNING');
$objUser = Users::Load($_SESSION[__USER_LOGIN__]);
$intWarning = $objParamExpireCount->ParamValue - $objParamExpireWarning->ParamValue;
if($objUser->ExpiredCount >= $intWarning){
    $expTotal = $objParamExpireCount->ParamValue - $objUser->ExpiredCount;
    if($expTotal == 1){
        QApplication::DisplayAlert('Your password will be expired in '.$expTotal.' day.');
    }else{
        QApplication::DisplayAlert('Your password will be expired in '.$expTotal.' days.');
    }
}
?>
<section class="content-header">
    <h1><center>I-BENEF</center></h1>
</section>

<div class="row">
    <div class="col-md-12">
        <div class="box box-danger">
            
        </div>
    </div>

    <div class="col-md-12">
    </div>
</div>

<script type = "text/javascript" >
    function changeHashOnLoad() {
        window.location.href += '#';
        setTimeout('changeHashAgain()', '50');
    }

    function changeHashAgain() {
        window.location.href += '1';
    }

    var storedHash = window.location.hash;
    window.setInterval(function () {
        if (window.location.hash != storedHash) {
            window.location.hash = storedHash;
        }
    }, 50);
    window.onload=changeHashOnLoad;
</script>


<?php require('includes/configuration/footer.inc.php'); ?>
