<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

	class StagingMenuApupptEditForm extends QForm {

        // Local instance of the StagingMenuApupptMetaControl
        /**
         * @var StagingMenuApupptMetaControlGen mctStagingMenuApuppt
         */
        protected $mctStagingMenuApuppt;

        // Controls for StagingMenuApuppt's Data Fields
        protected $lblIdStagingMenu;
        protected $txtMenuName;
        protected $txtUrlMenuStagging;
        protected $txtStagingOrder;
        protected $calCreatedDate;
        protected $txtCreatedBy;

        // Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References

        // Other Controls
        /**
         * @var QButton Save
         */
        protected $btnSave;
        /**
         * @var QButton Delete
         */
        protected $btnDelete;
        /**
         * @var QButton Cancel
         */
        protected $btnCancel;

        // Create QForm Event Handlers as Needed

//		protected function Form_Exit() {}
//		protected function Form_Load() {}
//		protected function Form_PreRender() {}

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            // Use the CreateFromPathInfo shortcut (this can also be done manually using the StagingMenuApupptMetaControl constructor)
            // MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
            $this->mctStagingMenuApuppt = StagingMenuApupptMetaControl::CreateFromPathInfo($this);

            // Call MetaControl's methods to create qcontrols based on StagingMenuApuppt's data fields
//            $this->lblIdStagingMenu = $this->mctStagingMenuApuppt->lblIdStagingMenu_Create();
            $this->txtMenuName = $this->mctStagingMenuApuppt->txtMenuName_Create();
            $this->txtMenuName->Required = true;
            $this->txtUrlMenuStagging = $this->mctStagingMenuApuppt->txtUrlMenuStagging_Create();
            $this->txtUrlMenuStagging->Required = true;
            $this->txtStagingOrder = $this->mctStagingMenuApuppt->txtStagingOrder_Create();
            $this->txtStagingOrder->Required = true;
            $this->calCreatedDate = $this->mctStagingMenuApuppt->calCreatedDate_Create();
            $this->txtCreatedBy = $this->mctStagingMenuApuppt->txtCreatedBy_Create();

            // Create Buttons and Actions on this Form
            $this->btnSave = new QButton($this);
            $this->btnSave->Text = QApplication::Translate('Save');
            $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
            $this->btnSave->CausesValidation = true;
            $this->btnSave->CssClass = 'btn btn-primary';

            $this->btnCancel = new QButton($this);
            $this->btnCancel->Text = QApplication::Translate('Cancel');
            $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
            $this->btnCancel->CssClass = 'btn btn-warning';

            $this->btnDelete = new QButton($this);
            $this->btnDelete->Text = QApplication::Translate('Delete');
            $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE you want to DELETE this %s?'), QApplication::Translate('StagingMenuApuppt'))));
            $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
            $this->btnDelete->Visible = $this->mctStagingMenuApuppt->EditMode;
            $this->btnDelete->CssClass = 'btn btn-danger';
        }

        /**
         * This Form_Validate event handler allows you to specify any custom Form Validation rules.
         * It will also Blink() on all invalid controls, as well as Focus() on the top-most invalid control.
         */
        protected function Form_Validate() {
            // By default, we report the result of validation from the parent
            $blnToReturn = parent::Form_Validate();

            // Custom Validation Rules
            // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

            $blnFocused = false;
            foreach ($this->GetErrorControls() as $objControl) {
                // Set Focus to the top-most invalid control
                if (!$blnFocused) {
                    $objControl->Focus();
                    $blnFocused = true;
                }

                // Blink on ALL invalid controls
                $objControl->Blink();
            }

            return $blnToReturn;
        }

        // Button Event Handlers

        protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Save" processing to the StagingMenuApupptMetaControl
            $this->mctStagingMenuApuppt->SaveStagingMenuApuppt();
            /*$objStagingMenu = StagingMenuApuppt::Load(QApplication::PathInfo(0));
            $objStagingMenu->CreatedDate = QDateTime::Now();
            $objStagingMenu->CreatedBy = $_SESSION[__USER_LOGIN__];
            $objStagingMenu->Save();*/
            $this->RedirectToListPage();
        }

        protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Delete" processing to the StagingMenuApupptMetaControl
            $this->mctStagingMenuApuppt->DeleteStagingMenuApuppt();
            $this->RedirectToListPage();
        }

        protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
            $this->RedirectToListPage();
        }

        // Other Methods

        protected function RedirectToListPage() {
            QApplication::Redirect(__SOURCE__ . '/staging_menu/list.php');
        }
	}

	StagingMenuApupptEditForm::Run('StagingMenuApupptEditForm');
?>