<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

<?php if($_SESSION[__STAGING__] != NULL){ ?>
    <section class="content-header">
        <h1>Run Staging</h1>
    </section>

    <div class="box box-primary">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <?php echo("Staging Begin <i class='fa fa-check'></i><br/>"); ?>
                <?php
                if($_SESSION[__STAGING__]){
                    if(substr($_SESSION[__STAGING__],-3) != 'php'){
                        echo "Staging Can't Running <i class='fa fa-close'></i>";
                    }else{
                        include($_SESSION[__STAGING__]);
                    }
                }
                $_SESSION[__STAGING__] = NULL;
                ?>
            </div>
        </div>
    </div>
<?php } ?>

    <section class="content-header">
        <h1>List Staging Menu</h1>
    </section>

    <div class="box box-primary">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <div class="col-md-1">
                    <a href="form.php" class="btn btn-success" title="Add"><span class="fa fa-plus"></span> Add</a>
                </div>
                <?php $this->dtgStagingMenus->Render(); ?>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>