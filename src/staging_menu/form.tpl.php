<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-xs-8">
            <div class="box">
                <div class="box-header with-border">
                    <h3><?php _p($this->mctStagingMenuApuppt->TitleVerb); ?> <?php _t('Staging Menu')?></h3>
                </div>
                <div class="box-body">
                    <?php $this->txtMenuName->RenderWithBootstrap(3,5); ?>
                    <?php $this->txtUrlMenuStagging->RenderWithBootstrap(3,5); ?>
                    <?php $this->txtStagingOrder->RenderWithBootstrap(3,5); ?>
                </div>

                <div class="box-footer">
                    <?php $this->btnSave->Render(); ?>
                    <?php $this->btnCancel->Render(); ?>
                    <?php $this->btnDelete->Render(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>