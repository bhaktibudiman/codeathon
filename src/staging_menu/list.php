<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

	class StagingMenuApupptListForm extends QForm {

        protected $dtgStagingMenus;
        protected $objLinkProxy;

		protected function Form_Run() {
			parent::Form_Run();

			QApplication::CheckRemoteAdmin();
		}

//		protected function Form_Load() {}

		protected function Form_Create() {
            $this->dtgStagingMenus = new StagingMenuApupptDataGrid($this);

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

            $this->dtgStagingMenus->Paginator = new QPaginator($this->dtgStagingMenus);
            $this->dtgStagingMenus->ItemsPerPage = 10;

            $this->dtgStagingMenus->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgStagingMenus->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM)?>','HtmlEntities=false', 'Width=100'));
            $this->dtgStagingMenus->MetaAddColumn('MenuName');
            $this->dtgStagingMenus->MetaAddColumn('UrlMenuStagging');
            $this->dtgStagingMenus->MetaAddColumn('StagingOrder');
            $this->dtgStagingMenus->MetaAddColumn('CreatedDate');
            $this->dtgStagingMenus->AddColumn(new QDataGridColumn('Created By', '<?= $_FORM->getUser($_ITEM->CreatedBy) ?>'));

            $this->dtgStagingMenus->SetDataBinder('PageBinder', $this);
        }

        public function rowActionButton($objRecord)
        {
            $strReturn = null;

            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="View"><span class="fa fa-pencil"></span></a>',
                __SOURCE__ . '/staging_menu/form.php/'.$objRecord->IdStagingMenu);
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Delete" onclick="return confirm(\'Delete confirmation %s ?\')">
                              <span class="fa fa-trash-o"></span></a>',$this->objLinkProxy->RenderAsHref('del_'.$objRecord->IdStagingMenu,false),$objRecord->MenuName);
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-warning" title="Start Screening" onclick="return confirm(\'Start Staging For %s ?\')">
                              <span class="fa fa-power-off"></span></a>',$this->objLinkProxy->RenderAsHref('run_'.$objRecord->UrlMenuStagging,false),$objRecord->MenuName);

            return $strReturn;
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            switch($section)
            {
                case'del':
                    $objMenu = StagingMenuApuppt::Load($param);
                    $objMenu->Delete();
                    QApplication::DisplayAlert('Menu ' . $objMenu->MenuName . ' has been Removed');
                    $this->dtgStagingMenus->Refresh();
                    break;
                case'run':
                    $_SESSION[__STAGING__] = $param;
                    $this->RedirectToListPage();
                    break;
            }
        }

        public function PageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            if($searchText = $this->dtgStagingMenus->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::StagingMenuApuppt()->MenuName, '%' . $searchText . '%')
                ));
            }

            $this->dtgStagingMenus->MetaDataBinderCustom(new StagingMenuApuppt(), QQ::AndCondition($objCondition));
        }

        public function getUser($id) {
            $objUser = Users::QuerySingle(
                QQ::Equal(QQN::Users()->UserId, $id)
            );
            $returnString = (isset($objUser))?$objUser->Username:'';

            return $returnString;
        }

        protected function RedirectToListPage() {
            QApplication::Redirect(__SOURCE__ . '/staging_menu/list.php');
        }
	}

	StagingMenuApupptListForm::Run('StagingMenuApupptListForm');
?>