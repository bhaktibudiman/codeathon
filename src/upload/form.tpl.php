<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

    <section class="content-header">
        <h1>Upload Data</h1>
    </section>
    <br>
<div class="col-md-6">
    <div class="box">
        <div class="box-body">
            <?php $this->lstDelimit->RenderWithName() ?>
            <br class="item_divider" />
            <?php $this->lstFileName->RenderWithName() ?>
            <br class="item_divider" />
            <?php $this->txtFile->RenderWithName() ?>
            <br class="item_divider" />
            <?php $this->btnUpload->Render() ?>&nbsp;
            <?php $this->objDefaultWaitIcon->Render('Position=absolute', 'Top=10px'); ?>
        </div>
    </div>
</div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>