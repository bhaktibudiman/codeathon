<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');
require_once($prePath.'/pdo_db.php');

class UploadForm extends QForm {

    protected $lstDelimit;
    protected $lstFileName;
    protected $txtFile;
    protected $btnUpload;

    protected function Form_Create() {
        $this->lstDelimit = new QListBox($this);
        $this->lstDelimit->Name = 'Delimiter';
        $this->lstDelimit->AddItem('- Select One -', null);
        $this->lstDelimit->AddItem(',', ',');
        $this->lstDelimit->AddItem('^', '^');
        $this->lstDelimit->AddItem(';', ';');
        $this->lstDelimit->AddItem('|', '|');
        $this->lstDelimit->Required = true;

        $this->lstFileName = new QListBox($this);
        $this->lstFileName->Name = 'Filename';
        $this->lstFileName->AddItem('- Select One -', null);
        /*$this->lstFileName->AddItem('CORP_LEGAL_CSV', 'CORP_LEGAL_CSV');
        $this->lstFileName->AddItem('FT_CSV', 'FT_CSV');
        $this->lstFileName->AddItem('CATEG_ENTRY_CSV', 'CATEG_ENTRY_CSV');
        $this->lstFileName->AddItem('CORP_BOD_CSV', 'CORP_BOD_CSV');
        $this->lstFileName->AddItem('CUST_EMAIL_CSV', 'CUST_EMAIL_CSV');
        $this->lstFileName->AddItem('CUST_PHONE_CSV', 'CUST_PHONE_CSV');
        $this->lstFileName->AddItem('CUST_SMS_CSV', 'CUST_SMS_CSV');
        $this->lstFileName->AddItem('EXT_ACCOUNT_CLOSED_CSV', 'EXT_ACCOUNT_CLOSED_CSV');
        $this->lstFileName->AddItem('OWNER_CSV', 'OWNER_CSV');
        $this->lstFileName->AddItem('EXT_ACCOUNT_CSV', 'EXT_ACCOUNT_CSV');
        $this->lstFileName->AddItem('ACCOUNT_CSV', 'ACCOUNT_CSV');
        $this->lstFileName->AddItem('TT_CSV', 'TT_CSV');
        $this->lstFileName->AddItem('PERS_EMPLOYMENT_CSV', 'PERS_EMPLOYMENT_CSV');
        $this->lstFileName->AddItem('STMT_ENTRY_CSV', 'STMT_ENTRY_CSV');
        $this->lstFileName->AddItem('EXT_CUSTOMER_CSV', 'EXT_CUSTOMER_CSV');*/
        // $this->lstFileName->AddItem('SUSP_KPK_CSV', 'SUSP_KPK_CSV');
        // $this->lstFileName->AddItem('SUSP_OTHER_CSV', 'SUSP_OTHER_CSV');
        // $this->lstFileName->AddItem('SUSP_OWNER_CSV', 'SUSP_OWNER_CSV');
        // $this->lstFileName->AddItem('SUSP_PPATK_CSV', 'SUSP_PPATK_CSV');
        // $this->lstFileName->AddItem('SUSP_PEP_CSV', 'SUSP_PEP_CSV');
        // $this->lstFileName->AddItem('SUSP_DTTOT', 'SUSP_DTTOT');
        $this->lstFileName->AddItem('SUSP_OFAC_XML', 'SUSP_OFAC');
        // $this->lstFileName->AddItem('TERORIS_XML', 'SUSP_TERORIS');
        $this->lstFileName->AddItem('STOCK_EXCHANGE', 'STOCK_EXCHANGE');
        // $this->lstFileName->AddItem('TERORIS_CORPORATE_CSV', 'TERORIS_CORPORATE_CSV');
        // $this->lstFileName->AddItem('NEW_KS_CUSTOMER', 'NEW_KS_CUSTOMER');
        $this->lstFileName->Required = true;

        $this->txtFile = new QFileControl($this);
        $this->txtFile->Name = 'File Upload';
        $this->txtFile->Required = true;

        $this->btnUpload = new QButton($this);
        $this->btnUpload->Text=QApplication::Translate('Upload');
        $this->btnUpload->AddAction(new QClickEvent(), new QServerAction('btnUpload_Click'));
        $this->btnUpload->PrimaryButton = true;
        $this->btnUpload->CausesValidation = true;
        $this->btnUpload->CssClass = 'btn btn-primary';

        $this->objDefaultWaitIcon = new QWaitIcon($this);
    }

    protected function Form_Validate() {
        $blnToReturn = parent::Form_Validate();

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            $objControl->Blink();
        }

        return $blnToReturn;
    }

    protected function btnKlik_Click($strFormId, $strControlId, $strParameter) {
        QApplication::DisplayAlert($this->lstFileName->SelectedValue);
    }

    protected function btnUpload_Click($strFormId, $strControlId, $strParameter) {
        try{
            if (file_exists($this->txtFile->File)) {
                if ($this->txtFile->Size > 0) {
                    $db = new core_db();
                    $dbconn4 = $db->connect();
                    if (!$dbconn4) {
                        QApplication::DisplayAlert("There is no connection !");
                    } else {
                        $fileName = $this->txtFile->FileName;
                        $tmpFile = $this->txtFile->File;

                        switch($this->lstFileName->SelectedValue){
                            case 'NEW_KS_CUSTOMER':
                                $columnTable = '(
                                  "insert_date",
                                  "id",
                                  "sid_related",
                                  "sid_golongan_de",
                                  "sid_dati2debtor",
                                  "sid_sektor_econ",
                                  "sid_jenis_usaha",
                                  "co_code",
                                  "kyc_intend_fund",
                                  "kyc_fund_src",
                                  "sector",
                                  "contact_date",
                                  "sid_sifat_kredi",
                                  "gur_name",
                                  "gur_bagian_dija",
                                  "name_1",
                                  "short_name",
                                  "street",
                                  "post_code",
                                  "ktp_kelurahan",
                                  "ktp_kacamatan",
                                  "legal_id",
                                  "legal_doc_name",
                                  "legal_holder_name",
                                  "sid_tempat_akte",
                                  "sid_tgl_akte_ak",
                                  "place_of_birth",
                                  "date_of_birth",
                                  "mother_maid_nam",
                                  "gender",
                                  "last_education",
                                  "sid_din",
                                  "ktp_rt",
                                  "ktp_rw",
                                  "phone_1",
                                  "sms_1",
                                  "country",
                                  "occupation",
                                  "employment_status",
                                  "employers_name",
                                  "sid_group_id",
                                  "sid_malanggar",
                                  "sid_melampaui",
                                  "sid_rating_deb",
                                  "sid_lembanga",
                                  "sid_go_public",
                                  "kyc_akte_no",
                                  "kyc_jenis_peng",
                                  "peng_nama",
                                  "peng_jenis_kela",
                                  "peng_identitas",
                                  "peng_npwp",
                                  "peng_alamat",
                                  "peng_dati2",
                                  "peng_kelurahan",
                                  "peng_kecamatan",
                                  "peng_pemilikan",
                                  "peng_id_jabatan",
                                  "peng_id",
                                  "gur_gol_penjami",
                                  "gur_legal_id",
                                  "gur_npwp",
                                  "gur_street",
                                  "gur_addrs_rt",
                                  "gur_addrs_rw",
                                  "gur_prime_bank",
                                  "title",
                                  "marital_status",
                                  "sid_id_debitur",
                                  "company_book",
                                  "alt_cust_id",
                                  "town_country",
                                  "fax_1",
                                  "email_1",
                                  "residence",
                                  "account_officer",
                                  "date_time",
                                  "religion",
                                  "residence_status",
                                  "residence_since",
                                  "legal_iss_date",
                                  "legal_exp_date",
                                  "job_title",
                                  "employers_buss",
                                  "employers_add",
                                  "off_street",
                                  "off_town_city",
                                  "off_post_code",
                                  "net_monthly_in",
                                  "salary",
                                  "company_type",
                                  "industry",
                                  "comp_permit_no",
                                  "birth_incorp_date",
                                  "kyc_incom_rng",
                                  "kyc_cap_owner",
                                  "loan_to_bank",
                                  "company_bank",
                                  "spouse_name",
                                  "spou_pl_of_birt",
                                  "spou_dt_of_birt",
                                  "spouse_id",
                                  "atm_number",
                                  "kyc_cash_dep_li",
                                  "kyc_cash_dep_fq",
                                  "kyc_noncash_lim",
                                  "kyc_noncash_fq",
                                  "kyc_cash_wd_lim",
                                  "kyc_cash_wd_fq",
                                  "kyc_non_cash_wd",
                                  "kyc_noncash_fqu",
                                  "bank_name",
                                  "type_of_loan",
                                  "loan_value",
                                  "ktp_provinsi",
                                  "ktp_provinsi_desc",
                                  "job_title_desc",
                                  "kyc_sumber_dana",
                                  "gur_phone",
                                  "contact_person",
                                  "con_address",
                                  "con_phone",
                                  "gur_id",
                                  "kyc_income_src",
                                  "off_phone",
                                  "kyc_sumber_dana_desc",
                                  "home_street",
                                  "home_kelurahan",
                                  "home_kacamatan",
                                  "hm_town_country",
                                  "home_provinsi",
                                  "home_post_code",
                                  "home_rt",
                                  "home_rw",
                                  "red_flag",
                                  "name_2",
                                  "cus_npwp",
                                  "risk_level",
                                  "record_status",
                                  "nationality",
                                  "customer_rating",
                                  "customer_type",
                                  "gur_city",
                                  "spouse_address",
                                  "credit_card",
                                  "residence_month",
                                  "residence_year",
                                  "given_names",
                                  "no_of_dependents",
                                  "residence_type",
                                  "home_provinsi_desc",
                                  "inputter",
                                  "authoriser",
                                  "sid_kode_peng",
                                  "curr_no",
                                  "sandi_bi",
                                  "cus_open_date",
                                  "sid_gol_kredit",
                                  "sandi_bpr_bi"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";

                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate staging_customer_ks");
                                    if($truncate)
                                        pg_query("COPY staging_customer_ks".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;



                            case 'CORP_LEGAL_CSV':
                                $columnTable = '(
                                    "cif",
                                    "legal_id",
                                    "legal_type",
                                    "legal_ish_auth",
                                    "legal_iss_data",
                                    "legal_exp_date"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";

                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_corp_legal");
                                    if($truncate)
                                    pg_query("COPY tmp_corp_legal".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'FT_CSV':
                                $columnTable = '(
                                    "id",
                                    "debit_their_ref",
                                    "credit_their_ref",
                                    "transaction_type",
                                    "debit_value"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_ft");
                                    if($truncate)
                                    pg_query("COPY tmp_ft".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'CATEG_ENTRY_CSV':
                                $columnTable = '(
                                    "categ_entry_id",
                                    "categ_entry_date"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_categ_entry");
                                    if($truncate)
                                    pg_query("COPY tmp_categ_entry".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'CORP_BOD_CSV':
                                $columnTable = '(
                                    "cif",
                                    "bd_name",
                                    "bd_gender",
                                    "bd_tax_number",
                                    "bd_address",
                                    "bd_rtrw",
                                    "bd_town_country",
                                    "bd_country",
                                    "bd_district_cd",
                                    "bd_pocode",
                                    "bd_prov",
                                    "bd_position"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_corp_bod");
                                    if($truncate)
                                    pg_query("COPY tmp_corp_bod".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'CUST_EMAIL_CSV':
                                $columnTable = '(
                                    "cust_id",
                                    "email"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_cust_email");
                                    if($truncate)
                                    pg_query("COPY tmp_cust_email".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'CUST_PHONE_CSV':
                                $columnTable = '(
                                    "cust_id",
                                    "phone"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_cust_phone");
                                    if($truncate)
                                    pg_query("COPY tmp_cust_phone".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'CUST_SMS_CSV':
                                $columnTable = '(
                                    "cust_id",
                                    "sms_no"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_cust_sms");
                                    if($truncate)
                                    pg_query("COPY tmp_cust_sms".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'EXT_ACCOUNT_CLOSED_CSV':
                                $columnTable = '(
                                    "account_closed",
                                    "closed_date"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_ext_account_closed");
                                    if($truncate)
                                    pg_query("COPY tmp_ext_account_closed".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'OWNER_CSV':
                                $columnTable = '(
                                    "cif",
                                    "ow_name",
                                    "ow_gender",
                                    "ow_tax_no",
                                    "ow_address",
                                    "ow_rtrw",
                                    "ow_town_country",
                                    "ow_country",
                                    "ow_district_cd",
                                    "ow_pocode",
                                    "ow_prov",
                                    "ow_position",
                                    "ow_share_prcnt"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_owner");
                                    if($truncate)
                                    pg_query("COPY tmp_owner".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'EXT_ACCOUNT_CSV':
                                $columnTable = '(
                                    "account",
                                    "num1"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_ext_account");
                                    if($truncate)
                                    pg_query("COPY tmp_ext_account".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'ACCOUNT_CSV':
                                $columnTable = '(
                                    "ac_01_report_date",
                                    "ac_02_account_number",
                                    "ac_03_co_code",
                                    "ac_04_cabang_bi",
                                    "ac_05_category",
                                    "ac_06_currency",
                                    "ac_07_cif",
                                    "ac_08_limit_id",
                                    "ac_09_opening_date",
                                    "ac_010_closure_date",
                                    "ac_011_ac_asset_class",
                                    "ac_012_interest_rate",
                                    "ac_013_open_actual_bal",
                                    "ac_014_ld_condition",
                                    "ac_015_condition_date",
                                    "ac_016_pk_number",
                                    "ac_017_first_pk_date",
                                    "ac_018_last_pk_num",
                                    "ac_019_last_pk_date",
                                    "ac_020_stagnant_date",
                                    "ac_021_stagnant_reason",
                                    "ac_022_tanggal_tunggakan",
                                    "ac_023_amount_tunggakan",
                                    "ac_024_frekuensi_tunggakan",
                                    "ac_025_tunggakan_pokok",
                                    "ac_026_frekuensi_tunggakan_pokok",
                                    "ac_027_tunggakan_bunga",
                                    "ac_028_frekuensi_tunggakan_bunga",
                                    "ac_029_denda",
                                    "ac_030_debtor_cont",
                                    "ac_031_debtor_prob",
                                    "ac_032_debtor_desc",
                                    "ac_033_debet_movement",
                                    "ac_034_credit_movement",
                                    "ac_035_debtor_category",
                                    "ac_036_portifolio_categ",
                                    "ac_037_accr_dr_ammout",
                                    "ac_038_credit_category",
                                    "ac_039_ac_bi_jenis",
                                    "ac_040_lokasi_cabang",
                                    "ac_041_ac_charac",
                                    "ac_042_account_title_1",
                                    "ac_043_alt_acct_id",
                                    "ac_044_type_of_liab",
                                    "ac_045_depo_jenis",
                                    "ac_046_depo_charac",
                                    "ac_047_locked_ammount",
                                    "ac_048_collatrl_no",
                                    "ac_049_collatrl_percth",
                                    "ac_050_accr_cr_ammount",
                                    "ac_051_ia_mat_date",
                                    "ac_052_lbu_cust_type",
                                    "ac_053_owner_status",
                                    "ac_054_nationality",
                                    "ac_055_ia_interest",
                                    "ac_056_ia_int_type",
                                    "ac_057_residence",
                                    "ac_058_sid_relati_bank",
                                    "ac_059_ac_col_amt",
                                    "ac_060_coll_charac",
                                    "ac_061_ia_coll_type",
                                    "ac_062_ia_coll_curr",
                                    "ac_063_ia_coll_start",
                                    "ac_064_ia_coll_end",
                                    "ac_065_ia_coll_amt",
                                    "ac_066_ia_review_end",
                                    "ac_067_coll_issuer",
                                    "ac_068_coll_rate_inst",
                                    "ac_069_coll_bank_level",
                                    "ac_070_coll_rate_date",
                                    "ac_071_ac_prov_amt",
                                    "ac_072_ind_placement",
                                    "ac_073_col_placement",
                                    "ac_074_ia_accr_amt",
                                    "ac_075_cu_rating",
                                    "ac_076_cu_rate_inst",
                                    "ac_077_cu_rate_date",
                                    "ac_078_cif_shot_name",
                                    "ac_079_address",
                                    "ac_080_type_of_use",
                                    "ac_081_prk_limit_ref",
                                    "ac_082_exchange_rate",
                                    "ac_083_ekuivalen_idr",
                                    "ac_084_ekuivalen_usd",
                                    "ac_085_kolektibilitas_cif",
                                    "ac_086_sector_cif",
                                    "ac_087_reside_y_n_cif",
                                    "ac_088_guarantor_name",
                                    "ac_089_guarantor_code",
                                    "ac_090_ow_share_percent",
                                    "ac_091_legal_type",
                                    "ac_092_tax_reg_no",
                                    "ac_093_guarantor_add",
                                    "ac_094_prime_bank",
                                    "ac_095_legal_id_no",
                                    "ac_096_ld_sub_product",
                                    "ac_097_detail_collateral"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file,777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_account");
                                    if($truncate)
                                    pg_query("COPY tmp_account".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'TT_CSV':
                                $columnTable = '(
                                    "id",
                                    "narrative_1",
                                    "narrative_2",
                                    "transaction_code"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_tt");
                                    if($truncate)
                                    pg_query("COPY tmp_tt".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'PERS_EMPLOYMENT_CSV':
                                $columnTable = '(
                                    "cust_id",
                                    "num1",
                                    "occupation",
                                    "num2",
                                    "employers_name",
                                    "employers_add",
                                    "employment_start"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_pers_employment");
                                    if($truncate)
                                    pg_query("COPY tmp_pers_employment".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'STMT_ENTRY_CSV':
                                $columnTable = '(
                                    "last_working_day",
                                    "id",
                                    "acc_number",
                                    "company_code",
                                    "ammount_fcy",
                                    "ammount_lcy",
                                    "transaction_code",
                                    "their_reference",
                                    "pl_category",
                                    "customer_id",
                                    "exchange_rate",
                                    "exchange_date",
                                    "currency_market",
                                    "local_ref",
                                    "system_id",
                                    "stmt_no",
                                    "record_status",
                                    "inputter",
                                    "booking_date",
                                    "value_date",
                                    "console_key",
                                    "accounting_date",
                                    "trade_date",
                                    "our_refference",
                                    "currency",
                                    "account_officer",
                                    "product_category",
                                    "trans_refference",
                                    "authoriser",
                                    "date_time",
                                    "reversal_marker",
                                    "processing_date",
                                    "narrative",
                                    "master_account"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_stmt_entry");
                                    if($truncate)
                                    pg_query("COPY tmp_stmt_entry".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            case 'EXT_CUSTOMER_CSV':
                                $columnTable = '(
                                    "c01id",
                                    "c02mne_monic",
                                    "c03short_name",
                                    "c04name_satu",
                                    "c05name_dua",
                                    "c06street",
                                    "c07town_country",
                                    "c08relation_code",
                                    "c09rel_customer",
                                    "c10revers_rel_code",
                                    "c11sector",
                                    "c12account_officer",
                                    "c13other_officer",
                                    "c14industry",
                                    "c15target",
                                    "c16nationality",
                                    "c17customer_status",
                                    "c18residence",
                                    "c19contact_date",
                                    "c20introducer",
                                    "c21texts",
                                    "c22legal_id",
                                    "c23review_frequency",
                                    "c24birth_incorp_date",
                                    "c25global_customer",
                                    "c26customer_liability",
                                    "c27languages",
                                    "c28record_status",
                                    "c29date_time",
                                    "c30inputter",
                                    "c31authoriser",
                                    "c32co_code",
                                    "c33dept_code",
                                    "c34post_code",
                                    "c35country",
                                    "c36confid_txt",
                                    "c37dispo_exempt",
                                    "c38issue_cheques",
                                    "c39cls_party",
                                    "c40residence_region",
                                    "c41company_book",
                                    "c42customer_rating",
                                    "c43address",
                                    "c44given_names",
                                    "c45family_name",
                                    "c46gender",
                                    "c47date_of_birth",
                                    "c48marital_status",
                                    "c49no_of_dependents",
                                    "c50phone_1",
                                    "c51sms_1",
                                    "c52email_1",
                                    "c53employment_status",
                                    "c54occupation",
                                    "c55employers_name",
                                    "c56employers_add",
                                    "c57employers_buss",
                                    "c58employers_start",
                                    "c59customer_currency",
                                    "c60salary",
                                    "c61annual_bonus",
                                    "c62salary_date_freq",
                                    "c63net_monthly_in",
                                    "c64net_monthly_out",
                                    "c65residence_status",
                                    "c66residence_type",
                                    "c67residence_since",
                                    "c68residence_value",
                                    "c69mortgage_amt",
                                    "c70customer_since",
                                    "c71legal_doc_name",
                                    "c72legal_iss_auth",
                                    "c73legal_iss_date",
                                    "c74legal_exp_date",
                                    "c75off_phone",
                                    "c76legal_holder_name",
                                    "c77job_title",
                                    "c78other_nationality",
                                    "c79second_income",
                                    "c80tax_id",
                                    "c81segment",
                                    "c82cust_title_2",
                                    "c83place_birth",
                                    "c84age",
                                    "c85reside_y_n",
                                    "c86guarantor_code",
                                    "c87moth_maiden",
                                    "c88legal_id_no",
                                    "c89expiry_date_id",
                                    "c90rt_rw",
                                    "c91district_code",
                                    "c92curr_address",
                                    "c93date_curr_address",
                                    "c94yrs_at_cur_addr",
                                    "c95accom_type",
                                    "c96same_as_resadd",
                                    "c97po_box_no",
                                    "c98po_rt_rw",
                                    "c99po_suburb_town",
                                    "c100po_city_municip",
                                    "c101po_dist_code",
                                    "c102po_post_code",
                                    "c103education",
                                    "c104educat_other",
                                    "c105secinc_level",
                                    "c106religion",
                                    "c107sp_name",
                                    "c108sp_occup",
                                    "c109spemp_name",
                                    "c110classification",
                                    "c111staff_official",
                                    "c112staff_design",
                                    "c113related_bank",
                                    "c114relation_bank",
                                    "c115sid_relati_bank",
                                    "c116tax_reg_no",
                                    "c117fax_no",
                                    "c118contact_nam",
                                    "c119contact_pos",
                                    "c120contact_work_tel",
                                    "c121contact_hom_tel",
                                    "c122contact_mob_tel",
                                    "c123contact_email",
                                    "c124contact_fax_no",
                                    "c125contact_relcus",
                                    "c126contact_street",
                                    "c127contact_rt_rw",
                                    "c128contact_sub_twn",
                                    "c129contact_cty_mun",
                                    "c130contact_dis_cod",
                                    "c131contact_pos_cod",
                                    "c132other_accts",
                                    "c133bank_branch",
                                    "c134product",
                                    "c135date_acopnd",
                                    "c136no_debit_trans",
                                    "c137value_dr_trans",
                                    "c138no_credit_trans",
                                    "c139value_cr_trans",
                                    "c140din_number",
                                    "c141bmpk_violation",
                                    "c142bmpk_exceeding",
                                    "c143business_type",
                                    "c144board_owner",
                                    "c145owner_cust_no",
                                    "c146share_percent",
                                    "c147board_of_manag",
                                    "c148manag_cust_no",
                                    "c149assentities",
                                    "c150bus_start_date",
                                    "c151bank_code",
                                    "c152fir_streg_no",
                                    "c153fir_streg_date",
                                    "c154co_group",
                                    "c155share_holding",
                                    "c156ops_allowed",
                                    "c157op_sconds",
                                    "c158acc_purpose",
                                    "c159fund_source",
                                    "c160annual_sales",
                                    "c161cust_type",
                                    "c162tax_able",
                                    "c163po_address",
                                    "c164legacy_ci_num",
                                    "c165lbu_cust_type",
                                    "c166plabi_code",
                                    "c167curating_inst",
                                    "c168curating",
                                    "c169last_agree_num",
                                    "c170last_agree_date",
                                    "c171gopub_flag",
                                    "c172group_code",
                                    "c173owner_status",
                                    "c174portfolio_categ",
                                    "c175curate_date",
                                    "c176ind_placement",
                                    "c177col_placement",
                                    "c178mis_cgol_debit",
                                    "c179sid_cust_type",
                                    "c180alt_cust_id",
                                    "c181legal_type",
                                    "c182province",
                                    "c183addr_phone_area",
                                    "c184addr_phone_no",
                                    "c185po_province",
                                    "c186side_cosector",
                                    "c187income",
                                    "c188acof_fund",
                                    "c189oth_acof_fund",
                                    "c190oth_bank_name",
                                    "c191oth_accn_bnk",
                                    "c192acc_typen_bnk",
                                    "c193oth_typen_bank",
                                    "c194lbbu_cust_type",
                                    "c195profit",
                                    "c196go_public_flag",
                                    "c197cust_sts_flag",
                                    "c198cust_title1",
                                    "c199black_list_bi",
                                    "c200qq_flag",
                                    "c201place_npwp_iss",
                                    "c202non_oprin_come",
                                    "c203authorized_cap",
                                    "c204issued_capital",
                                    "c205paid_up_capital",
                                    "c206contact_ot_name",
                                    "c207contact_ot_prod",
                                    "c208contact_ot_rel",
                                    "c209contact_tr_name",
                                    "c210contact_tx_rel",
                                    "c211contact_tx_buss",
                                    "c212contact_tx_add",
                                    "c213contact_tx_telp",
                                    "c214high_risk_flag",
                                    "c215contact_or_name",
                                    "c216contact_or_rel",
                                    "c217contact_or_buss",
                                    "c218contact_or_add",
                                    "c219contact_or_telp",
                                    "c220ofc_phone_area",
                                    "c221ofc_phone_no",
                                    "c222con_phone_area",
                                    "c223con_phone_no",
                                    "c224emr_phone_area",
                                    "c225emr_phone_no",
                                    "c226addr_fax_area",
                                    "c227addr_fax_no",
                                    "c228curating_code",
                                    "c229cu_interrating",
                                    "c230cu_exter_rating",
                                    "c231con_dua_phone_area",
                                    "c232con_dua_phone_no",
                                    "c233cif_hobby",
                                    "c234cu_extrating_dua",
                                    "c235sid_group_code",
                                    "c236bo_type",
                                    "c237business_spec",
                                    "c238employ_total",
                                    "c239no_tary_name",
                                    "c240reference",
                                    "c241bd_name",
                                    "c242bd_gender",
                                    "c243bd_tax_no",
                                    "c244bd_address",
                                    "c245bd_rt_rw",
                                    "c246bdt_owncountry",
                                    "c247bd_country",
                                    "c248bd_district_cd",
                                    "c249bd_po_code",
                                    "c250bd_prov",
                                    "c251bd_position",
                                    "c252ow_name",
                                    "c253ow_gender",
                                    "c254ow_tax_no",
                                    "c255ow_address",
                                    "c256ow_rt_rw",
                                    "c257owt_own_country",
                                    "c258ow_country",
                                    "c259ow_district_cd",
                                    "c260ow_po_code",
                                    "c261ow_prov",
                                    "c262ow_position",
                                    "c263ow_share_prcnt",
                                    "c264acct_purpose",
                                    "c265asset_total",
                                    "c266current_asset",
                                    "c267liab_total",
                                    "c268liab_bank",
                                    "c269liab_current",
                                    "c270off_shore_loan",
                                    "c271capital",
                                    "c272sales",
                                    "c273opr_income",
                                    "c274opr_expense",
                                    "c275no_pr_income",
                                    "c276no_pr_expense",
                                    "c277pl_last_year",
                                    "c278pl_current",
                                    "c279audited",
                                    "c280risk_country",
                                    "c281risk_occup",
                                    "c282risk_sector",
                                    "c283risk_id",
                                    "c284prevast_class",
                                    "c285astcl_supd",
                                    "c286bank_ops_type",
                                    "c287cif_status"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($tmpFile, $target_file . $fileName);
                                    $truncate = pg_query("truncate tmp_ext_customer");
                                    if($truncate)
                                    pg_query("COPY tmp_ext_customer".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                            break;

                            case 'SUSP_KPK_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_KPK');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                      pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_OWNER_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('BOMB_FACTOR_RISK','OWNERSHIP');
                                $insert = pg_query("insert into header_upload_suspect(filename,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findOwnerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];  

                                $columnTable = '(
                                      "nama",
                                      "tempat_lahir",
                                      "tanggal_lahir" 
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    pg_query("COPY detail_upload_bomb_fact".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    pg_query("update detail_upload_bomb_fact set upload_id="."$lastID. where upload_id is null;");
                                      //.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_OTHER_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_OTHER');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                      pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_DTTOT':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_DTTOT');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                      pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_PENDANAAN_SENJATA_PEMUSNAH_MASSAL':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_PENDANAAN_SENJATA_PEMUSNAH_MASSAL');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                      pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_PPATK_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_PPATK');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                  ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                        pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_PEP_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_PEP');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "nomor",
                                      "nama",
                                      "alias",
                                      "tempat_tgl_lahir",
                                      "jabatan",
                                      "news_1",
                                      "status",
                                      "tanggal_update",
                                      "hukuman",
                                      "tanggal_vonis",
                                      "reference_name"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $file = fopen($target_file . $fileName, 'r');
                                    ini_set('auto_detect_line_endings',TRUE);
                                    $row = 1;
                                    while(($data = fgetcsv($file, 1024, $this->lstDelimit->SelectedValue)) !== FALSE ) {
                                      pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($data[0]) ? strtoupper($data[0]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[1]) ? strtoupper($data[1]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[2]) ? strtoupper($data[2]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[3]) ? strtoupper($data[3]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[4]) ? strtoupper($data[4]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[5]) ? strtoupper($data[5]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[6]) ? strtoupper($data[6]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[7]) ? strtoupper($data[7]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[8]) ? strtoupper($data[8]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[9]) ? strtoupper($data[9]) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($data[10]) ? strtoupper($data[10]) : NULL)),'"')."'
                                          )"
                                        );
                                    $row++;}
                                    ini_set('auto_detect_line_endings',FALSE);
                                    fclose($file);
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_OFAC':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_OFAC');
                                $insert = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',".$objApuParameter->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '
                                      "id_teroris",
                                      "nama",
                                      "first_name",
                                      "second_name",
                                      "alias",
                                      "narrative_1",
                                      "address",
                                      "nationality",
                                      "tempat_tgl_lahir",
                                      "birth_date_place"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $xml_file = $target_file.$fileName;
                                    $ofac_file = simplexml_load_file($xml_file);

                                    foreach($ofac_file->sdnEntry as $ofac)
                                    {
                                        pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->uid) ? $ofac->uid : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($ofac->firstName) or isset($ofac->lastName)) ? strtoupper($ofac->firstName.' '.$ofac->lastName) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->firstName) ? strtoupper($ofac->firstName) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->lastName) ? strtoupper($ofac->lastName) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($ofac->akaList->aka->firstName) or isset($ofac->akaList->aka->lastName)) ? strtoupper($ofac->akaList->aka->firstName.' '.$ofac->akaList->aka->lastName) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->remarks) ? strtoupper($ofac->remarks) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($ofac->addressList->address->address1) or isset($ofac->addressList->address->address2) or isset($ofac->addressList->address->address3) or isset($ofac->addressList->address->city) or isset($ofac->addressList->address->postalCode) or isset($ofac->addressList->address->country)) ? $ofac->addressList->address->address1.', '.$ofac->addressList->address->address2.' '.$ofac->addressList->address->address3.' '.$ofac->addressList->address->city.' '.$ofac->addressList->address->postalCode.' '.$ofac->addressList->address->country : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->nationalityList->nationality->country) ? strtoupper($ofac->nationalityList->nationality->country) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->dateOfBirthList->dateOfBirthItem->dateOfBirth) ? $ofac->dateOfBirthList->dateOfBirthItem->dateOfBirth : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($ofac->placeOfBirthList->placeOfBirthItem->placeOfBirth) ? strtoupper($ofac->placeOfBirthList->placeOfBirthItem->placeOfBirth) : NULL)),'"')."'
                                          )"
                                        );
                                    }

                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'SUSP_TERORIS':
                                $columnTable = '
                                      "id_teroris",
                                      "version_number",
                                      "nama",
                                      "first_name",
                                      "second_name",
                                      "un_list_type",
                                      "reference_name",
                                      "listed_on",
                                      "narrative_1",
                                      "nationality",
                                      "list_type",
                                      "last_day_updated",
                                      "alias_1",
                                      "address",
                                      "tempat_tgl_lahir",
                                      "birth_date_2",
                                      "birth_date_place",
                                      "document_1",
                                      "document_2",
                                      "document_3",
                                      "document_4",
                                      "sort_key",
                                      "sort_key_last_mod"
                                ';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{

                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    $xml_file = $target_file.$fileName;
                                    $terrorist_file = simplexml_load_file($xml_file);
                                    
                                    $objApuParameterInd = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_TER_IND');
                                    $insertInd = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging) values ('".$fileName."',".$objApuParameterInd->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                    $selectIDInd = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                    $fetchInd = pg_fetch_row($selectIDInd);
                                    $lastIDInd = $fetchInd[0];

                                    foreach($terrorist_file->INDIVIDUALS->INDIVIDUAL as $trsInd)
                                    {
                                        pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->DATAID) ? $trsInd->DATAID : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->VERSIONNUM) ? strtoupper($trsInd->VERSIONNUM) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($trsInd->FIRST_NAME) or isset($trsInd->SECOND_NAME)) ? strtoupper($trsInd->FIRST_NAME.' '.$trsInd->SECOND_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->FIRST_NAME) ? strtoupper($trsInd->FIRST_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->SECOND_NAME) ? strtoupper($trsInd->SECOND_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->UN_LIST_TYPE) ? $trsInd->UN_LIST_TYPE : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->REFERENCE_NUMBER)) ? $trsInd->REFERENCE_NUMBER : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->LISTED_ON)) ? $trsInd->LISTED_ON : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->COMMENTS1)) ? strtoupper($trsInd->COMMENTS1) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->NATIONALITY->VALUE)) ? strtoupper($trsInd->NATIONALITY->VALUE) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->LIST_TYPE)) ? $trsInd->LIST_TYPE : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->INDIVIDUAL_ALIAS->ALIAS_NAME)) ? strtoupper($trsInd->INDIVIDUAL_ALIAS->ALIAS_NAME) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->INDIVIDUAL_DATE_OF_BIRTH->DATE)) ? strtoupper($trsInd->INDIVIDUAL_DATE_OF_BIRTH->DATE) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->INDIVIDUAL_PLACE_OF_BIRTH->COUNTRY)) ? strtoupper($trsInd->INDIVIDUAL_PLACE_OF_BIRTH->COUNTRY) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($trsInd->INDIVIDUAL_DOCUMENT->TYPE_OF_DOCUMENT) or isset($trsInd->INDIVIDUAL_DOCUMENT->NUMBER)) ? strtoupper($trsInd->INDIVIDUAL_DOCUMENT->TYPE_OF_DOCUMENT.' '.$trsInd->INDIVIDUAL_DOCUMENT->NUMBER) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->SORT_KEY)) ? $trsInd->SORT_KEY : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsInd->SORT_KEY_LAST_MOD)) ? $trsInd->SORT_KEY_LAST_MOD : NULL),'"')."'
                                          )"
                                        );
                                    }

                                    pg_query("update detail_upload_suspect set upload_id=".$lastIDInd.",parameter_id=".$objApuParameterInd->ParameterId." where upload_id is null;");

                                    $objApuParameterCorp = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_TER_CORP');
                                    $insertCorp = pg_query("insert into header_upload_suspect(filename,parameter_id,upload_status,screening_status,created_date,created_by,url_staging)
                                     values ('".$fileName."',".$objApuParameterCorp->ParameterId.",TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                    $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                    $fetchCorp = pg_fetch_row($selectID);
                                    $lastIDCorp = $fetchCorp[0];

                                    foreach($terrorist_file->ENTITIES->ENTITY as $trsCorp)
                                    {
                                        pg_query("INSERT INTO detail_upload_suspect(".$columnTable.") VALUES (
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->DATAID) ? $trsCorp->DATAID : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->VERSIONNUM) ? strtoupper($trsCorp->VERSIONNUM) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode((isset($trsInd->FIRST_NAME) or isset($trsInd->SECOND_NAME)) ? strtoupper($trsInd->FIRST_NAME.' '.$trsInd->SECOND_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->FIRST_NAME) ? strtoupper($trsCorp->FIRST_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->SECOND_NAME) ? strtoupper($trsCorp->SECOND_NAME) : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->UN_LIST_TYPE) ? $trsCorp->UN_LIST_TYPE : NULL)),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->REFERENCE_NUMBER)) ? $trsCorp->REFERENCE_NUMBER : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->LISTED_ON)) ? $trsCorp->LISTED_ON : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->COMMENTS1)) ? strtoupper($trsCorp->COMMENTS1) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->NATIONALITY->VALUE)) ? strtoupper($trsCorp->NATIONALITY->VALUE) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->LIST_TYPE)) ? $trsCorp->LIST_TYPE : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->ENTITY_ALIAS->ALIAS_NAME)) ? strtoupper($trsCorp->ENTITY_ALIAS->ALIAS_NAME) : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode('')),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->SORT_KEY)) ? $trsCorp->SORT_KEY : NULL),'"')."',
                                            '".trim(pg_escape_string(utf8_encode(isset($trsCorp->SORT_KEY_LAST_MOD)) ? $trsCorp->SORT_KEY_LAST_MOD : NULL),'"')."'
                                          )"
                                        );
                                    }

                                    pg_query("update detail_upload_suspect set upload_id=".$lastIDCorp.",parameter_id=".$objApuParameterCorp->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
                                break;
                            case 'TERORIS_INDIVIDU_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_TER_IND');
                                $insert = pg_query("insert into header_upload_suspect(filename,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].",'../service/findCustomerSuspect.php');");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '(
                                      "id_teroris",
                                      "version_number",
                                      "first_name",
                                      "second_name",
                                      "un_list_type",
                                      "reference_name",
                                      "listed_on",
                                      "narrative_1",
                                      "nationality",
                                      "list_type",
                                      "last_day_updated",
                                      "alias_1",
                                      "alias_2",
                                      "alias_3",
                                      "alias_4",
                                      "alias_5",
                                      "alias_6",
                                      "alias_7",
                                      "alias_8",
                                      "alias_9",
                                      "alias_10",
                                      "alias_11",
                                      "alias_12",
                                      "alias_13",
                                      "alias_14",
                                      "alias_15",
                                      "alias_16",
                                      "alias_17",
                                      "address",
                                      "birth_date_1",
                                      "birth_date_2",
                                      "birth_date_place",
                                      "document_1",
                                      "document_2",
                                      "document_3",
                                      "document_4",
                                      "sort_key",
                                      "sort_key_last_mod"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    pg_query("COPY detail_upload_suspect".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }
                                QApplication::DisplayAlert("Upload Success");
//                                note : di csv ganti delimiter jangan menggunakan tanda koma(,)
                                break;

                            case 'TERORIS_CORPORATE_CSV':
                                $objApuParameter = ApuParameter::LoadByParamTypeAndCode('SUSPECT_TYPE','SUSP_TER_CORP');
                                $insert = pg_query("insert into header_upload_suspect(filename,upload_status,screening_status,created_date,created_by,url_staging)
                                 values ('".$fileName."',TRUE,FALSE,'".date('Y-m-d')."',". $_SESSION[__USER_LOGIN__].");");
                                $selectID = pg_query("select currval('header_upload_suspect_upload_id_seq');");
                                $fetch = pg_fetch_row($selectID);
                                $lastID = $fetch[0];

                                $columnTable = '(
                                      "id_teroris",
                                      "version_number",
                                      "first_name",
                                      "un_list_type",
                                      "reference_name",
                                      "listed_on",
                                      "narrative_1",
                                      "nationality",
                                      "list_type",
                                      "last_day_updated",
                                      "alias_1",
                                      "alias_2",
                                      "alias_3",
                                      "alias_4",
                                      "alias_5",
                                      "alias_6",
                                      "alias_7",
                                      "alias_8",
                                      "alias_9",
                                      "alias_10",
                                      "alias_11",
                                      "alias_12",
                                      "alias_13",
                                      "alias_14",
                                      "alias_15",
                                      "alias_16",
                                      "address",
                                      "sort_key",
                                      "sort_key_last_mod"
                                )';

                                $target_file = __DOCROOT__.__SOURCE__."/upload/file_csv/";
                                if(!file_exists($target_file)){
                                    mkdir($target_file, 777);
                                }else{
                                    copy($this->txtFile->File, $target_file . $fileName);
                                    chmod($target_file.$fileName, 0777);
                                    pg_query("COPY detail_upload_suspect".$columnTable." FROM '".$target_file.$fileName."' WITH DELIMITER '".$this->lstDelimit->SelectedValue."'");
                                    pg_query("update detail_upload_suspect set upload_id=".$lastID.",parameter_id=".$objApuParameter->ParameterId." where upload_id is null;");
                                    unlink($target_file.$fileName);
                                }

                                # AUDIT TRAIL !
                                $log = new LogActivity();
                                $log->SaveUploadSuspect();
                                QApplication::DisplayAlert("Upload Success");
                                break;

                            default:
                                QApplication::DisplayAlert("Sorry Import Failed, Unauthorized file !");
                                break;
                        }
                    }
                }
            }
        }catch (Exception $e){
            QApplication::DisplayAlert($e->getMessage());
        }
    }
}
UploadForm::Run('UploadForm', 'form.tpl.php');
?>