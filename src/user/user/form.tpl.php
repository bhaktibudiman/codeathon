<?php
$strPageTitle = QApplication::Translate('Users') . ' - ' . $this->mctUsers->TitleVerb;
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3>.:: Form User ::.</h3>
                </div>
                <hr/>

                <div class="box-body">
                    <div class="form-controls">

                        <div class="form-group">
                            <?php $this->txtNik->RenderWithBootstrapInGroup(2,4); ?>
                        </div>

                        <div class="form-group">
                            <?php $this->txtEmployeeName->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtPositionName->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtPhoneNumber->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtEmailAddress->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtUsername->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->chkIsUserActive->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->rdUserType->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtRoleId->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtBranchCode->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtRoleName->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtRoleLevel->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                    </div>
                <div class="modal-footer">
                    <?php $this->btnSave->Render(); ?>
                    <?php $this->btnCancel->Render(); ?>
                    <?php $this->btnDelete->Render(); ?>
                    <?php $this->objDefaultWaitIcon->Render(); ?>
                    <br/>
                    <div style ='font:11px/21px Arial,tahoma,sans-serif;color:#ff0000'>
                        <i>
                            <b>
                                <?php $this->lblUsernameSame->Render(); ?>
                            </b>
                        </i>
                    </div>
                </div>
                    <div style ='font:11px/21px Arial,tahoma,sans-serif;color:#ff0000'>
                        <h6>
                            <i>
                                * Data Mandatory Yang Harus Terisi !
                            </i>
                        </h6>
                    </div>
                <div style ='font:11px/21px Arial,tahoma,sans-serif;color:#ff0000'>
                    <h6>
                        <i>
                            *) Untuk Role, Setelah Pilih Role Klik Harap ENTER !
                        </i>
                    </h6>
                </div>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>