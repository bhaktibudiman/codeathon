<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

    <section class="content-header">
        <h1>..:: User List ::..</h1>
    </section>
    <div class="box box-primary">
        <div class="box-body">
            <div>
                <div class="col-md-2">
                    <label>Param 1</label>
                    <?php
                    $this->lstParam1->Render();
                    ?>
                </div>
                <div class="col-md-2">
                    <label>Pencarian</label>
                    <?php
                    $this->lstAutoComplete->Render();
                    $this->textSearch1->Render();
                    // $this->datePicker1->Input->Render();
                    ?>
                </div>

                <div class="col-md-2">
                    <label>Param 2</label>
                    <?php
                    $this->lstParam2->Render();
                    ?>
                </div>
                <div class="col-md-2">
                    <label>Pencarian</label>
                    <?php
                    $this->lstAutoComplete2->Render();
                    $this->textSearch2->Render();
                    // $this->datePicker2->Input->Render();
                    // $this->datePicker1->Render();
                    // $this->datePicker2->Render();
                    ?>
                </div>
                <div class="col-md-1">
                    <label>.</label>
                    <a href="form.php" class="btn btn-success"><span class="fa fa-plus"> Create New</span></a>
                    <?php
                    $this->btnFilter->Render();
                    $this->objDefaultWaitIcon->Render();
                    ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php $this->dtgUserses->Render(); ?>
        </div>
    </div>



<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>