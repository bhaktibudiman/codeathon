<?php
$strPageTitle = QApplication::Translate('Users') . ' - ' . $this->mctUsers->TitleVerb;
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3><?php _p($this->mctUsers->TitleVerb); ?> <?php _t('Users')?></h3>
                </div>

                <div class="box-body">
                    <div class="form-controls">

                        <div class="form-group">
                            <?php $this->txtUpdateNIK->RenderWithBootstrapInGroup(2,4); ?>
                        </div>


                        <div class="form-group">
                            <?php $this->txtUpdateEmployeeName->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtUpdatePositionName->RenderWithBootstrapInGroup(2,4); ?>
                        </div>

                        <div class="form-group">
                            <?php $this->txtUpdatePhoneNumber->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtUpdateEmailAddress->RenderWithBootstrapInGroup(2,4); ?>
                        </div>

                        <div class="form-group">
                            <?php $this->txtUpdateUsername->RenderWithBootstrapInGroup(2,4); ?>
<!--                            --><?php //$this->lstRole->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                    </div>

                <div class="modal-footer">
                    <?php
                    $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
                    if($objUser->Role->RoleName != 'ADMIN_IT' ) {?>
                        <?php $this->btnApprove->Render(); ?>
                        <?php $this->btnDelete->Render(); ?>
                    <?php }else{?>
                    <?php $this->btnSave->Render (); }?>
                    <?php $this->btnCancel->Render(); ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>