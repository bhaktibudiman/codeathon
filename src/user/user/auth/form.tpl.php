<?php
$strPageTitle = QApplication::Translate('Users') . ' - ' . $this->mctUsers->TitleVerb;
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-md-10">
            <div class="box">
                <div class="box-header">
                    <h3>.:: Form User ::.</h3>
                </div>
                <hr>
                <div class="modal-header">
                    <?php $this->btnSave->Render(); ?>
<!--                    --><?php //$this->btnCancel->Render(); ?>
                    <?php $this->btnDelete->Render(); ?>
                    <?php $this->objDefaultWaitIcon->Render(); ?>
                </div>

                <div class="box-body">
                    <div class="form-controls">

                        <div class="form-group">
                            <?php $this->txtNik->RenderWithBootstrapInGroup(2,4); ?>
                        </div>

                        <div class="form-group">
                            <?php $this->txtEmployeeName->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtPositionName->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtPhoneNumber->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtEmailAddress->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtUsername->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->chkIsUserActive->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->rdUserType->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtRoleId->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtBranchCode->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                        <div class="form-group">
                            <?php $this->txtRoleName->RenderWithBootstrapInGroup(2,4); ?>
                            <?php $this->txtRoleLevel->RenderWithBootstrapInGroup(2,4); ?>
                        </div>
                    </div>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>