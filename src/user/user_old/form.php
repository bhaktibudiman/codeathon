<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');
class UsersEditForm extends QForm {
    protected $mctUsers;

    // Controls for Users's Data Fields
    protected $txtUserId;
    protected $txtNik;
    protected $txtEmployeeName;
    protected $txtPhoneNumber;
    protected $txtEmailAddress;
    protected $txtPositionName;
    protected $txtUsername;
    protected $txtPassword;
    protected $txtRoleId;
    protected $txtRoleName;
    protected $txtRoleLevel;
    protected $chkIsUserActive;
    protected $txtBranchCode;
    protected $txtBranchName;
    protected $txtLinkImage;
    protected $calLastLogin;
    protected $txtExpiredCount;
    protected $txtLockedCount;
    protected $chkIsLocked;
    protected $txtLockedSession;
    protected $txtCreatedBy;
    protected $txtCreatedUser;
    protected $calLastUpdate;
    protected $txtRecordStatus;
    protected $txtRecordStatusName;
    protected $chkIsLoggedIn;
    protected $lblUsernameSame;
    protected $lblPwdWarning;
    protected $txtCountExpired;
    protected $rdUserType;
    protected $txtEmail;
    protected $txtUserType;


    protected $btnSave;
    protected $btnDelete;
    protected $btnCancel;

    protected $txtLongPassword;
    protected $txtNewPassword;
    protected $txtConfirmPassword;
    protected $btnSavePassword;
    protected $btnChangePassword;

    protected $lstRoleId;
    protected $lstBranchCode;

    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();

        $this->mctUsers = UsersMetaControl::CreateFromPathInfo($this);
        $this->objDefaultWaitIcon = new QWaitIcon($this);

        $this->txtUserId = $this->mctUsers->txtUserId_Create();

        $this->chkIsUserActive = $this->mctUsers->chkIsUserActive_Create();
        $this->chkIsUserActive->Name = 'Status User ID';

        $this->txtEmployeeName = $this->mctUsers->txtEmployeeName_Create();
        $this->txtEmployeeName->Name = 'Nama User *';

        $this->txtNik = $this->mctUsers->txtNik_Create();
        $this->txtNik->Name = 'NIK *';
        $this->txtNik->MinLength = 8;
        $this->txtNik->MaxLength = 8;

        $this->txtPhoneNumber = $this->mctUsers->txtPhoneNumber_Create();
        $this->txtPhoneNumber->Name = 'Telepon *';
        $this->txtPhoneNumber->Placeholder = ' - - Numeric Only - -';

        $this->txtEmailAddress  = $this->mctUsers->txtEmailAddress_Create();
        $this->txtEmailAddress->Name = 'Email *';
        $this->txtEmailAddress->Placeholder = 'example@banksampoerna.com';

        $this->txtPositionName = $this->mctUsers->txtPositionName_Create();
        $this->txtPositionName->Name = 'Posisi *';

        $this->txtLinkImage = $this->mctUsers->txtLinkImage_Create();

        $this->txtUsername = $this->mctUsers->txtUsername_Create();
        $this->txtUsername->Name = 'Username *';

        $this->txtBranchCode = $this->mctUsers->lstBranch_Create();
        $this->txtBranchCode->Name = 'Cabang *';
        $this->txtBranchCode->AddItem('- Kantor Pusat -',null);
        if($arrBranch = BankBranch::LoadAll()){
            foreach($arrBranch as $bankBranch){
                $this->txtBranchCode->AddItem($bankBranch->BranchCode." - ".$bankBranch->BranchName, $bankBranch->BranchCode);
            }
        }

        $this->txtBranchCode->AddAction(new QClickEvent(), new QAjaxAction('Branch_Click'));
        $this->txtBranchName = $this->mctUsers->txtBranchName_Create();
        $this->txtBranchName->Enabled = false;

        if($this->mctUsers->EditMode){
            $objUser = Users::Load(QApplication::PathInfo(0));
            $this->txtBranchCode->SelectedValue = $objUser->BranchCode;
            if($objUser->BranchCode == NULL){
                $this->txtBranchCode->SelectedIndex = 0;
            }
        }

        $this->txtRoleId = $this->mctUsers->lstRoleId_Create();
        $this->txtRoleId->Name = 'Role *';
        $this->txtRoleId->AddItem('- Pilih Role User -');
        if($arrRole = RoleUser::LoadAll()){
            foreach($arrRole as $role){
                $this->txtRoleId->AddItem($role->RoleLevel." - ".$role->RoleName, $role->RoleId);
            }
        }
        $this->txtRoleId->ActionParameter = 'role';
        $this->txtRoleId->AddAction(new QEnterKeyEvent(), new QAjaxAction('RoleName_Click'));
        $this->txtRoleName = $this->mctUsers->txtRoleName_Create();
        $this->txtRoleName->Enabled = false;
        $this->txtRoleLevel = $this->mctUsers->txtRoleLevel_Create();
        $this->txtRoleLevel->Enabled = false;

        $this->txtUserType = $this->mctUsers->txtUserType_Create();
        $this->rdUserType = new QRadioButtonList($this);
        $this->rdUserType->Name = 'Tipe User *';
        $this->rdUserType->AddItem('Pusat',1);
        $this->rdUserType->AddItem('Cabang',2);
        $this->rdUserType->ActionParameter = 'rdUserType';
        $this->rdUserType->AddAction(new QClickEvent(), new QAjaxAction('lstUserType_Click'));


        if($this->mctUsers->EditMode){
            $objUser = Users::Load(QApplication::PathInfo(0));
            $this->txtRoleId->SelectedValue = $objUser->RoleId;
            $this->rdUserType->SelectedValue = $objUser->UserType;
            $this->txtRoleName->Text = $objUser->RoleName;
            $this->txtRoleLevel->Text = $objUser->RoleLevel;
            $this->txtBranchCode->SelectedValue = $objUser->BranchCode;
            $this->txtBranchName->Text = $objUser->BranchName;
        }

        $this->txtLongPassword = new QTextBox($this);
        $this->txtLongPassword->Name = 'Password Sekarang';
        $this->txtLongPassword->Enabled = false;
        $this->txtLongPassword->TextMode = QTextMode::Password;
        $this->txtLongPassword->Required = true;

        $this->txtNewPassword = new QTextBox($this);
        $this->txtNewPassword->Name = 'Password Baru';
        $this->txtNewPassword->Enabled = false;
        $this->txtNewPassword->TextMode = QTextMode::Password;
        $this->txtNewPassword->Required = true;

        $this->txtConfirmPassword = new QTextBox($this);
        $this->txtConfirmPassword->Name = 'Konfirmasi Password';
        $this->txtConfirmPassword->Enabled = false;
        $this->txtConfirmPassword->TextMode = QTextMode::Password;
        $this->txtConfirmPassword->Required = true;

        // Create Buttons and Actions on this Form
        $this->btnSave = new QButton($this);
        $this->btnSave->Text = QApplication::Translate('Simpan');
        $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
        $this->btnSave->CausesValidation = true;
        $this->btnSave->CssClass = 'btn btn-success';

        $this->btnCancel = new QButton($this);
        $this->btnCancel->Text = QApplication::Translate('Batal');
        $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
        $this->btnCancel->CssClass = 'btn btn-warning';

        $this->btnDelete = new QButton($this);
        $this->btnDelete->Text = QApplication::Translate('Hapus');
        $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE want to DELETE this %s?'), QApplication::Translate('Users'))));
        $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
        $this->btnDelete->Visible = false;
        $this->btnDelete->CssClass = 'btn btn-danger';

        $this->btnChangePassword = new QButton($this);
        $this->btnChangePassword->Text = QApplication::Translate('Change Password');
        $this->btnChangePassword->AddAction(new QClickEvent(), new QAjaxAction('btnChange_Click'));
        $this->btnChangePassword->CssClass = 'btn btn-primary';

        $this->btnSavePassword = new QButton($this);
        $this->btnSavePassword->Text = QApplication::Translate('Save New Password');
        $this->btnSavePassword->AddAction(new QClickEvent(), new QAjaxAction('btnSavePassword_Click'));
        $this->btnSavePassword->CausesValidation = true;
        $this->btnSavePassword->CssClass = 'btn btn-success';
        $this->btnSavePassword->Visible = false;

        $this->lblUsernameSame = new QLabel($this);
        $this->lblUsernameSame->Text = ' *) Periksa kembali Username (Alpha Numeric) atau NIK sudah tersedia ! Terimakasih';
        $this->lblUsernameSame->Visible = false;

        $this->lblPwdWarning = new QLabel($this);
        $this->lblPwdWarning->Text = 'Password should be a series of alphabetic, numeric , uppercase , lowercase and characters special !';
        $this->lblPwdWarning->Visible = false;

        if($this->mctUsers->EditMode){
            # FIELD DATA MANDATORY !
            $this->txtUsername->Enabled = false;
            $this->txtEmployeeName->Required = true;
            $this->txtNik->Required = true;
            $this->txtPhoneNumber->Required = true;
            $this->txtEmailAddress->Required = true;
            $this->txtPositionName->Required = true;
            $this->txtUsername->Required = true;
            $this->rdUserType->Required = true;
            if($this->rdUserType->SelectedIndex == 0){
                $this->txtBranchCode->Required = false;
            }
            $this->txtRoleName->Required = true;
            $this->txtRoleLevel->Required = true;

            # VAR DISABLE !
            $objParam = Users::Load(QApplication::PathInfo(0));
            $this->txtNik->Enabled = false;
            $this->txtEmployeeName->Enabled = false;
            $this->txtPhoneNumber->Enabled = false;
            $this->txtPositionName->Enabled = false;
            $this->txtEmailAddress->Enabled = false;
            $this->txtUsername->Enabled = false;
            $this->chkIsUserActive->Enabled = false;
            $this->rdUserType->Enabled = false;
            $this->txtRoleId->Enabled = false;
            $this->txtRoleName->Enabled = false;
            $this->txtRoleLevel->Enabled = false;
            $this->txtBranchCode->Enabled = false;
            $this->txtBranchName->Enabled = false;

            # CHECK STATUS AUTHOR !
            if($objParam->RecordStatus == 'U' || $objParam->RecordStatus == 'D' || $objParam->RecordStatus == 'C'){
                $this->btnSave->Hide();
                $this->btnDelete->Hide();
                $this->rdUserType->Hide();
            }else{
                $this->txtNik->Enabled = true;
                $this->txtEmployeeName->Enabled = true;
                $this->txtPhoneNumber->Enabled = true;
                $this->txtPositionName->Enabled = true;
                $this->txtEmailAddress->Enabled = true;
                $this->txtUsername->Enabled = true;
                $this->chkIsUserActive->Enabled = true;
                $this->rdUserType->Enabled = true;
                $this->txtRoleId->Enabled = true;
                $this->txtBranchCode->Enabled = true;
                $this->txtBranchName->Enabled = true;
                $this->btnSave->UnHide();
                $this->btnDelete->UnHide();
            }
        }else{
            $this->txtEmployeeName->Required = true;
            $this->txtNik->Required = true;
            $this->txtPhoneNumber->Required = true;
            $this->txtEmailAddress->Required = true;
            $this->txtPositionName->Required = true;
            $this->txtUsername->Required = true;
            $this->rdUserType->Required = true;
            $this->txtRoleName->Required = true;
            $this->txtRoleLevel->Required = true;
            if($this->rdUserType->SelectedIndex == 0){
                $this->txtBranchCode->Required = false;
            }
            $this->chkIsUserActive->Hide();
        }
    }

    protected function lstUserType_Click($strFormId, $strControlId, $strParameter) {
        if($this->rdUserType->SelectedIndex == 0){
            $objBranch = new Users();
            $this->txtRoleId->RemoveAllItems();
            $this->txtRoleId->AddItem('- Pilih Role User -');
                $roleUser = RoleUser::LoadRolePusat(2);
                foreach($roleUser as $role){
                    $this->txtRoleId->AddItem($role->RoleLevel." - ".$role->RoleName, $role->RoleId);
                }


            $this->txtBranchCode->RemoveAllItems();
            $this->txtBranchCode->AddItem('- Kantor Pusat -',null);
            if($objBranch->BranchCode){
                $param = BankBranch::Load(null);
                foreach($param as $bankBranch) {
                    $this->txtBranchCode->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                }
            }

        }else{
            $this->txtRoleId->RemoveAllItems();
            $this->txtRoleId->AddItem('- Pilih Role User -');
            $roleUser = RoleUser::LoadRolePusat(1);
            foreach($roleUser as $role){
                $this->txtRoleId->AddItem($role->RoleLevel." - ".$role->RoleName, $role->RoleId);
            }

            if($arrBranch = BankBranch::LoadAll()){
                $this->txtBranchCode->RemoveAllItems();
                $this->txtBranchCode->AddItem('- Pilih Cabang -');
                foreach($arrBranch as $bankBranch){
                    $this->txtBranchCode->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                }
            }
        }
    }

    protected function RoleName_Click($strFormId, $strControlId, $strParameter) {
        $param = $this->txtRoleId->SelectedValue;
        $role = RoleUser::Load($param);
        if(isset($role)){
            $this->txtRoleName->Text = $role->RoleName;
            $this->txtRoleLevel->Text = $role->RoleLevel;
        }
    }

    protected function Branch_Click($strFormId, $strControlId, $strParameter) {
        $param = $this->txtBranchCode->SelectedValue;
        $branch = BankBranch::Load($param);
        if($branch){
            $this->txtBranchName->Text = $branch->BranchName;
        }
    }

    protected function Form_Validate() {
        $blnToReturn = parent::Form_Validate();

        // Custom Validation Rules
        // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            // Set Focus to the top-most invalid control
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            $objControl->Blink();
        }

        # VAL IS NUMBER IN NIK & PHONE !
        if(!is_numeric($this->txtNik->Text)){
            $this->txtNik->Focus();
            $this->txtNik->blink();
            $blnToReturn = false;
        }elseif(!is_numeric($this->txtPhoneNumber->Text)){
            $this->txtPhoneNumber->Focus();
            $this->txtPhoneNumber->blink();
            $blnToReturn = false;
        }

        # VAL CHECK LENGTH NIK !
        if (strlen($this->txtNik->Text) < 8)
        {
            $this->txtNik->Focus();
            $this->txtNik->blink();
            $blnToReturn = false;
        }

        # VAL EMAIL !
        if ($this->txtEmailAddress != "")
        {
            if ( !preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", $this->txtEmailAddress->Text) )
            {
                $this->txtEmailAddress->Focus();
                return false;
            }
        }

        # VAL USERNAME IS ALPHANUMERIC !
        if ($this->txtUsername != "")
        {
            if (!preg_match("/^[\w]+$/", $this->txtUsername->Text) )
            {
                $this->lblUsernameSame->Visible = true;
                $blnToReturn = false;
                return false;
            }else{
                $this->lblUsernameSame->Visible = false;
            }
        }

        if(!$this->mctUsers->EditMode){
            # VAL USERNAME IS SAME !
            if(!$this->mctUsers->EditMode){
                $user = Users::loadByUserName($this->txtUsername->Text);
                if($this->txtUsername->Text!='' && isset($user)){
                    $this->lblUsernameSame->Visible = true;
                    $blnToReturn = false;
                }else{
                    $this->lblUsernameSame->Visible = false;
                }
            }

            # VAL NIK IS SAME !
            $valNik = Users::loadByNik($this->txtNik->Text);
            if (isset($valNik)) {
                $this->lblUsernameSame->Visible = true;
                $blnToReturn = false;
            }
        }

        return $blnToReturn;
    }

    protected function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    protected function btnSave_Click($strFormId, $strControlId, $strParameter) {

        $error = true;
        try{
            if($this->mctUsers->EditMode){
                $paramEdit = new AuthUsers();
//                $paramEdit->UserIdAuth  = System::GetId();
                $paramEdit->UserId  = $this->txtUserId->Text;
                $paramEdit->Nik = $this->txtNik->Text;
                $paramEdit->EmployeeName = $this->txtEmployeeName->Text;
                $paramEdit->PhoneNumber = $this->txtPhoneNumber->Text;
                $paramEdit->EmailAddress = $this->txtEmailAddress->Text;
                $paramEdit->PositionName = $this->txtPositionName->Text ;
                $paramEdit->Username = $this->txtUsername->Text;
                $paramEdit->RoleId = $this->txtRoleId->SelectedValue;
                $paramEdit->RoleName = $this->txtRoleName->Text ;
                $paramEdit->RoleLevel = $this->txtRoleLevel->Text;
                $paramEdit->RoleLevel = $this->txtRoleLevel->Text;
                $paramEdit->UserType = $this->rdUserType->SelectedValue;
                $paramEdit->BranchCode = $this->txtBranchCode->SelectedValue;
                $paramEdit->BranchName = $this->txtBranchName->Text;
                $paramEdit->IsUserActive = $this->chkIsUserActive->Checked;
                $paramEdit->LastUpdate = QDateTime::Now();
                $paramEdit->RecordStatus = 'U';
                $paramEdit->RecordStatusName = 'UPDATED AUTHOR';
                $paramEdit->Save();
                $error = false;
            }else{
                $paramEdit = new AuthUsers();
//                $paramEdit->UserIdAuth  = System::GetId();
                $paramEdit->UserId  = $this->txtUserId->Text = System::GetId();
                $paramEdit->Nik = $this->txtNik->Text;
                $paramEdit->EmployeeName = $this->txtEmployeeName->Text;
                $paramEdit->PhoneNumber = $this->txtPhoneNumber->Text;
                $paramEdit->EmailAddress = $this->txtEmailAddress->Text;
                $paramEdit->PositionName = $this->txtPositionName->Text ;
                $paramEdit->Username = $this->txtUsername->Text;
                $paramEdit->RoleId = $this->txtRoleId->SelectedValue ;
                $paramEdit->RoleName = $this->txtRoleName->Text ;
                $paramEdit->RoleLevel = $this->txtRoleLevel->Text;
                $paramEdit->UserType = $this->rdUserType->SelectedValue;
                $paramEdit->BranchCode = $this->txtBranchCode->SelectedValue;
                $paramEdit->BranchName = $this->txtBranchName->Text;
                $paramEdit->IsUserActive = false;
                $paramEdit->LastUpdate = QDateTime::Now();
                $paramEdit->RecordStatus = 'C';
                $paramEdit->RecordStatusName = 'CREATED AUTHOR';
                $paramEdit->Save();
                $error = false;
            }

        }catch (Exception $e){
            $error = true;
            throw $e;
        }

        if(!$error){
            QApplication::DisplayAlert('Author Proses');
            if($this->mctUsers->EditMode){
                /*---------- Update Record Status If Action Edit Mode ------------*/
                $IsUpdated = Users::Load(QApplication::PathInfo(0));
                $IsUpdated->LastUpdate = QDateTime::Now();
                $IsUpdated->RecordStatus = 'U';
                $IsUpdated->RecordStatusName = 'UPDATED AUTHOR';
                $IsUpdated->LinkImage = 'avatar5.png';
                $IsUpdated->Update();
//				$log = new LogActivity();
//				$log->UpdatedMaster();
            }else{
                /*---------- Save Parameter First ------------*/
                $objUsers = Users::Load($_SESSION[__USER_LOGIN__]);
                $this->txtUserId = $this->mctUsers->txtUserId_Create();
                $this->txtUserId->Text = System::GetId();
                $this->txtCreatedBy = $this->mctUsers->txtCreatedBy_Create();
                $this->txtCreatedBy->Text = $_SESSION[__USER_LOGIN__];
                $this->txtCreatedUser = $this->mctUsers->txtCreatedUser_Create();
                $this->txtCreatedUser->Text = $objUsers->Username;
                $this->calLastUpdate = $this->mctUsers->calLastUpdate_Create();
                $this->calLastUpdate->DateTime = QDateTime::Now();
                $this->chkIsLocked = $this->mctUsers->chkIsLocked_Create();
                $this->chkIsLocked->Checked = true;
                $this->txtUserType = $this->mctUsers->txtUserType_Create();
                $this->txtUserType->Text = $this->rdUserType->SelectedValue;
                $this->txtRecordStatus = $this->mctUsers->txtRecordStatus_Create();
                $this->txtRecordStatus->Text = 'C';
                $this->txtRecordStatusName = $this->mctUsers->txtRecordStatusName_Create();
                $this->txtRecordStatusName->Text = 'CREATED AUTHOR';
                $this->txtLinkImage = $this->mctUsers->txtLinkImage_Create();
                $this->txtLinkImage->Text = 'avatar5.png';
//				$log = new LogActivity();
//				$log->CreatedMaster();
                $this->mctUsers->SaveUsers();
            }
            $this->RedirectToListPage();
        }

    }

    protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
        // Delegate "Delete" processing to the UsersMetaControl
        $this->mctUsers->DeleteUsers();
        $this->RedirectToListPage();
    }

    protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
        $this->RedirectToListPage();
    }

    protected function RedirectToListPage() {
        QApplication::Redirect(__SOURCE__ . '/user/user/list.php');
    }

    protected function btnChange_Click(){
        $this->txtNewPassword->Enabled = true;
        $this->txtNewPassword->Required = true;
        $this->txtNewPassword->Text = '';

        $this->txtLongPassword->Enabled = true;
        $this->txtLongPassword->Required = true;
        $this->txtLongPassword->Text = '';

        $this->txtConfirmPassword->Enabled = true;
        $this->txtConfirmPassword->Required = true;
        $this->txtConfirmPassword->Text = '';

        $this->btnSavePassword->Visible = true;
    }

    protected function btnSavePassword_Click(){
        $objUser = Users::Load($this->txtUserId->Text);
        $error = true;
        $db = $objUser->GetDatabase();
        $db->TransactionBegin();

        try{
            if($this->txtLongPassword->Text){

                if(System::getHash($this->txtLongPassword->Text) != $objUser->Password){
                    QApplication::DisplayAlert('Your Password is Wrong');
                    $error = true;
                }else if($this->txtNewPassword->Text != $this->txtConfirmPassword->Text){
                    QApplication::DisplayAlert('Confirmation Password and New Password are not Same');
                    $error = true;
                }else if(!preg_match("/^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]+).*$/", $this->txtNewPassword->Text)) {
                    $this->lblPwdWarning->Visible = true;
                }else{
                    $this->lblPwdWarning->Visible = false;
                    $objUser->Password = System::getHash($this->txtNewPassword->Text);
                    $objUser->LockedCount = 0;
                    $objUser->ExpiredCount = 0;
                    $objUser->Save();
                    $error = false;
                }
            }
        }catch (Exception $e){
            $error = true;
            $db->TransactionRollBack();
            throw $e;
        }

        if(!$error){
            QApplication::DisplayAlert('Save Password Success');
            $db->TransactionCommit();

            $this->txtNewPassword->Enabled = false;
            $this->txtNewPassword->Required = false;

            $this->txtLongPassword->Enabled = false;
            $this->txtLongPassword->Required = false;

            $this->txtConfirmPassword->Enabled = false;
            $this->txtConfirmPassword->Required = false;

            $this->btnSavePassword->Visible = false;
        }
    }
}
UsersEditForm::Run('UsersEditForm');
?>