<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

<section class="content-header">
    <h1>.:: List User ::.</h1>
</section>

<div class="box box-primary">
    <div class="box-body">
        <div class="col-xs-6">
            <a href="form.php" class="btn btn-success"><span class="fa fa-plus"> Tambah User </span></a>
        </div>
        <div>
            <br/><br>
            <hr/>

            <div class="col-md-2"">
            <label>Param 1</label>
            <?php
            $this->lstParam1->Render();
            ?>
        </div>
        <div class="col-md-3">
            <label>Pencarian</label>
            <?php
            $this->lstAutoComplete->Render();
            $this->textSearch1->Render();
            ?>
        </div>

        <div class="col-md-2">
            <label>Param 2</label>
            <?php
            $this->lstParam2->Render();
            ?>
        </div>
        <div class="col-md-3">
            <label>Pencarian</label>
            <?php
            $this->lstAutoComplete2->Render();
            $this->textSearch2->Render();
            ?>
        </div>
        <div class="col-md-1">
            <label>.</label>
            <?php $this->btnFilter->Render(); ?>
            <?php $this->objDefaultWaitIcon->Render(); ?>
        </div>
    </div>
</div>
<hr/>
<div class="box-body">
    <?php $this->dtgUserses->Render(); ?>
</div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>
<script type="text/javascript">
	$(function(){
		$('input[placeholder="Search"]').remove();
	});

    function changeHashOnLoad() {
        window.location.href += '#';
        setTimeout('changeHashAgain()', '50');
    }

    function changeHashAgain() {
        window.location.href += '1';
    }

    var storedHash = window.location.hash;
    window.setInterval(function () {
        if (window.location.hash != storedHash) {
            window.location.hash = storedHash;
        }
    }, 50);
    window.onload=changeHashOnLoad;
</script>