<?php
$strPageTitle = QApplication::Translate('Users') . ' - ' . $this->mctUsers->TitleVerb;
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3>.:: My Profile ::.</h3>
            </div>
            <hr/>

            <div class="box-body">
                <div class="form-controls">

                    <div class="form-group">
                        <?php $this->txtNik->RenderWithBootstrapInGroup(3,4); ?>
                    </div>

                    <div class="form-group">
                        <?php $this->txtEmployeeName->RenderWithBootstrapInGroup(3,8); ?>
                    </div>
                    <div class="form-group">
                        <?php $this->txtPositionName->RenderWithBootstrapInGroup(3,6); ?>
                    </div>
                    <div class="form-group">
                        <?php $this->txtPhoneNumber->RenderWithBootstrapInGroup(3,4); ?>
                    </div>
                    <div class="form-group">
                            <?php $this->txtEmailAddress->RenderWithBootstrapInGroup(3,8); ?>
                    </div>

                    <div class="form-group">
                        <?php $this->txtBranchCode->RenderWithBootstrapInGroup(3,8); ?>
                    </div>
                    <div class="form-group">
                        <?php $this->txtRoleId->RenderWithBootstrapInGroup(3,6); ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php $this->objDefaultWaitIcon->Render(); ?>
                    <br/>
                    <div style ='font:11px/21px Arial,tahoma,sans-serif;color:#ff0000'>
                        <i>
                            <b>
                                <?php $this->lblUsernameSame->Render(); ?>
                            </b>
                        </i>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <div class="col-md-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3> .:: Ubah Password ::. </h3>

                </div>
                <br/>
                <div class="form-group">
                    <?php $this->txtUsername->RenderWithBootstrapInGroup(3,8); ?>
                </div>
                <hr/>

                <div class="box-body">
                    <?php $this->btnChangePassword->Render(); ?>
                    <hr/>
                    <?php $this->txtLongPassword->RenderWithBootstrap(3,8); ?>
                    <?php $this->txtNewPassword->RenderWithBootstrap(3,8); ?>
                    <?php $this->txtConfirmPassword->RenderWithBootstrap(3,8); ?>
                </div>

                <div class="box-footer">
                    <?php $this->btnSavePassword->Render() ?>
                    <br /><?php $this->lblPwdWarning->Render(); ?>
                </div>
            </div>
        </div>
</div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>