<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');


class UsersEditForm extends QForm {
    protected $mctUsers;

    protected $txtUserId;
    protected $txtNik;
    protected $txtEmployeeName;
    protected $txtPhoneNumber;
    protected $txtEmailAddress;
    protected $txtPositionName;
    protected $txtLinkImage;
    protected $txtUsername;
    protected $lstRole;
    protected $chkIsUserActive;
    protected $lstBankBranchId;
    protected $txtUpdatedBy;
    protected $calUpdatedOn;
    protected $calCreatedOn;
    protected $txtCreatedBy;
    protected $txtPassword;
    protected $txtWorkedUser;
    protected $txtUpdateNIK;
    protected $txtUpdateEmployeeName;
    protected $txtUpdatePhoneNumber;
    protected $txtUpdateEmailAddress;
    protected $txtUpdatePositionName;
    protected $txtUpdateUsername;


    protected $btnSave;
    protected $btnCancel;
    protected $btnDelete;
    protected $btnApprove;

    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();
        $this->mctUsers = UsersMetaControl::CreateFromPathInfo($this);

        $this->txtUpdateNIK= $this->mctUsers->txtUpdateNIK_Create();
        $this->txtUpdateNIK->Enabled= false;

        $this->txtUpdateEmployeeName = $this->mctUsers->txtUpdateEmployeeName_Create();
        $this->txtUpdateEmployeeName->Enabled= false;

        $this->txtUpdatePositionName = $this->mctUsers->txtUpdatePositionName_Create();
        $this->txtUpdatePositionName->Enabled= false;

        $this->txtUpdatePhoneNumber = $this->mctUsers->txtUpdatePhoneNumber_Create();
        $this->txtUpdatePhoneNumber->Enabled= false;

        $this->txtUpdateEmailAddress = $this->mctUsers->txtUpdateEmailAddress_Create();
        $this->txtUpdateEmailAddress->Enabled= false;

        $this->txtUpdateUsername = $this->mctUsers->txtUpdateUsername_Create();
        $this->txtUpdateUsername->Enabled= false;


        $this->btnCancel = new QButton($this);
        $this->btnCancel->Text = QApplication::Translate('Back');
        $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
        $this->btnCancel->CssClass = 'btn btn-warning';

        $this->btnDelete = new QButton($this);
        $this->btnDelete->Text = QApplication::Translate('Reject');
        $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnReject_Click'));
        $this->btnDelete->CssClass = 'btn btn-danger';

        $this->btnSave = new QButton($this);
        $this->btnSave->Text = QApplication::Translate('Request');
        $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
        $this->btnSave->CausesValidation = true;
        $this->btnSave->CssClass = 'btn btn-success';

        $this->btnApprove = new QButton($this);
        $this->btnApprove->Text = QApplication::Translate('Approve');
        $this->btnApprove->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
        $this->btnApprove->CausesValidation = true;
        $this->btnApprove->CssClass = 'btn btn-success';

        /*$objTest = ApuParameter::Load(100000);
        $this->btnSave->Text = $objTest->ParamType;*/
    }

    protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
        if($this->mctUsers->EditMode){
        $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            $obj = Users::Load(QApplication::PathInfo(0));
            $this->txtUpdateEmployeeName = $this->mctUsers->txtUpdateUsername_Create();
            $this->txtEmployeeName = $this->mctUsers->txtEmployeeName_Create();
            $obj->IsUserActive = true;
            $obj ->WorkedUser = 1;
            $obj->Nik = $obj->UpdateNik;
            $obj->PositionName = $obj->UpdatePositionName;
            $obj->PhoneNumber = $obj->UpdatePhoneNumber;
            $obj->EmailAddress = $obj->UpdateEmailAddress;
            $obj->Username = $obj->UpdateUsername;
//            $obj->UpdateEmployeeName= $this->txtEmployeeName->Text;
            $obj->Save();
            $obj->ActiveBy = $_SESSION[__USER_LOGIN__];

        }else{
            $this->txtWorkedUser = $this->mctUsers->txtWorkedUser_Create();
            $this->txtWorkedUser->Text = 1;
            $this->calUpdatedOn->DateTime = QDateTime::Now();
            $this->txtUpdatedBy->Text = $_SESSION[__USER_LOGIN__];
            $this->txtUserId->Text = System::GetId();
            $this->chkIsUserActive->Checked = true;
            $this->calCreatedOn->DateTime = QDateTime::Now();
            $this->txtCreatedBy->Text = $_SESSION[__USER_LOGIN__];
            $this->txtLinkImage->Text = 'avatar5.png';
            $password = ApuParameter::LoadByParamType('APPLICATION_PARAM');
            $this->txtPassword->Text = System::getHash($password->ParamValue);
            $this->txtActiveBy->Text = $_SESSION[__USER_LOGIN__];
        }
//        $this->mctUsers->SaveUsers();
        $this->RedirectToListPage();

    }

    protected function Form_Validate() {
        // By default, we report the result of validation from the parent
        $blnToReturn = parent::Form_Validate();

        // Custom Validation Rules
        // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            // Set Focus to the top-most invalid control
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            // Blink on ALL invalid controls
            $objControl->Blink();
        }

        return $blnToReturn;
    }

    protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
        $this->RedirectToListPage();
    }

    protected function btnReject_Click($strFormId, $strControlId, $strParameter) {
        $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
        $obj = Users::Load(QApplication::PathInfo(0));
        $obj->IsUserActive = true;
        $obj ->WorkedUser = 1;
        $obj->UpdateNik= $obj->Nik;
        $obj->UpdateEmployeeName= $obj->EmployeeName;
        $obj->UpdatePositionName= $obj->PositionName;
        $obj->UpdateEmailAddress= $obj->EmailAddress;
        $obj->UpdatePhoneNumber= $obj->PositionName;
        $obj->UpdateUsername= $obj->Username;
        $obj->Save();
        $this->RedirectToListPage();
    }

    // Other Methods

    protected function RedirectToListPage() {
        QApplication::Redirect(__SOURCE__ . '/user/user/list.php');
    }
}

// Go ahead and run this form object to render the page and its event handlers, implicitly using
// users_edit.tpl.php as the included HTML template file
UsersEditForm::Run('UsersEditForm');
?>