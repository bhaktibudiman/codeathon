<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');
class UsersEditForm extends QForm {
    protected $mctUsers;

    // Controls for Users's Data Fields
    protected $txtUserId;
    protected $txtNik;
    protected $txtEmployeeName;
    protected $txtPhoneNumber;
    protected $txtEmailAddress;
    protected $txtPositionName;
    protected $txtUsername;
    protected $txtPassword;
    protected $txtRoleId;
    protected $txtRoleName;
    protected $txtRoleLevel;
    protected $chkIsUserActive;
    protected $txtBranchCode;
    protected $txtBranchName;
    protected $txtLinkImage;
    protected $calLastLogin;
    protected $txtExpiredCount;
    protected $txtLockedCount;
    protected $chkIsLocked;
    protected $txtLockedSession;
    protected $txtCreatedBy;
    protected $txtCreatedUser;
    protected $calLastUpdate;
    protected $txtRecordStatus;
    protected $txtRecordStatusName;
    protected $chkIsLoggedIn;
    protected $lblUsernameSame;
    protected $lblPwdWarning;
    protected $txtCountExpired;
    protected $rdUserType;
    protected $txtEmail;
    protected $txtUserType;


    protected $btnSave;
    protected $btnDelete;
    protected $btnCancel;

    protected $txtLongPassword;
    protected $txtNewPassword;
    protected $txtConfirmPassword;
    protected $btnSavePassword;
    protected $btnChangePassword;

    protected $objAuth;

    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();

        $this->mctUsers = AuthUsersMetaControl::CreateFromPathInfo($this);
        $this->objAuth = AuthUsers::Load(QApplication::PathInfo(0));
//        QApplication::DisplayAlert(QApplication::PathInfo(1));
        $objUser = Users::Load($this->objAuth->UserId);

        $this->objDefaultWaitIcon = new QWaitIcon($this);

        $this->txtUserId = $this->mctUsers->txtUserId_Create();

        $this->chkIsUserActive = $this->mctUsers->chkIsUserActive_Create();
        $this->chkIsUserActive->Name = 'Status User ID';

        $this->txtEmployeeName = $this->mctUsers->txtEmployeeName_Create();
        $this->txtEmployeeName->Name = 'Nama User *';
        $this->txtEmployeeName->Required = true;

        $this->txtNik = $this->mctUsers->txtNik_Create();
        $this->txtNik->Name = 'NIK *';
        $this->txtNik->Required = true;

        $this->txtPhoneNumber = $this->mctUsers->txtPhoneNumber_Create();
        $this->txtPhoneNumber->Name = 'Telepon *';

        $this->txtEmailAddress  = $this->mctUsers->txtEmailAddress_Create();
        $this->txtEmailAddress->Name = 'Email *';
        $this->txtEmailAddress->Placeholder = 'example@banksampoerna.com';
        $this->txtEmailAddress->Required = true;

        $this->txtPositionName = $this->mctUsers->txtPositionName_Create();
        $this->txtPositionName->Name = 'Posisi *';
        $this->txtPositionName->Required = true;

        $this->txtUsername = $this->mctUsers->txtUsername_Create();
        $this->txtUsername->Name = 'Username *';
        $this->txtUsername->Required = true;

        $this->txtBranchCode = $this->mctUsers->lstBranch_Create();
        $this->txtBranchCode->Name = 'Cabang *';
        $this->txtBranchCode->AddItem('- Kantor Pusat -',null);
        if($arrBranch = BankBranch::LoadAll()){
            foreach($arrBranch as $bankBranch){
                $this->txtBranchCode->AddItem($bankBranch->BranchCode." - ".$bankBranch->BranchName, $bankBranch->BranchCode);
            }
        }
        if($this->mctUsers->EditMode){
            $objUser = Users::Load($objUser->UserId);
            $this->txtBranchCode->SelectedValue = $objUser->BranchCode;
            if($objUser->BranchCode == NULL){
                $this->txtBranchCode->SelectedIndex = 0;
            }
        }
        $this->txtBranchCode->AddAction(new QClickEvent(), new QAjaxAction('Branch_Click'));
        $this->txtBranchName = $this->mctUsers->txtBranchName_Create();
        $this->txtBranchName->Enabled = false;


        $this->txtRoleId = $this->mctUsers->lstRoleId_Create();
        $this->txtRoleId->Name = 'Role *';
        $this->txtRoleId->AddItem('- Pilih Role User -');
        if($arrRole = RoleUser::LoadAll()){
            foreach($arrRole as $role){
                $this->txtRoleId->AddItem($role->RoleLevel." - ".$role->RoleName, $role->RoleId);
            }
        }
        $this->txtRoleId->ActionParameter = 'role';
        $this->txtRoleId->AddAction(new QEnterKeyEvent(), new QAjaxAction('RoleName_Click'));
        $this->txtRoleName = $this->mctUsers->txtRoleName_Create();
        $this->txtRoleName->Enabled = false;
        $this->txtRoleLevel = $this->mctUsers->txtRoleLevel_Create();
        $this->txtRoleLevel->Enabled = false;

        $this->txtUserType = $this->mctUsers->txtUserType_Create();
        $this->rdUserType = new QRadioButtonList($this);
        $this->rdUserType->Name = 'Tipe User *';
        $this->rdUserType->AddItem('Pusat',1,true);
        $this->rdUserType->AddItem('Cabang',2,true);
        $this->rdUserType->ActionParameter = 'rdUserType';
        $this->rdUserType->AddAction(new QClickEvent(), new QAjaxAction('lstUserType_Click'));


        if($this->mctUsers->EditMode){
            if(isset($this->objAuth)){
                $this->txtRoleId->SelectedValue = $this->objAuth->RoleId;
                $this->rdUserType->SelectedValue = $this->objAuth->UserType;
                $this->txtRoleName->Text = $this->objAuth->RoleName;
                $this->txtRoleLevel->Text = $this->objAuth->RoleLevel;
                $this->txtBranchCode->SelectedValue = $this->objAuth->BranchCode;
                $this->txtBranchName->Text = $this->objAuth->BranchName;
            }
        }

        $this->txtLongPassword = new QTextBox($this);
        $this->txtLongPassword->Name = 'Password Sekarang';
        $this->txtLongPassword->Enabled = false;
        $this->txtLongPassword->TextMode = QTextMode::Password;
        $this->txtLongPassword->Required = true;

        $this->txtNewPassword = new QTextBox($this);
        $this->txtNewPassword->Name = 'Password Baru';
        $this->txtNewPassword->Enabled = false;
        $this->txtNewPassword->TextMode = QTextMode::Password;
        $this->txtNewPassword->Required = true;

        $this->txtConfirmPassword = new QTextBox($this);
        $this->txtConfirmPassword->Name = 'Konfirmasi Password';
        $this->txtConfirmPassword->Enabled = false;
        $this->txtConfirmPassword->TextMode = QTextMode::Password;
        $this->txtConfirmPassword->Required = true;

        // Create Buttons and Actions on this Form
        $this->btnSave = new QButton($this);
        $this->btnSave->Text = QApplication::Translate('Author');
        $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
        $this->btnSave->CausesValidation = true;
        $this->btnSave->CssClass = 'btn btn-success';

        $this->btnCancel = new QButton($this);
        $this->btnCancel->Text = QApplication::Translate('Batal');
        $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
        $this->btnCancel->CssClass = 'btn btn-warning';

        $this->btnDelete = new QButton($this);
        $this->btnDelete->Text = QApplication::Translate('Reject');
//        $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Reject Data ini %s ?'), $this->objAuth->Username)));
        $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
        $this->btnDelete->CssClass = 'btn btn-danger';

        $this->btnChangePassword = new QButton($this);
        $this->btnChangePassword->Text = QApplication::Translate('Change Password');
        $this->btnChangePassword->AddAction(new QClickEvent(), new QAjaxAction('btnChange_Click'));
        $this->btnChangePassword->CssClass = 'btn btn-primary';

        $this->btnSavePassword = new QButton($this);
        $this->btnSavePassword->Text = QApplication::Translate('Save New Password');
        $this->btnSavePassword->AddAction(new QClickEvent(), new QAjaxAction('btnSavePassword_Click'));
        $this->btnSavePassword->CausesValidation = true;
        $this->btnSavePassword->CssClass = 'btn btn-success';
        $this->btnSavePassword->Visible = false;

        $this->lblUsernameSame = new QLabel($this);
        $this->lblUsernameSame->Text = 'Username is already exist !';
        $this->lblUsernameSame->Visible = false;

        $this->lblPwdWarning = new QLabel($this);
        $this->lblPwdWarning->Text = 'Password should be a series of alphabetic, numeric , uppercase , lowercase and characters special !';
        $this->lblPwdWarning->Visible = false;

        if($this->mctUsers->EditMode){
            $this->txtUsername->Enabled = false;
            $this->rdUserType->Hide();
        }else{
            $this->chkIsUserActive->Hide();
        }

        $this->txtNik->Enabled = false;
        $this->txtEmployeeName->Enabled = false;
        $this->txtPhoneNumber->Enabled = false;
        $this->txtPositionName->Enabled = false;
        $this->txtEmailAddress->Enabled = false;
        $this->txtUsername->Enabled = false;
        $this->chkIsUserActive->Enabled = false;
        $this->rdUserType->Enabled = false;
        $this->txtRoleId->Enabled = false;
        $this->txtRoleName->Enabled = false;
        $this->txtRoleLevel->Enabled = false;
        $this->txtBranchCode->Enabled = false;
        $this->txtBranchName->Enabled = false;
    }

    protected function lstUserType_Click($strFormId, $strControlId, $strParameter) {
        if($this->rdUserType->SelectedIndex == 0){
            $objBranch = new Users();
            $this->txtBranchCode->RemoveAllItems();
            $this->txtBranchCode->AddItem('- Kantor Pusat -',null);
            if($objBranch->BranchCode){
                $param = BankBranch::Load(null);
                foreach($param as $bankBranch){
                    $this->txtBranchCode->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                }
            }
        }else{
            if($arrBranch = BankBranch::LoadAll()){
                $this->txtBranchCode->RemoveAllItems();
                $this->txtBranchCode->AddItem('- Pilih Cabang -');
                foreach($arrBranch as $bankBranch){
                    $this->txtBranchCode->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                }
            }
        }
    }

    protected function RoleName_Click($strFormId, $strControlId, $strParameter) {
        $param = $this->txtRoleId->SelectedValue;
        $role = RoleUser::Load($param);
        if(isset($role)){
            $this->txtRoleName->Text = $role->RoleName;
            $this->txtRoleLevel->Text = $role->RoleLevel;
        }
    }

    protected function Branch_Click($strFormId, $strControlId, $strParameter) {
        $param = $this->txtBranchCode->SelectedValue;
        $branch = BankBranch::Load($param);
        if($branch){
            $this->txtBranchName->Text = $branch->BranchName;
        }
    }

    protected function Form_Validate() {
        // By default, we report the result of validation from the parent
        $blnToReturn = parent::Form_Validate();

        // Custom Validation Rules
        // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            // Set Focus to the top-most invalid control
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            // Blink on ALL invalid controls
            $objControl->Blink();
        }

//        if(!is_numeric($this->txtNik->Text)){
//            $this->txtNik->Focus();
//            $this->txtNik->blink();
//            $blnToReturn = false;
//        }elseif(!is_numeric($this->txtPhoneNumber->Text)){
//            $this->txtPhoneNumber->Focus();
//            $this->txtPhoneNumber->blink();
//            $blnToReturn = false;
//        }
        return $blnToReturn;
    }

    protected function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }

    protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
        $this->objAuth = AuthUsers::Load(QApplication::PathInfo(0));
        $deleted = Users::Load($this->objAuth->UserId);
        $error = true;
        $db = $this->objAuth->GetDatabase();
        $db->TransactionBegin();

        try{
            if($this->objAuth->RecordStatus == 'D'){
                $users = Users::Load($this->objAuth->UserId);
                $users->Delete();
                $this->mctUsers->DeleteAuthUsers();
                $deleted->Delete();
//                $log = new LogActivity();
//                $log->ApprovalDeleted();
                $error = false;
            }elseif($this->objAuth->RecordStatus == 'C'){
                $deleted->Delete();
                /*---------- Save Data First ------------*/
                $paramEdit = new Users();
                $paramEdit->UserId  = $this->txtUserId->Text;
                $paramEdit->Nik = $this->txtNik->Text;
                $paramEdit->EmployeeName = $this->txtEmployeeName->Text;
                $paramEdit->PhoneNumber = $this->txtPhoneNumber->Text;
                $paramEdit->EmailAddress = $this->txtEmailAddress->Text;
                $paramEdit->PositionName = $this->txtPositionName->Text ;
                $paramEdit->Username = $this->txtUsername->Text;
                $paramEdit->RoleId = $this->txtRoleId->SelectedValue ;
                $paramEdit->RoleName = $this->txtRoleName->Text ;
                $paramEdit->RoleLevel = $this->txtRoleLevel->Text;
                $paramEdit->UserType = $this->rdUserType->SelectedValue;
                $paramEdit->BranchCode = $this->txtBranchCode->SelectedValue;
                $paramEdit->BranchName = $this->txtBranchName->Text;
                $paramEdit->IsUserActive = true;
                $paramEdit->IsLocked = false;
                $paramEdit->LastUpdate = QDateTime::Now();
                $paramEdit->RecordStatus = 'A';
                $paramEdit->RecordStatusName = 'COMPLETED AUTHOR';
                $paramEdit->LinkImage = 'avatar5.png';

                # AUTHORIZE PASSWORD EMAIL !
                $password = $this->random_password(8);
                $paramEdit->Password= System::getHash($password);

                QEmailServer::$SmtpServer = "117.54.9.245";
                $objMessage = new QEmailMessage();
                $objMessage->From = 'info user login <info.user.login@fds.co.id>';
                $objMessage->To = $paramEdit->EmailAddress;
                $objMessage->Cc = 'Alpin Wahyudi <alpin.wahyudi@fds.co.id>';
                $objMessage->Subject = 'Permintaan Username dan Password Aplikasi Apuppt';

                # KONTEN !
                $strBody = "Dear " . $paramEdit->EmployeeName . ',<br/><br/>';
                $strBody .= sprintf("You have successfully submitted your Username request."."<br/><br/>");
                $strBody .= sprintf("<br>Silahkan untuk memulai login di aplikasi apuppt."."<br/>");
                $strBody .= sprintf("<br>Dengan Username :   "."<b>". $paramEdit->Username."</b>");
                $strBody .= sprintf("<br>Dan Password : "."<b>". $password."</b><br/><br/><br/>");
                $strBody .= '<br>Regards,<br/><br/><b>ADMINISTRATOR</b>';
                $objMessage->HtmlBody = $strBody;

                # DETAIL !
                $objMessage->SetHeader('apuppt', 'Jiarsi');
                QApplication::DisplayAlert('Username Berhasil Di Kirim ke email' . $paramEdit->EmailAddress);
                QEmailServer::Send($objMessage);
                $paramEdit->Password = System::getHash($password);
                QApplication::DisplayAlert("Data Berhasil Di aktidkan, Periksa Email !");
                $paramEdit->Save();
//                $log = new LogActivity();
//                $log->ApprovalCreated();
                $error = false;
            }else{
                $deleted->Delete();
                /*---------- Save Data First ------------*/
                $paramEdit = new Users();
                $paramEdit->UserId  = $this->txtUserId->Text;
                $paramEdit->Nik = $this->txtNik->Text;
                $paramEdit->EmployeeName = $this->txtEmployeeName->Text;
                $paramEdit->PhoneNumber = $this->txtPhoneNumber->Text;
                $paramEdit->EmailAddress = $this->txtEmailAddress->Text;
                $paramEdit->PositionName = $this->txtPositionName->Text ;
                $paramEdit->Username = $this->txtUsername->Text;
                $paramEdit->RoleId = $this->txtRoleId->SelectedValue ;
                $paramEdit->RoleName = $this->txtRoleName->Text ;
                $paramEdit->RoleLevel = $this->txtRoleLevel->Text;
                $paramEdit->UserType = $this->rdUserType->SelectedValue;
                $paramEdit->BranchCode = $this->txtBranchCode->SelectedValue;
                $paramEdit->BranchName = $this->txtBranchName->Text;
                $paramEdit->IsUserActive = true;
                $paramEdit->IsLocked = false;
                $paramEdit->LastUpdate = QDateTime::Now();
                $paramEdit->RecordStatus = 'A';
                $paramEdit->RecordStatusName = 'COMPLETED AUTHOR';
                $paramEdit->LinkImage = 'avatar5.png';
                $paramEdit->Save();
//                $log = new LogActivity();
//                $log->ApprovalCreated();
                $error = false;
            }
        }catch (Exception $e){
            $error = true;
            $db->TransactionRollBack();
            throw $e;
        }

        if(!$error){
            QApplication::DisplayAlert('AUTHOR BERHASIL !');
            $db->TransactionCommit();
            $this->mctUsers->DeleteAuthUsers();
            $this->RedirectToListPage();
        }
    }

    protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
        $this->objAuth = AuthUsers::Load(QApplication::PathInfo(0));
        $save = Users::Load($this->objAuth->UserId);

        if($this->objAuth->RecordStatus == 'C'){
            $users = Users::Load($this->objAuth->UserId);
            $users->Delete();
            $this->mctUsers->DeleteAuthUsers();
            $save->Delete();
//                $log = new LogActivity();
//                $log->ApprovalDeleted();
        }else{
         $save->CreatedBy = $_SESSION[__USER_LOGIN__];
            $save->CreatedUser = $this->objAuth->Username;
            $save->LastUpdate = QDateTime::Now();
            $save->RecordStatus = 'R';
            $save->RecordStatusName = 'REJECTED AUTHOR';
            $save->Save();
            $this->mctUsers->DeleteAuthUsers();
//            $log = new LogActivity();
//            $log->RejectCreated();
            QApplication::DisplayAlert('Data Berhasil di Reject !!');
        }
        $this->RedirectToListPage();
    }

    protected function RedirectToListPage() {
        QApplication::Redirect(__SOURCE__ . '/user/user/auth/list.php');
    }

    protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
        $this->RedirectToListPage();
    }
}
UsersEditForm::Run('UsersEditForm');
?>