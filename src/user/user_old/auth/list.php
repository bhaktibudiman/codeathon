<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

class UsersListForm extends QForm {
    protected $dtgUserses;
    protected $objLinkProxy;
    protected $lstAutoComplete;
    protected $lstAutoComplete2;
    protected $lstParam1;
    protected $lstParam2;
    protected $btnFilter;
    protected $textSearch1;
    protected $textSearch2;
    protected $hideButton;

    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create()
    {
        parent::Form_Create();

        // Instantiate the Meta DataGrid
        $this->objDefaultWaitIcon = new QWaitIcon($this);
        $this->dtgUserses = new AuthUsersDataGrid($this);

        $this->objLinkProxy = new QControlProxy($this);
        $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));
        $this->dtgUserses->Paginator = new QPaginator($this->dtgUserses);
        $this->dtgUserses->ShowSearch = false;
        $this->dtgUserses->ItemsPerPage = 10;
        $this->dtgUserses->ShowFilterButton = false;
        $this->dtgUserses->ShowFilterResetButton = false;

        $this->lstAutoComplete = new QSelect2ListBox($this);
        $this->lstAutoComplete->Width = '250';
        $this->lstAutoComplete->AddItem('- Select One -', null);

        $this->lstAutoComplete2 = new QListBox($this);
        $this->lstAutoComplete2->Width = '250';
        $this->lstAutoComplete2->AddItem('- Select One -', null);

        $this->textSearch1 = new QTextBox($this);
        $this->textSearch1->hide();

        $this->textSearch2 = new QTextBox($this);
        $this->textSearch2->Width = '250';
        $this->textSearch2->hide();

        $this->lstParam1 = new QListBox($this);
        $this->lstParam1->Name = 'Param 1';
        $this->lstParam1->Width = '160';
        $this->lstParam1->AddItem('-Any-');
        $this->lstParam1->AddItem('Cabang', 'bankBranch');
        $this->lstParam1->AddItem('Role', 'role');
        $this->lstParam1->AddItem('NIK', 'nik');
        $this->lstParam1->AddItem('Nama User', 'name');
        $this->lstParam1->AddItem('Status Author', 'author');
//        $this->lstParam1->AddItem('Lock', 'lock');
        $this->lstParam1->ActionParameter = 'changeParam1';
        $this->lstParam1->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

        $this->lstParam2 = new QListBox($this);
        $this->lstParam2->Name = 'Param 2';
        $this->lstParam2->Width = '160';
        $this->lstParam2->AddItem('-Any-');
        $this->lstParam2->AddItem('Cabang', 'bankBranch');
        $this->lstParam2->AddItem('Role', 'role');
        $this->lstParam2->AddItem('NIK', 'nik');
        $this->lstParam2->AddItem('Nama User', 'name');
        $this->lstParam2->AddItem('Status Author', 'author');
//        $this->lstParam2->AddItem('Lock', 'lock');
        $this->lstParam2->ActionParameter = 'changeParam3';
        $this->lstParam2->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

        $this->btnFilter = new QButton($this);
        $this->btnFilter->Text = QApplication::Translate('Cari');
        $this->btnFilter->CssClass = 'btn btn-primary';
        $this->btnFilter->ActionParameter = 'changeParam2';
        $this->btnFilter->CausesValidation = true;
        $this->btnFilter->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

        $this->hideButton = false;
        $this->dtgUserses->AddColumn(new QDataGridColumn('No.', '<?= $_CONTROL->CurrentNumber ?>.'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Actions', '<?= $_FORM->rowActionButton($_ITEM)?>', 'HtmlEntities=false','Width=100'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Status Author', '<?= $_ITEM->RecordStatusName ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Cabang', '<?= $_FORM->branch_null($_ITEM->BranchName) ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('NIK', '<?= $_ITEM->Nik ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Username', '<?= $_ITEM->Username ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Email', '<?= $_ITEM->EmailAddress ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Role', '<?= $_ITEM->RoleName ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Aktif/n', '<?= $_FORM->is_active($_ITEM->IsUserActive) ?>'));
        $this->dtgUserses->AddColumn(new QDataGridColumn('Last Login', '<?= $_ITEM->LastLogin ?>'));
/*        $this->dtgUserses->AddColumn(new QDataGridColumn('Lock', '<?= $_FORM->locked($_ITEM->IsLocked) ?>'));*/

        $this->dtgUserses->SetDataBinder('pageBinder');
    }

    public function rowActionButton($objRecord)
    {
        # VIEW USER
        $strReturn = null;
        $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="Detail"><span class="fa fa-pencil"></span></a>',
            __SOURCE__ . '/user/user/auth/form.php/'.$objRecord->UserIdAuth /*. '/'. $objRecord->UserId*/);

        # ACTIVE USER AUTHOR
        if($objRecord->RecordStatus != 'D' && $objRecord->RecordStatus != 'U') {
            $this->hideButton = false;
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-warning" title="Active"><span class="fa fa-lock"></span></a>',
                $this->objLinkProxy->RenderAsHref('active_' . $objRecord->UserId, false));
        }

        # DELETE USER
        if($objRecord->RecordStatus != 'C' && $objRecord->RecordStatus != 'U') {
            $this->hideButton = false;
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Request Delete"><span class="fa fa-trash-o"></span></a>',
                $this->objLinkProxy->RenderAsHref('otordel_' . $objRecord->UserIdAuth, false), $objRecord->Username);
        }

        return $strReturn;
    }

    protected function random_password( $length = 8 ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$^&*()_-=+;:,.?";
        $password = substr( str_shuffle( $chars ), 0, $length );
        return $password;
    }


    protected function pageAction($strFormId, $strControlId, $strParameter)
    {
        if (stristr($strParameter, '_')) list($section, $select) = @explode('_', $strParameter);
        else $section = $strParameter;

        switch ($section) {
            case'otordel':
                $auth = AuthUsers::Load($select);
                $users = Users::Load($auth->UserId);
                $users->Delete();
                $obj = AuthUsers::LoadUserId($users->UserId);
                $obj->Delete();
//                $log = new LogActivity();
//                $log->ApprovalDeleted();
                QApplication::DisplayAlert($obj->Username . ' Berhasil Terhapus !');
                $this->dtgUserses->Refresh();
//                QApplication::DisplayAlert($users);
                break;

            case'active':
                $error = true;
                $auth = AuthUsers::LoadUserId($select);
                $deleted = Users::Load($select);
                $deleted->Delete();
                /*----------Deleted Existing Data Parameter ------------*/
                $param = new Users();
                try{
                    /*---------- Author Data ------------*/
                    $param->UserId  = $select;
                    $param->Nik = $auth->Nik;
                    $param->EmployeeName = $auth->EmployeeName;
                    $param->BranchName = $auth->BranchName;
                    $param->PhoneNumber= $auth->PhoneNumber;
                    $param->EmailAddress = $auth->EmailAddress;
                    $param->PositionName = $auth->PositionName;
                    $param->Username= $auth->Username;
                    $param->RoleId= $auth->RoleId;
                    $param->RoleName= $auth->RoleName;
                    $param->RoleLevel= $auth->RoleLevel;
                    $param->IsUserActive= true;
                    $param->BranchCode= $auth->BranchCode;
                    $param->BranchName= $auth->BranchName;
                    $param->LinkImage = 'avatar5.png';
                    $param->CreatedBy = $_SESSION[__USER_LOGIN__];
                    $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
                    $param->CreatedUser= $objUser->Username;
                    $param->LastUpdate= QDateTime::Now();
                    $param->RecordStatus= 'A';
                    $param->RecordStatusName = 'COMPLETED AUTHOR';
                    $param->UserType = $auth->UserType;

                    # AUTHORIZE PASSWORD EMAIL !
                    $password = $this->random_password(8);
                    $param->Password= System::getHash($password);

                    QEmailServer::$SmtpServer = "117.54.9.245";
                    $objMessage = new QEmailMessage();
                    $objMessage->From = 'info user login <info.user.login@fds.co.id>';
                    $objMessage->To = $param->EmailAddress;
                    $objMessage->Cc = 'Alpin Wahyudi <alpin.wahyudi@fds.co.id>';
                    $objMessage->Subject = 'Permintaan Username dan Password Aplikasi Apuppt';

                        # KONTEN !
                        $strBody = "Dear " . $objUser->EmployeeName . ',<br/><br/>';
                        $strBody .= sprintf("You have successfully submitted your Username request."."<br/><br/>");
                        $strBody .= sprintf("<br>Silahkan untuk memulai login di aplikasi apuppt."."<br/>");
                        $strBody .= sprintf("<br>Dengan Username :   "."<b>". $param->Username."</b>");
                        $strBody .= sprintf("<br>Dan Password : "."<b>". $password."</b><br/><br/><br/>");
                        $strBody .= '<br>Regards,<br/><br/><b>ADMINISTRATOR</b>';
                        $objMessage->HtmlBody = $strBody;

                        # DETAIL !
                        $objMessage->SetHeader('apuppt', 'Jiarsi');
                        QApplication::DisplayAlert('Username Berhasil Di Kirim ke email' . $param->EmailAddress);
                        QEmailServer::Send($objMessage);
                        $param->Password = System::getHash($password);
                        QApplication::DisplayAlert("Data Berhasil Di aktifkan, Periksa Email !");
                        $param->Save();
//                    $log = new LogActivity();
//                    $log->ApprovalCreated();
                    $error = false;
                }catch (Exception $e){
                    $error = true;
                    throw $e;
                }

                /*----------Deleted Existing Data Temporary ------------*/
                if(!$error){
                    $obj = AuthUsers::LoadUserId($select);
                    $obj->Delete();
                    QApplication::DisplayAlert($obj->Username. ' Data Berhasil Di aktifkan, Periksa Email !');
                    $this->dtgUserses->Refresh();
                }
                break;

//            case'unlock':
//                $objUser = Users::Load($param);
//                $objUser->LockedSession = null;
//                $objUser->IsLocked = false;
//                $objUser->IsUserActive = true;
////                $objUser->WorkedUser= 1;
//                $objUser->LockedCount = 0;
//
//                QApplication::DisplayAlert('Username dengan NIK '. $objUser->Nik . ', Nama User '  . $objUser->EmployeeName.' berhasil di aktifkan');
//                $objUser->Save();
//                $this->dtgUserses->Refresh();
//                break;


            case 'changeParam1':
                if($this->lstParam1->SelectedValue == 'bankBranch'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');
//						if(branch = BankBranch::Load(29))
                    if($arrBranch = BankBranch::LoadAll()){
                        foreach($arrBranch as $bankBranch){
                            $this->lstAutoComplete->AddItem('[ '.$bankBranch->BranchCode.' ] '.$bankBranch->BranchName, $bankBranch->BranchCode);
                        }
                    }
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->hide();
//						$this->datePicker1->Input->hide();
                }elseif($this->lstParam1->SelectedValue == 'role'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');

                    if($arrRole = RoleUser::LoadAll()){
                        foreach($arrRole as $role){
                            $this->lstAutoComplete->AddItem('[ '.$role->RoleLevel.' ] '.$role->RoleName, $role->RoleId);
                        }
                    }
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->hide();
//						$this->datePicker->Input->hide();
                }elseif($this->lstParam1->SelectedValue == 'author'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');
                    $this->lstAutoComplete->AddItem('CREATED AUTHOR','C');
                    $this->lstAutoComplete->AddItem('COMPLETED AUTHOR','A');
                    $this->lstAutoComplete->AddItem('REJECTED AUTHOR', 'R');
//						$this->lstAutoComplete->AddItem('DELETED', 'D');
                    $this->lstAutoComplete->AddItem('UPDATED AUTHOR', 'U');
                    $this->lstAutoComplete->AddItem('NOT AUTHOR', 'BLANK');
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->hide();
//						$this->datePicker1->Input->hide();
                }elseif($this->lstParam1->SelectedValue == 'lock'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');
                    $this->lstAutoComplete->AddItem('Y',TRUE);
                    $this->lstAutoComplete->AddItem('N',FALSE);
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->hide();
//						$this->datePicker1->Input->hide();
                }elseif($this->lstParam1->SelectedValue == 'name'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->hide();
                    $this->textSearch1->unhide();
                    $this->textSearch1->Text = '';
                }elseif($this->lstParam1->SelectedValue == 'nik'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->hide();
                    $this->textSearch1->unhide();
                    $this->textSearch1->Text = '';
                }else{
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');
                    $this->textSearch1->Text = '';
                    $this->dtgUserses->Refresh();
                }
                break;
            case 'changeParam2':
                $this->dtgUserses->Refresh();
                break;
            case 'changeParam3':
                if($this->lstParam2->SelectedValue == 'bankBranch'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');

                    if($arrBranch = BankBranch::LoadAll()){
                        foreach($arrBranch as $bankBranch){
                            $this->lstAutoComplete2->AddItem('[ '.$bankBranch->BranchCode.' ] '.$bankBranch->BranchName, $bankBranch->BranchCode);
                        }
                    }
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->hide();
//						$this->datePicker2->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'role'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');

                    if($arrRole = RoleUser::LoadAll()){
                        foreach($arrRole as $role){
                            $this->lstAutoComplete2->AddItem('[ '.$role->RoleLevel.' ] '.$role->RoleName, $role->RoleId);
                        }
                    }
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->hide();
//						$this->datePicker2->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'author'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');
                    $this->lstAutoComplete2->AddItem('CREATED AUTHOR','C');
                    $this->lstAutoComplete2->AddItem('COMPLETED AUTHOR','A');
                    $this->lstAutoComplete2->AddItem('REJECTED AUTHOR', 'R');
//						$this->lstAutoComplete2->AddItem('DELETED', 'D');
                    $this->lstAutoComplete2->AddItem('UPDATED AUTHOR', 'U');
                    $this->lstAutoComplete2->AddItem('NOT AUTHOR', 'BLANK');
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->hide();
//						$this->datePicker2->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'lock'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');
                    $this->lstAutoComplete2->AddItem('Y',TRUE);
                    $this->lstAutoComplete2->AddItem('N',FALSE);
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->hide();
//						$this->datePicker1->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'name'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->hide();
                    $this->textSearch2->unhide();
                    $this->textSearch2->Text = '';
                }elseif($this->lstParam2->SelectedValue == 'nik'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->hide();
                    $this->textSearch2->unhide();
                    $this->textSearch2->Text = '';
                }else{
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');
                    $this->textSearch2->Text = '';
                    $this->dtgUserses->Refresh();
                }
                break;
        }
    }

    public function PageBinder(){

        sleep(1);

        $objCondition = array();

        $param1 = $this->lstParam1->SelectedValue;
        $param2 = $this->lstParam2->SelectedValue;

        if ($param1 == 'name' or $param1 == 'nik'){
            $val1 = $this->textSearch1->Text;
        }/*elseif ($param1 == 'lastlogin'){
            if ($this->datePicker1->Input->Text) {
                $exval1 = explode(' - ',$this->datePicker1->Input->Text);
                if (count($exval1) > 1) {
                    $val1[0] = $exval1[0];
                    $val1[1] = $exval1[1];
                }else{
                    $val1[0] = $exval1[0];
                    $val1[1] = $exval1[0];
                }
            }else{
                $val1 = NULL;
            }
        }*/else{
            $val1 = $this->lstAutoComplete->SelectedValue;
        }

        if ($param2 == 'name' or $param2 == 'nik'){
            $val2 = $this->textSearch2->Text;
        }/*elseif ($param2 == 'lastlogin'){
            if ($this->datePicker2->Input->Text) {
                $exval2 = explode(' - ',$this->datePicker2->Input->Text);
                if (count($exval2) > 1) {
                    $val2[0] = $exval2[0];
                    $val2[1] = $exval2[1];
                }else{
                    $val2[0] = $exval2[0];
                    $val2[1] = $exval2[0];
                }
            }else{
                $val2 = NULL;
            }
        }*/else{
            $val2 = $this->lstAutoComplete2->SelectedValue;
        }

        $y1 = ''; $y2 = ''; $q1 = ''; $q2 = '';

        switch ($param1) {
            case 'bankBranch':      $x1 = QQN::AuthUsers()->BranchCode;                 $y1 = 'fixed_value';    break;
            case 'name':        	$x1 = QQN::AuthUsers()->Username;                  	$y1 = 'free_text';      break;
            case 'nik':             $x1 = QQN::AuthUsers()->Nik;                 		$y1 = 'fixed_text';     break;
            case 'role':    		$x1 = QQN::AuthUsers()->RoleId;             		$y1 = 'fixed_value';    break;
            case 'author':        	$x1 = QQN::AuthUsers()->RecordStatus;               $y1 = 'fixed_value';    break;
            case 'lock':        	$x1 = QQN::AuthUsers()->IsLocked;     	        	$y1 = 'fixed_value';    break;
        }

        switch ($param2) {
            case 'bankBranch':      $x2 = QQN::AuthUsers()->BranchCode;                 $x2 = 'fixed_value';    break;
            case 'name':        	$x2 = QQN::AuthUsers()->Username;                  	$x2 = 'free_text';      break;
            case 'nik':             $x2 = QQN::AuthUsers()->Nik;                 		$x2 = 'fixed_text';     break;
            case 'role':    		$x2 = QQN::AuthUsers()->RoleId;             		$x2 = 'fixed_value';    break;
            case 'author':        	$x2 = QQN::AuthUsers()->RecordStatus;               $x2 = 'fixed_value';    break;
            case 'lock':        	$x2 = QQN::AuthUsers()->IsLocked;     	        	$x2 = 'fixed_value';    break;
        }

        switch ($y1) {
            case 'free_text'    : $q1 = QQ::Like($x1, strtoupper('%'.$val1.'%'));                                           break;
            case 'fixed_text'   : $q1 = QQ::Equal($x1, $val1);                                                              break;
            case 'with_null'    : $q1 = ($val1 == 'BLANK') ? QQ::isNull($x1) : QQ::Equal($x1, $val1);                       break;
            case 'fixed_value'  : $q1 = QQ::Equal($x1, $val1);                                                              break;
        }

        switch ($y2) {
            case 'free_text'    : $q2 = QQ::Like($x2, strtoupper('%'.$val2.'%'));                                           break;
            case 'fixed_text'   : $q2 = QQ::Equal($x2, $val2);                                                              break;
            case 'with_null'    : $q2 = ($val2 == 'BLANK') ? QQ::isNull($x2) : QQ::Equal($x2, $val2);                       break;
            case 'fixed_value'  : $q2 = QQ::Equal($x2, $val2);                                                              break;
        }

        $condition = "";
        if     ($val1 != NULL && $val2 != NULL) { $condition = array_push($objCondition, $q1, $q2);     }
        elseif ($val1 != NULL && $val2 == NULL) { $condition = array_push($objCondition, $q1);          }
        elseif ($val1 == NULL && $val2 != NULL) { $condition = array_push($objCondition, $q2);          }
        else                                    { $condition = array_push($objCondition, QQ::All());    }

        $this->dtgUserses->MetaDataBinderCustom(new AuthUsers(), QQ::AndCondition($objCondition));

//			$this->dtgUserss->MetaDataBinderCustom(new Users(), QQ::AndCondition($objCondition), array(QQ::GroupBy(array(QQN::Users()->UserId, QQN::Users()->RbaFinalRiskTypeCode)),QQ::OrderBy(QQ::NotEqual(QQN::Users()->BranchCode, $objUser->BranchCode), QQN::Users()->FirstName)/*, QQ::LimitInfo(1000)*/));
    }

    public function locked($word){
        switch($word){
            case true:
                $wordNew = 'Terkunci';
                break;
            case false:
                $wordNew = 'Aktif';
                break;
            default :
                $wordNew = 'Terkunci';
                break;
        }
        return $wordNew;
    }

    public function is_active($word){
        switch($word){
            case true:
                $wordNew = 'Aktif';
                break;
            default :
                $wordNew = 'Tidak Aktif';
                break;
        }
        return $wordNew;
    }

    public function branch_null($word){
        switch($word){
            case null:
                $wordNew = 'Kantor Pusat';
                break;
            default :
                $wordNew = '-';
                break;
        }
        return $wordNew;
    }
}
UsersListForm::Run('UsersListForm');
?>