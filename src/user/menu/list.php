<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

	class DynamicMenuListForm extends QForm {

        protected $dtgDynamicMenus;
        protected $objLinkProxy;

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->dtgDynamicMenus = new DynamicMenuDataGrid($this);

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

            // Add Pagination (if desired)
            $this->dtgDynamicMenus->Paginator = new QPaginator($this->dtgDynamicMenus);
            $this->dtgDynamicMenus->ItemsPerPage = 10;

            $this->dtgDynamicMenus->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgDynamicMenus->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM)?>','HtmlEntities=false', 'Width=70'));
            $this->dtgDynamicMenus->MetaAddColumn('Title');
            $this->dtgDynamicMenus->MetaAddColumn('Description');

            $this->dtgDynamicMenus->SetDataBinder('PageBinder', $this);
        }

        public function rowActionButton($objRecord)
        {
            $strReturn = null;

            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="View"><span class="fa fa-pencil"></span></a>',
                __SOURCE__ . '/user/menu/form.php/'.$objRecord->Id);
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Delete" onclick="return confirm(\'Konfirmasi hapus Data User %s ?\')">
                          <span class="fa fa-trash-o"></span></a>',$this->objLinkProxy->RenderAsHref('del_'.$objRecord->Id,false),$objRecord->Title);

            return $strReturn;
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            switch($section)
            {
                case'del':
                    $objMenu = DynamicMenu::Load($param);
                    $objMenu->Delete();
                    QApplication::DisplayAlert('Menu ' . $objMenu->Title . ' has been Removed');
                    $this->dtgDynamicMenus->Refresh();
                    break;
            }
        }

        public function PageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            if($searchText = $this->dtgDynamicMenus->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::DynamicMenu()->Title, '%' . $searchText . '%'),
                    QQ::Like(QQN::DynamicMenu()->Description, '%' . $searchText . '%')
                ));
            }

            $this->dtgDynamicMenus->MetaDataBinderCustom(new DynamicMenu(), QQ::AndCondition($objCondition));
        }
	}

	DynamicMenuListForm::Run('DynamicMenuListForm');
?>