<?php
	require(__CONFIGURATION__ . '/header.inc.php');
?>
	<?php $this->RenderBegin() ?>
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header with-border">
                <h3><?php _p($this->mctDynamicMenu->TitleVerb); ?> <?php _t('DynamicMenu')?></h3>
            </div>
            <div class="box-body">
                <?php $this->txtTitle->RenderWithBootstrap(3,5); ?>
                <?php $this->txtUrl->RenderWithBootstrap(3,5); ?>
                <?php $this->chkIsActive->RenderWithBootstrap(3,5); ?>
                <?php $this->txtIcon->RenderWithBootstrap(3,5); ?>
                <?php $this->txtDescription->RenderWithBootstrap(3,5); ?>
                <?php $this->chkIsParent->RenderWithBootstrap(3,5); ?>
                <?php $this->lstParentId->RenderWithBootstrap(3,5); ?>
            </div>

            <div class="box-footer">
                <?php $this->btnSave->Render(); ?>
                <?php $this->btnCancel->Render(); ?>
                <?php $this->btnDelete->Render(); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>