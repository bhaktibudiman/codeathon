<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

	class DynamicMenuEditForm extends QForm {

        protected $mctDynamicMenu;
        protected $txtId;
        protected $txtTitle;
        protected $txtUrl;
        protected $chkIsActive;
        protected $txtIcon;
        protected $txtDescription;
        protected $chkIsParent;
        protected $txtParentId;
        protected $lstParentId;

        protected $btnSave;
        protected $btnDelete;
        protected $btnCancel;

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            // Use the CreateFromPathInfo shortcut (this can also be done manually using the DynamicMenuMetaControl constructor)
            // MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
            $this->mctDynamicMenu = DynamicMenuMetaControl::CreateFromPathInfo($this);

            // Call MetaControl's methods to create qcontrols based on DynamicMenu's data fields
            $this->txtId = $this->mctDynamicMenu->txtId_Create();
            $this->txtTitle = $this->mctDynamicMenu->txtTitle_Create();
            $this->txtUrl = $this->mctDynamicMenu->txtUrl_Create();
            $this->chkIsActive = $this->mctDynamicMenu->chkIsActive_Create();
            $this->txtIcon = $this->mctDynamicMenu->txtIcon_Create();
            $this->txtDescription = $this->mctDynamicMenu->txtDescription_Create();
            $this->txtParentId = $this->mctDynamicMenu->txtParentId_Create();
            $this->chkIsParent = $this->mctDynamicMenu->chkIsParent_Create();
            $this->chkIsParent->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

            $this->lstParentId = new QListBox($this);
            $this->lstParentId->Name = "Parent Menu";
            $this->lstParentId->AddItem("--SELECT ONE--");
            $arr = DynamicMenu::LoadByParentMenu();
            foreach($arr as $key){
                $select = ($this->txtParentId->Text==$key->Id)?TRUE:NULL;
                $this->lstParentId->AddItem($key->Title, $key->Id,$select);
            }

            if($this->chkIsParent->Checked == true){
                $this->lstParentId->Hide();
            }else{
                $this->lstParentId->UnHide();
            }

            // Create Buttons and Actions on this Form
            $this->btnSave = new QButton($this);
            $this->btnSave->Text = QApplication::Translate('Save');
            $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
            $this->btnSave->CausesValidation = true;
            $this->btnSave->CssClass = 'btn btn-primary';

            $this->btnCancel = new QButton($this);
            $this->btnCancel->Text = QApplication::Translate('Cancel');
            $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
            $this->btnCancel->CssClass = 'btn btn-warning';

            $this->btnDelete = new QButton($this);
            $this->btnDelete->Text = QApplication::Translate('Delete');
            $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE you want to DELETE this %s?'), QApplication::Translate('DynamicMenu'))));
            $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
            $this->btnDelete->Visible = $this->mctDynamicMenu->EditMode;
            $this->btnDelete->CssClass = 'btn btn-danger';
        }

        /**
         * This Form_Validate event handler allows you to specify any custom Form Validation rules.
         * It will also Blink() on all invalid controls, as well as Focus() on the top-most invalid control.
         */
        protected function Form_Validate() {
            // By default, we report the result of validation from the parent
            $blnToReturn = parent::Form_Validate();

            // Custom Validation Rules
            // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

            $blnFocused = false;
            foreach ($this->GetErrorControls() as $objControl) {
                // Set Focus to the top-most invalid control
                if (!$blnFocused) {
                    $objControl->Focus();
                    $blnFocused = true;
                }

                // Blink on ALL invalid controls
                $objControl->Blink();
            }

            return $blnToReturn;
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if($this->chkIsParent->Checked == true){
                $this->lstParentId->Hide();
            }else{
                $this->lstParentId->UnHide();
            }
        }

        // Button Event Handlers

        protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Save" processing to the DynamicMenuMetaControl
            if(!$this->mctDynamicMenu->EditMode){
                $this->txtId->Text = System::GetId();
            }
            if($this->lstParentId->SelectedValue != ''){
                $this->txtParentId->Text = $this->lstParentId->SelectedValue;
            }

            $this->mctDynamicMenu->SaveDynamicMenu();
            $this->RedirectToListPage();
        }

        protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Delete" processing to the DynamicMenuMetaControl
            $this->mctDynamicMenu->DeleteDynamicMenu();
            $this->RedirectToListPage();
        }

        protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
            $this->RedirectToListPage();
        }

        // Other Methods

        protected function RedirectToListPage() {
            QApplication::Redirect(__SOURCE__ . '/user/menu/list.php');
        }
	}

	// Go ahead and run this form object to render the page and its event handlers, implicitly using
	// dynamic_menu_edit.tpl.php as the included HTML template file
	DynamicMenuEditForm::Run('DynamicMenuEditForm');
?>