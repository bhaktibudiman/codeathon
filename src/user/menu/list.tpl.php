<?php
	require(__CONFIGURATION__ . '/header.inc.php');
?>

	<?php $this->RenderBegin() ?>
    <section class="content-header">
        <h1>List Menu</h1>
    </section>

    <div class="box box-primary">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <div class="col-md-1">
                    <a href="form.php" class="btn btn-success" title="Add"><span class="fa fa-plus"></span> Add</a>
                </div>
                <?php $this->dtgDynamicMenus->Render(); ?>
            </div>
        </div>
    </div>

	<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>