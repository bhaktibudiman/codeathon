<?php
	$strPageTitle = QApplication::Translate('Role Users') . ' - ' . QApplication::Translate('List All');
	require(__CONFIGURATION__ . '/header.inc.php');
?>

	<?php $this->RenderBegin() ?>

    <section class="content-header">
        <h1>List Role</h1>
    </section>

    <div class="box">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <div class="col-md-1">
                    <a href="form.php" class="btn btn-success"><span class="fa fa-plus"></span> Add</a>
                </div>
                <?php $this->dtgRoleUsers->Render(); ?>
            </div>
        </div>
    </div>

	<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>