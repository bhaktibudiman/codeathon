<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

class RoleUserEditForm extends QForm {
    protected $mctRoleUser;

    // Controls for RoleUser's Data Fields
    protected $txtRoleId;
    protected $txtRoleName;
    protected $txtDescription;
    protected $txtCreatedBy;
    protected $calCreatedDate;
    protected $txtUpdateBy;
    protected $calUpdatedDate;
    protected $txtRoleLevel;

    // Other ListBoxes (if applicable) via Unique ReverseReferences and ManyToMany References
    protected $dtgDynamicMenusAsRoleMenu;

    // Other Controls
    /**
     * @var QButton Save
     */
    protected $btnSave;
    /**
     * @var QButton Delete
     */
    protected $btnDelete;
    /**
     * @var QButton Cancel
     */
    protected $btnCancel;


    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();

        // Use the CreateFromPathInfo shortcut (this can also be done manually using the RoleUserMetaControl constructor)
        // MAKE SURE we specify "$this" as the MetaControl's (and thus all subsequent controls') parent
        $this->mctRoleUser = RoleUserMetaControl::CreateFromPathInfo($this);

        // Call MetaControl's methods to create qcontrols based on RoleUser's data fields
        $this->txtRoleId = $this->mctRoleUser->txtRoleId_Create();
        $this->txtRoleName = $this->mctRoleUser->txtRoleName_Create();
        $this->txtDescription = $this->mctRoleUser->txtDescription_Create();
        $this->txtCreatedBy = $this->mctRoleUser->txtCreatedBy_Create();
        $this->calCreatedDate = $this->mctRoleUser->calCreatedDate_Create();
        $this->txtUpdateBy = $this->mctRoleUser->txtUpdatedBy_Create();
        $this->calUpdatedDate = $this->mctRoleUser->calUpdatedDate_Create();
        $this->txtRoleLevel = $this->mctRoleUser->txtRoleLevel_Create();

        $this->dtgDynamicMenusAsRoleMenu = $this->mctRoleUser->dtgDynamicMenusAsRoleMenu_Create();

        // Create Buttons and Actions on this Form
        $this->btnSave = new QButton($this);
        $this->btnSave->Text = QApplication::Translate('Save');
        $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
        $this->btnSave->CausesValidation = true;
        $this->btnSave->CssClass = 'btn btn-primary';

        $this->btnCancel = new QButton($this);
        $this->btnCancel->Text = QApplication::Translate('Cancel');
        $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
        $this->btnCancel->CssClass = 'btn btn-warning';

        $this->btnDelete = new QButton($this);
        $this->btnDelete->Text = QApplication::Translate('Delete');
        $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE you want to DELETE this %s?'), QApplication::Translate('RoleUser'))));
        $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
        $this->btnDelete->Visible = $this->mctRoleUser->EditMode;
        $this->btnDelete->CssClass = 'btn btn-danger';
    }

    /**
     * This Form_Validate event handler allows you to specify any custom Form Validation rules.
     * It will also Blink() on all invalid controls, as well as Focus() on the top-most invalid control.
     */
    protected function Form_Validate() {
        // By default, we report the result of validation from the parent
        $blnToReturn = parent::Form_Validate();

        // Custom Validation Rules
        // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            // Set Focus to the top-most invalid control
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            // Blink on ALL invalid controls
            $objControl->Blink();
        }

        return $blnToReturn;
    }

    // Button Event Handlers

    protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
        // Delegate "Save" processing to the RoleUserMetaControl
        if(!$this->mctRoleUser->EditMode){
            $this->txtRoleId->Text = System::GetId();
            $this->txtCreatedBy->Text = $_SESSION[__USER_LOGIN__];
            $this->calCreatedDate->DateTime = QDateTime::Now();
        }else{
            $this->txtUpdateBy->Text = $_SESSION[__USER_LOGIN__];
            $this->calUpdatedDate->DateTime = QDateTime::Now();
        }

        $this->mctRoleUser->SaveRoleUser();
        $this->RedirectToListPage();
    }

    protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
        // Delegate "Delete" processing to the RoleUserMetaControl
        $this->mctRoleUser->DeleteRoleUser();
        $this->RedirectToListPage();
    }

    protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
        $this->RedirectToListPage();
    }

    // Other Methods

    protected function RedirectToListPage() {
        QApplication::Redirect(__SOURCE__ . '/user/role/list.php');
    }
}

// Go ahead and run this form object to render the page and its event handlers, implicitly using
// role_user_edit.tpl.php as the included HTML template file
RoleUserEditForm::Run('RoleUserEditForm');
?>