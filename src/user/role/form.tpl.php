<?php
	require(__CONFIGURATION__ . '/header.inc.php');
?>
	<?php $this->RenderBegin() ?>
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header with-border">
                <h3><?php _p($this->mctRoleUser->TitleVerb); ?> <?php _t('Role User')?></h3>
            </div>
            <div class="box-body">
                <?php $this->txtRoleName->RenderWithBootstrap(2,3); ?>
                <?php $this->txtDescription->RenderWithBootstrap(2,3); ?>
                <?php $this->txtRoleLevel->RenderWithBootstrap(2,3); ?>
                <?php $this->dtgDynamicMenusAsRoleMenu->RenderWithName(true); ?>
            </div>

            <div class="box-footer">
                <?php $this->btnSave->Render(); ?>
                <?php $this->btnCancel->Render(); ?>
                <?php $this->btnDelete->Render(); ?>
            </div>
        </div>
    </div>
</div>

	<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>