<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

    class RoleUserListForm extends QForm {
        protected $dtgRoleUsers;
        protected $objLinkProxy;

        protected function Form_Create() {
            parent::Form_Create();

            // Instantiate the Meta DataGrid
            $this->dtgRoleUsers = new RoleUserDataGrid($this);

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

            // Add Pagination (if desired)
            $this->dtgRoleUsers->Paginator = new QPaginator($this->dtgRoleUsers);
            $this->dtgRoleUsers->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
            $this->dtgRoleUsers->ShowFilter = false;

            // Use the MetaDataGrid functionality to add Columns for this datagrid

            // Create an Edit Column
            $this->dtgRoleUsers->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgRoleUsers->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM)?>','HtmlEntities=false', 'Width=70'));
            $this->dtgRoleUsers->MetaAddColumn('RoleName');
            $this->dtgRoleUsers->MetaAddColumn('Description');

            $this->dtgRoleUsers->SetDataBinder("PageBinder", $this);
        }

        public function rowActionButton($objRecord)
        {
            $strReturn = null;

            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="View"><span class="fa fa-pencil"></span></a>',
                __SOURCE__ . '/user/role/form.php/'.$objRecord->RoleId);
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Delete" onclick="return confirm(\'Konfirmasi hapus Data User %s ?\')">
                          <span class="fa fa-trash-o"></span></a>',$this->objLinkProxy->RenderAsHref('del_'.$objRecord->RoleId,false),$objRecord->RoleName);

            return $strReturn;
        }

        public function PageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            if($searchText = $this->dtgRoleUsers->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::RoleUser()->RoleName, '%' . $searchText . '%'),
                    QQ::Like(QQN::RoleUser()->Description, '%' . $searchText . '%')
                ));
            }

            $this->dtgRoleUsers->MetaDataBinderCustom(new RoleUser(), QQ::AndCondition($objCondition));
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            switch($section)
            {
                case'del':
                    $objRole = RoleUser::Load($param);
                    $objRole->Delete();
                    QApplication::DisplayAlert('Menu ' . $objRole->RoleName . ' has been Removed');
                    $this->dtgRoleUsers->Refresh();
                    break;
            }
        }
	}

	// Go ahead and run this form object to generate the page and event handlers, implicitly using
	// role_user_list.tpl.php as the included HTML template file
	RoleUserListForm::Run('RoleUserListForm');
?>