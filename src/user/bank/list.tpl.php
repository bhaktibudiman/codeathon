<?php
	require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

<section class="content-header">
    <h1><?php _t('Listing All'); ?> <?php _t('Bank Branches'); ?></h1>
</section>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="dataTables_wrapper form-inline">
                    <div class="col-xs-6">
                        <a href="form.php" class="btn btn-success"><span class="fa fa-plus"> Create New</span></a>
                    </div>
                    <?php $this->dtgBankBranches->Render(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>