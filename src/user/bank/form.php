<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

	class BankBranchEditForm extends QForm {
        protected $mctBankBranch;
        protected $objBankBranch;
        protected $objAddress;
        protected $objCustomer;

        protected $lblBankBranchId;
        protected $txtBranchCode;
        protected $txtBranchName;
        protected $txtBranchPrarentId;
        protected $txtBranchAddress1;
        protected $txtBranchAddress2;
        protected $txtHeadName;
        protected $txtPhone1;
        protected $txtPhone2;
        protected $txtHeadPhone;
        protected $txtDistrictId;
        protected $txtProvinceId;
        protected $txtPostCode;
        protected $txtCountryId;
        protected $chkIsBranchActive;
        protected $calLastRecordedDate;
        protected $txtCreatedBy;
        protected $calCreatedOn;
        protected $txtUpdatedBy;
        protected $calUpdatedOn;

        protected $btnSave;
        protected $btnDelete;
        protected $btnCancel;

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->mctBankBranch = BankBranchMetaControl::CreateFromPathInfo($this);
//            $this->objBankBranch = BankBranch::Load(QApplication::PathInfo(0));
//            $this->objCustomer = BasicCustomer::Load($this->objCustomer->BankBranchId);
//            $this->objAddress = BasicAddress::LoadByIDCust($this->objCustomer->BankBranchId);

            // Call MetaControl's methods to create qcontrols based on BankBranch's data fields
            // $this->lblBankBranchId = $this->mctBankBranch->lblBankBranchId_Create();
            $this->txtBranchCode = $this->mctBankBranch->txtBranchCode_Create();
            $this->txtBranchName = $this->mctBankBranch->txtBranchName_Create();
            // $this->txtBranchPrarentId = $this->mctBankBranch->txtBranchPrarentId_Create();
            $this->txtBranchAddress1 = $this->mctBankBranch->txtBranchAddress1_Create();
            $this->txtBranchAddress1->TextMode = QTextMode::MultiLine;
            $this->txtBranchAddress2 = $this->mctBankBranch->txtBranchAddress2_Create();
            $this->txtBranchAddress2->TextMode = QTextMode::MultiLine;
            $this->txtHeadName = $this->mctBankBranch->txtHeadName_Create();
            $this->txtPhone1 = $this->mctBankBranch->txtPhone1_Create();
            $this->txtPhone2 = $this->mctBankBranch->txtPhone2_Create();
            $this->txtHeadPhone = $this->mctBankBranch->txtHeadPhone_Create();

            $this->txtDistrictId = $this->mctBankBranch->txtDistrictId_Create();
//            $this->txtDistrictId = new QListBox($this);
//            $this->txtDistrictId->Name = 'Kelurahan';
//            $this->txtDistrictId->AddItem("--SELECT ONE--");
//            if($this->objAddress->DistrictId){
//                $prov = ApuRegion::LoadArrayByRegionType(2,$objOptionalClauses = null);
//                foreach($prov as $key)
//                {
//                    $select = ($key->RegionId=$this->objAddress->DistrictId);
//                    $this->txtDistrictId->AddItem($key->RegionName, $key->RegionId,$select);
//                }
//
//            }

            $this->txtProvinceId = $this->mctBankBranch->txtProvinceId_Create();
            $this->txtPostCode = $this->mctBankBranch->txtPostCode_Create();
            $this->txtCountryId = $this->mctBankBranch->txtCountryId_Create();
            $this->chkIsBranchActive = $this->mctBankBranch->chkIsBranchActive_Create();
            $this->calLastRecordedDate = $this->mctBankBranch->calLastRecordedDate_Create();
            $this->txtCreatedBy = $this->mctBankBranch->txtCreatedBy_Create();
            $this->calCreatedOn = $this->mctBankBranch->calCreatedOn_Create();
            $this->txtUpdatedBy = $this->mctBankBranch->txtUpdatedBy_Create();
            $this->calUpdatedOn = $this->mctBankBranch->calUpdatedOn_Create();

            // Create Buttons and Actions on this Form
            $this->btnSave = new QButton($this);
            $this->btnSave->Text = QApplication::Translate('Save');
            $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
            $this->btnSave->CausesValidation = true;
            $this->btnSave->CssClass = 'btn btn-primary';

            $this->btnCancel = new QButton($this);
            $this->btnCancel->Text = QApplication::Translate('Cancel');
            $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
            $this->btnCancel->CssClass = 'btn btn-warning';

            $this->btnDelete = new QButton($this);
            $this->btnDelete->Text = QApplication::Translate('Delete');
            $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE you want to DELETE this %s?'), QApplication::Translate('BankBranch'))));
            $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
            $this->btnDelete->Visible = $this->mctBankBranch->EditMode;
            $this->btnDelete->CssClass = 'btn btn-danger';
        }

        /**
         * This Form_Validate event handler allows you to specify any custom Form Validation rules.
         * It will also Blink() on all invalid controls, as well as Focus() on the top-most invalid control.
         */
        protected function Form_Validate() {
            // By default, we report the result of validation from the parent
            $blnToReturn = parent::Form_Validate();

            // Custom Validation Rules
            // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

            $blnFocused = false;
            foreach ($this->GetErrorControls() as $objControl) {
                // Set Focus to the top-most invalid control
                if (!$blnFocused) {
                    $objControl->Focus();
                    $blnFocused = true;
                }

                // Blink on ALL invalid controls
                $objControl->Blink();
            }

            return $blnToReturn;
        }

        // Button Event Handlers

        protected function btnSave_Click($strFormId, $strControlId, $strParameter) {

            if(!$this->mctBankBranch->EditMode){
                // $this->lblBankBranchId->Text = System::GetId();
                $this->calCreatedOn->DateTime = QDateTime::Now();
                $this->txtCreatedBy->Text = $_SESSION[__USER_LOGIN__];
            }else{
                $this->calUpdatedOn->DateTime = QDateTime::Now();
                $this->txtUpdatedBy->Text = $_SESSION[__USER_LOGIN__];
            }

            $this->mctBankBranch->SaveBankBranch();
            $this->RedirectToListPage();
        }

        protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Delete" processing to the BankBranchMetaControl
            $this->mctBankBranch->DeleteBankBranch();
            $this->RedirectToListPage();
        }

        protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
            $this->RedirectToListPage();
        }

        // Other Methods

        protected function RedirectToListPage() {
            QApplication::Redirect(__SOURCE__ . '/user/bank/list.php');
        }
	}

	// Go ahead and run this form object to render the page and its event handlers, implicitly using
	// bank_branch_edit.tpl.php as the included HTML template file
	BankBranchEditForm::Run('BankBranchEditForm');
?>