<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

	class BankBranchListForm extends QForm {
        protected $dtgBankBranches;
        protected $objLinkProxy;

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->dtgBankBranches = new BankBranchDataGrid($this);

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

            $this->dtgBankBranches->Paginator = new QPaginator($this->dtgBankBranches);
            $this->dtgBankBranches->ItemsPerPage = 10;

            $this->dtgBankBranches->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgBankBranches->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM)?>','HtmlEntities=false', 'Width=70'));
            $this->dtgBankBranches->AddColumn(new QDataGridColumn('Branch Code', '<?= $_ITEM->BranchCode ?>'));
            $this->dtgBankBranches->AddColumn(new QDataGridColumn('Branch Name', '<?= $_ITEM->BranchName ?>'));
            $this->dtgBankBranches->AddColumn(new QDataGridColumn('Address', '<?= $_ITEM->BranchAddress1 ?>'));
            $this->dtgBankBranches->AddColumn(new QDataGridColumn('Province', '<?= $_ITEM->ProvinceId ?>'));

            $this->dtgBankBranches->SetDataBinder('pageBinder');
        }

        protected function pageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            if($search = $this->dtgBankBranches->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::BankBranch()->BranchName, '%'.$search.'%'),
                    QQ::Like(QQN::BankBranch()->BranchAddress1, '%'.$search.'%')
                ));
            }

            $this->dtgBankBranches->MetaDataBinderCustom(new BankBranch(), QQ::AndCondition($objCondition));
        }

        public function rowActionButton($objRecord)
        {
            $strReturn = null;
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="Detail Bank Branch"><span class="fa fa-pencil"></span></a>',
                __SOURCE__ . '/user/bank/form.php/'.$objRecord->BranchCode);
            $strReturn .= " ";
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Delete" onclick="return confirm(\'Konfirmasi hapus Data Bank Branch %s ?\')">
                          <span class="fa fa-trash-o"></span></a>',$this->objLinkProxy->RenderAsHref('del_'.$objRecord->BranchCode,false),$objRecord->BranchCode);

            return $strReturn;
        }
	}

	// Go ahead and run this form object to generate the page and event handlers, implicitly using
	// bank_branch_list.tpl.php as the included HTML template file
	BankBranchListForm::Run('BankBranchListForm');
?>