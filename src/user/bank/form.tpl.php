<?php
	require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>

<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                <h3><?php _p($this->mctBankBranch->TitleVerb); ?> <?php _t('Bank Branch')?></h3>
            </div>

            <div class="box-body">
                <div class="form-controls">
                    <?php $this->txtBranchCode->RenderWithBootstrap(3,3); ?>
                    <?php $this->txtBranchName->RenderWithBootstrap(3,7); ?>
                    <?php //$this->txtBranchPrarentId->RenderWithBootstrap(3,2); ?>
                    <?php $this->txtBranchAddress1->RenderWithBootstrap(3,7); ?>
                    <?php $this->txtBranchAddress2->RenderWithBootstrap(3,7); ?>
                    <?php $this->txtHeadName->RenderWithBootstrap(3,5); ?>
                    <?php $this->txtPhone1->RenderWithBootstrap(3,3); ?>
                    <?php $this->txtPhone2->RenderWithBootstrap(3,3); ?>
                    <?php $this->txtHeadPhone->RenderWithBootstrap(3,3); ?>
                    <?php $this->txtDistrictId->RenderWithBootstrap(3,2); ?>
                    <?php $this->txtProvinceId->RenderWithBootstrap(3,2); ?>
                    <?php $this->txtPostCode->RenderWithBootstrap(3,2); ?>
                    <?php $this->txtCountryId->RenderWithBootstrap(3,3); ?>
                    <?php $this->chkIsBranchActive->RenderWithBootstrap(3,6); ?>
                    <?php $this->calLastRecordedDate->RenderWithBootstrap(3,6); ?>
                </div>
            </div>

            <div class="box-footer">
                <div class="form-actions">
                    <?php $this->btnSave->Render(); ?>
                    <?php $this->btnCancel->Render(); ?>
                    <?php $this->btnDelete->Render(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

	<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>