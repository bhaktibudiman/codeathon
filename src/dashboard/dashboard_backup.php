<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');


	class CashTransactionListForm extends QForm {
        protected $datePickerReport;
        protected $btnFilter;
        protected $dtgCashTransactions;
        protected $objLinkProxy;
        protected $pryLinks;
        protected $lstStatus1;
        protected $lstStatus2;

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->dtgCashTransactions = new CashTransactionDataGrid($this);

            $this->dtgCashTransactions->Paginator = new QPaginator($this->dtgCashTransactions);
            $this->dtgCashTransactions->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;

            $this->lstStatus1 = new QListBox($this);
            $this->lstStatus1->Name = 'Status 1';
            $this->lstStatus1->AddItem('- Any Status -');
            $this->lstStatus1->AddItem('Report Status','reportStatus');
            $this->lstStatus1->AddItem('Transaction Status','transactionStatus');
            $this->lstStatus1->ActionParameter = 'changeStatus1';
            $this->lstStatus1->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

            $this->lstStatus2 = new QListBox($this);
            $this->lstStatus2->Name = 'Status 2';
            $this->lstStatus2->AddItem('-Any-');
            $this->lstStatus2->ActionParameter = 'changeStatus2';
            $this->lstStatus2->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));
            $this->lstStatus2->Hide();

            $this->dtgCashTransactions->ShowFilter = true;
            $this->dtgCashTransactions->ShowFilterButton = false;
            $this->dtgCashTransactions->ShowFilterResetButton = false;
            $this->dtgCashTransactions->Width = '1500';

            $this->dtgCashTransactions->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=15'));

            $this->dtgCashTransactions->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM->CashTransactionId)
                ?>','HtmlEntities=false','Width=50'));

            $tglTransaksi = new QDataGridColumn('Tanggal', '<?= date_format($_ITEM->TrxValueDate, "d M Y") ?>');
            $tglTransaksi->OrderByClause = QQ::OrderBy(QQN::CashTransaction()->TrxValueDate);
            $tglTransaksi->ReverseOrderByClause = QQ::OrderBy(QQN::CashTransaction()->TrxValueDate, false);
            QQN::CashTransaction()->TrxValueDate->SetFilteredDataGridColumnFilter($tglTransaksi);
            $this->dtgCashTransactions->AddColumn($tglTransaksi);

            $cif = new QDataGridColumn('CIF', '<?= $_ITEM->CustomerId ?>');
            $cif->OrderByClause = QQ::OrderBy(QQN::CashTransaction()->CustomerId);
            $cif->ReverseOrderByClause = QQ::OrderBy(QQN::CashTransaction()->CustomerId, false);
            $cif->Filter = QQ::Like(QQN::CashTransaction()->CustomerId, null);
            $cif->FilterPrefix = $cif->FilterPostfix = '%';
            $cif->FilterBoxSize = 15;
            $cif->FilterType = QFilterType::TextFilter;
            $this->dtgCashTransactions->AddColumn($cif);

            $firstName = new QDataGridColumn('Customer Name', '<?= $_ITEM->FirstName ?>');
            $firstName->OrderByClause = QQ::OrderBy(QQN::CashTransaction()->FirstName);
            $firstName->ReverseOrderByClause = QQ::OrderBy(QQN::CashTransaction()->FirstName, false);
            $firstName->Filter = QQ::Like(QQN::CashTransaction()->FirstName, null);
            $firstName->FilterPrefix = $firstName->FilterPostfix = '%';
            $firstName->FilterBoxSize = 30;
            $firstName->FilterType = QFilterType::TextFilter;
            $this->dtgCashTransactions->AddColumn($firstName);

            $bankBranch = new QDataGridColumn('Bank Branch', '<?= $_FORM->getBranch($_ITEM->BankBranchId) ?>');
            $this->dtgCashTransactions->AddColumn($bankBranch);

            $workStatus = new QDataGridColumn('Worked Status', '<?= $_FORM->WorkedStatus($_ITEM->WorkedStatus) ?>');
            $this->dtgCashTransactions->AddColumn($workStatus);

            $this->dtgCashTransactions->AddColumn(new QDataGridColumn('Amount', '<?="Rp. " .number_format($_ITEM->TrxAmount,0,",",".") ?>','HtmlEntities=false','HorizontalAlign='.QHorizontalAlign::Right));

            $reportStatus = new QDataGridColumn('Report Status', '<?= $_FORM->getReportStatus($_ITEM->ReportStatus) ?>');
            $this->dtgCashTransactions->AddColumn($reportStatus);

            $transactionStatus = new QDataGridColumn('Transaction Status', '<?= $_FORM->getTrxStatus($_ITEM->TransactionStatus) ?>');
            $this->dtgCashTransactions->AddColumn($transactionStatus);

            $this->datePickerReport = new QDateRangePicker($this);
            $this->datePickerReport->Input = new QTextBox($this);
            $this->datePickerReport->Input->Name = "Mulai";
            $this->datePickerReport->SecondInput = new QTextBox($this);
            $this->datePickerReport->SecondInput->Name = "Sampai";
            $this->datePickerReport->CloseOnSelect= true;
            $this->datePickerReport->JqDateFormat = 'yy-mm-dd';

            $this->btnFilter = new QButton($this);
            $this->btnFilter->Text = QApplication::Translate('Filter');
            $this->btnFilter->CssClass = 'btn btn-primary';
            $this->btnFilter->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));
            $this->btnFilter->ActionParameter = 'FilterDate';
            $this->btnFilter->CausesValidation = true;

            $this->dtgCashTransactions->SetDataBinder('pageBinder', $this);
        }

        public function PageBinder(){
            /*$strCond = "";
            if($this->dtgCashTransactions->SearchText !=''){
                $strCond .= "ba.account_number like '%".$this->dtgCashTransactions->SearchText."%' or bc.first_name like '%".$this->dtgCashTransactions->SearchText."%' and ";
            }
            if($this->datePickerReport->Input->Text != '' && $this->datePickerReport->SecondInput->Text != ''){
                $strCond .= "st.trx_value_date between '".$this->datePickerReport->Input->Text."' and '".$this->datePickerReport->SecondInput->Text."' and ";
            }
            $intOffset = ($this->dtgCashTransactions->PageNumber - 1) * $this->dtgCashTransactions->ItemsPerPage;
            $sql = 'select distinct(st.basic_account_id) from cash_transaction st inner join basic_account ba
                on st.basic_account_id=ba.basic_account_id inner join basic_customer bc on ba.customer_id=bc.customer_id
                where '.$strCond.'st.basic_account_id is not null';
            $sql .=" LIMIT ".$this->dtgCashTransactions->ItemsPerPage." OFFSET ".$intOffset;
            $sqlCount = 'select count(distinct(st.basic_account_id)) from cash_transaction st inner join basic_account ba
                    on st.basic_account_id=ba.basic_account_id inner join basic_customer bc on ba.customer_id=bc.customer_id
                    where '.$strCond.'st.basic_account_id is not null';
            $objDatabase = QApplication::$Database[1];
            $objDbResult = $objDatabase->Query($sql);
            $dataCount = $objDatabase->Query($sqlCount);

            $count = 0;
            while ($cRow = $dataCount->FetchArray()) {
                $count = $cRow[0];
            };

            $array = array();
            while ($mixRow = $objDbResult->FetchArray()) {
                array_push($array, $mixRow);
            }

            $this->dtgCashTransactions->DataSource = $array;
            $this->dtgCashTransactions->TotalItemCount = $count;*/

            $objCondition = array();
            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);

            if($objUser->Role->RoleName == 'CS' || $objUser->Role->RoleName == 'CUSTOMER_SERVICE'){
                array_push($objCondition, QQ::AndCondition(
                    QQ::Equal(QQN::CashTransaction()->WorkedStatus, 1),
                    QQ::Equal(QQN::CashTransaction()->WorkflowStarted, 1)
                ));
            }else if($objUser->Role->RoleName == 'BRANCH_SPV'){
                array_push($objCondition, QQ::AndCondition(
                    QQ::Equal(QQN::CashTransaction()->WorkedStatus, 2),
                    QQ::Equal(QQN::CashTransaction()->WorkflowStarted, 1)
                ));
            }else if($objUser->Role->RoleName == 'HQ_STAFF'){
                array_push($objCondition, QQ::AndCondition(
                    QQ::Equal(QQN::CashTransaction()->WorkedStatus, 3),
                    QQ::Equal(QQN::CashTransaction()->WorkflowStarted, 1)
                ));
            }else if($objUser->Role->RoleName == 'HQ_SPV'){
                array_push($objCondition, QQ::AndCondition(
                    QQ::Equal(QQN::CashTransaction()->WorkedStatus, 4),
                    QQ::Equal(QQN::CashTransaction()->WorkflowStarted, 1)
                ));
            }

            if($search = $this->dtgCashTransactions->SearchText){
                array_push($objCondition, QQ::OrCondition(
                //QQ::Equal(QQN::SuspiciousTransaction()->CustomerId, $search),
                    QQ::Like(QQN::CashTransaction()->AccountNumber, $search),
                    QQ::Like(QQN::CashTransaction()->BranchName, '%'.$search.'%'),
                    QQ::Like(QQN::CashTransaction()->FirstName, '%'.$search.'%')
                ));
            }else if($this->datePickerReport->Input->Text != '' || $this->datePickerReport->SecondInput->Text != ''){
                $startDate = $this->datePickerReport->Input->Text;
                $EndDate = $this->datePickerReport->SecondInput->Text;

                array_push($objCondition, QQ::OrCondition(
                    QQ::Equal(QQN::CashTransaction()->TrxValueDate, $startDate),
                    QQ::AndCondition(
                        QQ::GreaterThan(QQN::CashTransaction()->TrxValueDate, $startDate),
                        QQ::LessThan(QQN::CashTransaction()->TrxValueDate, $EndDate)
                    )
                ));
            }

            array_push($objCondition, QQ::AndCondition(
                QQ::Equal(QQN::CashTransaction()->CtrFlagException, false)
            ));

            array_push($objCondition, QQ::AndCondition(
                QQ::Equal(QQN::CashTransaction()->BankBranchId, $objUser->BankBranchId)
            ));

            $this->dtgCashTransactions->MetaDataBinderCustom(new CashTransaction(), QQ::AndCondition($objCondition));
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;
            switch($section)
            {
                case 'changeStatus1':
                    $this->lstStatus2->UnHide();
                    if($this->lstStatus1->SelectedValue == 'reportStatus')
                    {
                        $this->lstStatus2->RemoveAllItems();
                        $this->lstStatus2->AddItem('- Any -');
                        $this->lstStatus2->AddItem('Dikecualikan','0');
                        $this->lstStatus2->AddItem('Dilaporkan','1');
                        $this->lstStatus2->AddItem('Belum Dikerjakan',NULL);

                    }elseif($this->lstStatus1->SelectedValue == 'transactionStatus'){
                        $this->lstStatus2->RemoveAllItems();
                        $this->lstStatus2->AddItem('- Any -');
                        $this->lstStatus2->AddItem('Approve','A');
                        $this->lstStatus2->AddItem('Reject','R');
                        $this->lstStatus2->AddItem('Belum Dikerjakan',NULL);
                    }else{
                        $this->lstStatus2->RemoveAllItems();
                        $this->lstStatus2->AddItem('- Any -');
                    }
                    break;
                case 'FilterDate':
                case 'changeStatus2':
                    $this->dtgCashTransactions->Refresh();
                    break;
            }
        }

        public function rowActionButton($record)
        {
            $strReturn = null;
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-warning" title="Detail Transaksi"><span class="fa fa-plus-circle"></span></a>',
                __SOURCE__ . '/transaksi/cash/detail.php/'.$record);

            return $strReturn;
        }

        public function cropString($word){
            $wordNew = intval(substr($word,0,3));
            if(is_string($wordNew) == true){
                $wordNew = substr($word,3);
            }
            return $wordNew;
        }

        public function getName($id) {
            $nameCust = '';

            $objAccount = BasicAccount::QuerySingle(
                QQ::Equal(QQN::BasicAccount()->BasicAccountId, $id)
            );

            if($objAccount){
                $objCust = BasicCustomer::QuerySingle(
                    QQ::Equal(QQN::BasicCustomer()->CustomerId, $objAccount->CustomerId)
                );

                if($objCust){
                    $nameCust = $objCust->FirstName;
                }else{
                    $nameCust = '';
                }
            }

            return $nameCust;
        }

        public function getBranch($id){
            $objBranch = BankBranch::QuerySingle(
                QQ::Equal(QQN::BankBranch()->BankBranchId,$id)
            );

            if($objBranch){
                return $objBranch->BranchName;
            }else{
                return null;
            }
        }

        public function WorkedStatus($word){
            switch($word){
                case '1':
                    $wordNew = '';
                    break;
                case '2':
                    $wordNew = 'CS';
                    break;
                case '3':
                    $wordNew = 'BRANCH SPV';
                    break;
                case '4':
                    $wordNew = 'HQ SPV';
                    break;
                default :
                    $wordNew = '';
                    break;
            }
            return $wordNew;
        }

        public function getReportStatus($status){
            switch($status){
                case '0':
                    $statusInfo = 'Dikecualikan';
                    break;
                case '1':
                    $statusInfo = 'Dilaporkan';
                    break;
                default :
                    $statusInfo = 'Belum Dikerjakan';
                    break;
            }
            return $statusInfo;
        }

        public function getTrxStatus($status){
            switch($status){
                case 'A':
                    $statusInfo = 'Approve';
                    break;
                case 'R':
                    $statusInfo = 'Reject';
                    break;
                default :
                    $statusInfo = '-';
                    break;
            }
            return $statusInfo;
        }

        public function getCif($id){
            $CifCust = '';

            $objAccount = BasicAccount::QuerySingle(
                QQ::Equal(QQN::BasicAccount()->BasicAccountId,$id)
            );

            if($objAccount){
                $objCustomer = BasicCustomer::QuerySingle(
                    QQ::Equal(QQN::BasicCustomer()->CustomerId, $objAccount->CustomerId)
                );

                if($objCustomer){
                    $CifCust = $objCustomer->CustomerId;
                }else{
                    $CifCust = '';
                }
            }
            return $CifCust;
        }

        public function getAccountNumber($cif){
            $AccNumber = '';

            $objAccount = CashTransaction::QuerySingle(
                QQ::Equal(QQN::CashTransaction()->CashTransactionId,$cif)
            );

            if($objAccount){
                $AccNumber = $objAccount->BasicAccountId;
            }
            return $AccNumber;
        }

        public function getJoinAccount($id){
            $account = '';

            $objParticipantsArray = CashTransaction::QueryArray(
                QQ::AndCondition (
                    QQ::Equal(QQN::CashTransaction()->BasicAccountId, $id),
                    QQ::Equal(QQN::CashTransaction()->BasicAccountId, 3)
                ),
                QQ::Clause(
                    QQ::Distinct(),
                    QQ::OrderBy(QQN::CashTransaction()->CashTransactionId, QQN::CashTransaction()->BasicAccountId)
                )
            );


        }

        public function Sum($id){
            $objPersonArray = CashTransaction::QueryArray(
                QQ::All(),
                QQ::Clause(
                    QQ::OrderBy(QQ::NotEqual(QQN::CashTransaction()->DebitAmount, 'BasicAccountId'), QQN::CashTransaction()->CreditAmount)
                )
            );

            foreach ($objPersonArray as $objPerson) {
                _p('<li>'.$objPerson->DebitAmount . ' ' . $objPerson->CreditAmount.'</li>', false);
            }
        }
	}

	CashTransactionListForm::Run('CashTransactionListForm');
?>