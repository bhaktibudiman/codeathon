<?php
require(__CONFIGURATION__ . '/header.inc.php');
    $conn_string = "host=156.67.217.190 port=5432 dbname=codeathon user=postgres password=postgres";
    $dbconn4 = pg_connect($conn_string);
    $risk = "  SELECT data_code, COUNT(*) as jumlah_idx FROM t_idx
                GROUP BY data_code
                UNION ALL
                    SELECT  data_code, COUNT(*) as jumlah_phl FROM t_phl
                GROUP BY data_code
                UNION ALL
                    SELECT  data_code, COUNT(*) as jumlah_tha FROM t_tha
                GROUP BY data_code
                UNION ALL
                    SELECT  data_code, COUNT(*) as jumlah_aus FROM t_aus
                GROUP BY data_code
                UNION ALL
                    SELECT  data_code, COUNT(*) as jumlah_mys FROM t_mys
                GROUP BY data_code
                            ";

    $result = pg_query($dbconn4, $risk);


    $off = "  SELECT of_country_code, COUNT(*) as jumlah_off FROM external_offshore
                GROUP BY of_country_code
                UNION ALL
                    SELECT  of_country_code, COUNT(*) as jumlah_pan FROM external_panama
                GROUP BY of_country_code
                UNION ALL
                    SELECT  of_country_code, COUNT(*) as jumlah_par FROM external_paradise
                GROUP BY of_country_code
                UNION ALL
                    SELECT  of_country_code, COUNT(*) as jumlah_ex FROM external_bermuda
                GROUP BY of_country_code
                ";

    $result_off = pg_query($dbconn4, $off);
?>


    <title>Stock Exchange Of Country</title>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart()
        {
            var data = google.visualization.arrayToDataTable([
                ['DataCode', 'Jumlah'],
                <?php
                while($row = pg_fetch_array($result))
                {
                    echo "['".$row["data_code"]."', ".$row["jumlah"]."],";
                }
                ?>
            ]);
            var options = {
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 1,
                    plotShadow: false
                },



                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },

                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            format: '<b>{point.name}</b>: {point.y}',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        },
                        showInLegend: true
                    }
                },

//                    is3D:true,
                pieHole: 0.4
            };

            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    </script>





<?php $this->RenderBegin() ?>

<?php
$conn_string = "host=156.67.217.190 port=5432 dbname=codeathon user=postgres password=postgres";
$dbconn4 = pg_connect($conn_string);

$idx = "  SELECT data_code, COUNT(*) as jumlah_idx FROM t_idx
          GROUP BY data_code";

$tha  = " SELECT  data_code, COUNT(*) as jumlah_phl FROM t_phl
        GROUP BY data_code";

$aus = "SELECT  data_code, COUNT(*) as jumlah_aus FROM t_aus
                GROUP BY data_code";

$mys = " SELECT  data_code, COUNT(*) as jumlah_mys FROM t_mys
            GROUP BY data_code";

$r_idx = pg_query($dbconn4, $idx);
$r_tha = pg_query($dbconn4, $tha);
$r_aus = pg_query($dbconn4, $aus);
$r_mys = pg_query($dbconn4, $mys);

?>



    <section class="content-header">
        <h1>Dashboard</h1>
    </section>
    <br>
    <div class="box box-primary">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <!--                <div class="col-md-6">-->
                <!--                    <div id="container"></div>-->
                <!--                </div>-->
                <div class="col-md-6">
                    <div id="piechart" style="width: 756px; height: 500px;"></div>
                </div>

                <div class="col-md-6">
                    <br>
                    <br>
                    <h1>
                        <center> Stock Exchange Of Country</center>
                    </h1>
                    <hr/>

                    <p>
                    </p>

                    <?php
//                    $r_idx = pg_query($dbconn4, $idx);
//                    $execRiskQuery = pg_query($idx);
//                    while ($row = pg_fetch_array($execRiskQuery)) {
//                        $ex = explode(" ",$row['data_source']);
//                        $risk_output[] = array($ex[0], (int)$row['jumlah_idx']);
                    ?>

                    <ul>
                        <li>
                            <p>
<!--                                Summary Of <B>IDX </B> Source : --><?php //echo $risk_output['jumlah_idx']; ?>
                            </p>
                        </li>
                        <li>
                                <p>
<!--                                    Summary Of <B>PHL</B> Source : --><?php //echo $r_tha['jumlah_phl']; ?>
                                </p>
                        </li>
                        <li>
                            <p>

                            </p>
                        </li>
                        <li>
                            <p>
                            </p>
                        </li>
                        <li>
                            <p>
                            </p>
                        </li>
                </div>
            </div>
        </div>
    </div>



<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>