<?php
require(__CONFIGURATION__ . '/header.inc.php');
    $conn_string = "host=localhost port=5432 dbname=apuppt-btn user=postgres password=P455@4dmJRS";
    $dbconn4 = pg_connect($conn_string);
    $risk = "SELECT ap.param_desc, COUNT(1) as jumlah FROM basic_score_risk sc INNER JOIN apu_parameter ap ON ap.param_seq = sc.risk_type_id
             WHERE param_type = 'TYPE_RISK' GROUP BY param_desc
            ";

    // Get Records from the table`
    $execRiskQuery = pg_query($risk);
    while ($row = pg_fetch_array($execRiskQuery)) {
        $ex = explode(" ",$row['param_desc']);
        $risk_output[] = array($ex[0], (int)$row['jumlah']); 
    }

    $nationality = "SELECT 'INDONESIA' AS nationality, count(1) as jumlah FROM basic_customer WHERE nationality_code = '62' GROUP BY nationality_code
                    UNION ALL
                    SELECT 'OTHER' AS nationality, SUM(x) AS jumlah FROM (
                    SELECT nationality_code, count(1) as x FROM basic_customer WHERE nationality_code <> '62' GROUP BY nationality_code
                    ) as y";

    // Get Records from the table
    $execNationalityQuery = pg_query($nationality);
    while ($row = pg_fetch_array($execNationalityQuery)) {
        $nationality_output[] = array($row['nationality'], (int)$row['jumlah']); 
    }

    $custtype = "SELECT ap.param_desc, COUNT(1) as jumlah FROM basic_customer sc INNER JOIN apu_parameter ap ON ap.parameter_id = sc.customer_type_id::int WHERE param_type = 'CUSTOMER_TYPE' GROUP BY param_desc";

    // Get Records from the table
    $execCusttypeQuery = pg_query($custtype);
    while ($row = pg_fetch_array($execCusttypeQuery)) {
        $custtype_output[] = array($row['param_desc'], (int)$row['jumlah']); 
    }

    $transaction = "
        SELECT SUM(trx_amount) as trx_amount, COUNT(1) as trx_count,trx_type, EXTRACT (MONTH FROM trx_value_date) as trx_month,EXTRACT (YEAR FROM trx_value_date) as trx_year,customer_type_id FROM suspicious_transaction st
        WHERE trx_type = 'DEBIT' AND customer_type_id = '9'
        GROUP BY trx_type, EXTRACT (MONTH FROM trx_value_date),EXTRACT (YEAR FROM trx_value_date), customer_type_id
        ORDER  BY EXTRACT (MONTH FROM trx_value_date) 
    ";

    // Get Records from the table
    $execTransactionQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execTransactionQuery)) {
        $transaction_personal_db_output[] = array($row['customer_type_id'], (int)$row['trx_amount']); 
    }

    $transaction = "
        SELECT SUM(trx_amount) as trx_amount, COUNT(1) as trx_count,trx_type, EXTRACT (MONTH FROM trx_value_date) as trx_month,EXTRACT (YEAR FROM trx_value_date) as trx_year,customer_type_id FROM suspicious_transaction st
        WHERE trx_type = 'DEBIT' AND customer_type_id = '10'
        GROUP BY trx_type, EXTRACT (MONTH FROM trx_value_date),EXTRACT (YEAR FROM trx_value_date), customer_type_id
        ORDER  BY EXTRACT (MONTH FROM trx_value_date) 
    ";

    // Get Records from the table
    $execTransactionQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execTransactionQuery)) {
        $transaction_corporate_db_output[] = array($row['customer_type_id'], (int)$row['trx_amount']); 
        $transaction_month[] = array($row['trx_month']); 
    }

    $transaction = "
        SELECT SUM(trx_amount) as trx_amount, COUNT(1) as trx_count,trx_type, EXTRACT (MONTH FROM trx_value_date) as trx_month,EXTRACT (YEAR FROM trx_value_date) as trx_year,customer_type_id FROM suspicious_transaction st
        WHERE trx_type = 'CREDIT' AND customer_type_id = '9'
        GROUP BY trx_type, EXTRACT (MONTH FROM trx_value_date),EXTRACT (YEAR FROM trx_value_date), customer_type_id
        ORDER  BY EXTRACT (MONTH FROM trx_value_date) 
    ";

    // Get Records from the table
    $execTransactionQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execTransactionQuery)) {
        $transaction_personal_cr_output[] = array($row['customer_type_id'], (int)$row['trx_amount']); 
    }

    $transaction = "
        SELECT SUM(trx_amount) as trx_amount, COUNT(1) as trx_count,trx_type, EXTRACT (MONTH FROM trx_value_date) as trx_month,EXTRACT (YEAR FROM trx_value_date) as trx_year,customer_type_id FROM suspicious_transaction st
        WHERE trx_type = 'CREDIT' AND customer_type_id = '10'
        GROUP BY trx_type, EXTRACT (MONTH FROM trx_value_date),EXTRACT (YEAR FROM trx_value_date), customer_type_id
        ORDER  BY EXTRACT (MONTH FROM trx_value_date) 
    ";

    // Get Records from the table
    $execTransactionQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execTransactionQuery)) {
        $transaction_corporate_cr_output[] = array($row['customer_type_id'], (int)$row['trx_amount']); 
    }

    $month = 4;
    $day_of_month = date("t", strtotime("1111-4-11"));
    for ($i=1; $i <= $day_of_month; $i++) { 
        $ctr_day[] = $i; 
    }

    #CTR Transaction 1
    $transaction = "
        WITH x AS (
        SELECT EXTRACT(DAY FROM trx_value_date) AS hari,SUM(trx_amount)::BIGINT AS trx_amount, COUNT(1) AS trx_count FROM cash_transaction WHERE EXTRACT (MONTH FROM trx_value_date) = ".$month." AND trx_type = 'DEBIT'
        GROUP BY EXTRACT (DAY FROM trx_value_date)
        )

        SELECT * FROM x
        UNION ALL
        SELECT a.n, 0, 0 from generate_series(1, ".end($ctr_day).") as a(n)
        WHERE a.n NOT IN(SELECT hari FROM x)
        ORDER BY hari

    ";

    // Get Records from the table
    $execCTRDebitQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execCTRDebitQuery)) {
        $transaction_ctr_debit[] = array((int)$row['trx_amount']); 
        $count_trx_ctr_debit[] = array((int)$row['trx_count']); 
    }

    $transaction = "
        WITH x AS (
        SELECT EXTRACT(DAY FROM trx_value_date) AS hari,SUM(trx_amount)::BIGINT AS trx_amount, COUNT(1) AS trx_count FROM cash_transaction WHERE EXTRACT (MONTH FROM trx_value_date) = ".$month." AND trx_type = 'Credit'
        GROUP BY EXTRACT (DAY FROM trx_value_date)
        )

        SELECT * FROM x
        UNION ALL
        SELECT a.n, 0, 0 from generate_series(1, ".end($ctr_day).") as a(n)
        WHERE a.n NOT IN(SELECT hari FROM x)
        ORDER BY hari

    ";

    // Get Records from the table
    $execCTRCreditQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execCTRCreditQuery)) {
        $transaction_ctr_credit[] = array((int)$row['trx_amount']); 
        $count_trx_ctr_credit[] = array((int)$row['trx_count']); 
    }

    #UTR Transaction 2
    $transaction = "
        WITH x AS (
        SELECT EXTRACT(DAY FROM trx_value_date) AS hari,SUM(trx_amount)::BIGINT AS trx_amount FROM suspicious_transaction WHERE EXTRACT (MONTH FROM trx_value_date) = ".$month." AND trx_type = 'DEBIT'
        GROUP BY EXTRACT (DAY FROM trx_value_date)
        )

        SELECT * FROM x
        UNION ALL
        SELECT a.n, 0 from generate_series(1, ".end($ctr_day).") as a(n)
        WHERE a.n NOT IN(SELECT hari FROM x)
        ORDER BY hari

    ";

    // Get Records from the table
    $execUTRDebitQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execUTRDebitQuery)) {
        $transaction_utr_debit[] = array((int)$row['trx_amount']); 
    }

    $transaction = "
        WITH x AS (
        SELECT EXTRACT(DAY FROM trx_value_date) AS hari,SUM(trx_amount)::BIGINT AS trx_amount FROM suspicious_transaction WHERE EXTRACT (MONTH FROM trx_value_date) = ".$month." AND trx_type = 'CREDIT'
        GROUP BY EXTRACT (DAY FROM trx_value_date)
        )

        SELECT * FROM x
        UNION ALL
        SELECT a.n, 0 from generate_series(1, ".end($ctr_day).") as a(n)
        WHERE a.n NOT IN(SELECT hari FROM x)
        ORDER BY hari

    ";

    // Get Records from the table
    $execuTRCreditQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuTRCreditQuery)) {
        $transaction_utr_credit[] = array((int)$row['trx_amount']); 
    }

    //CTR & UTR Freq
    $transaction = "
        SELECT bk.branch_code,bk.branch_name,count(st.*) AS utr_count, ct.trx_count AS ctr_count FROM suspicious_transaction st
        JOIN bank_branch bk ON bk.branch_code = st.branch_code
        LEFT JOIN (
            SELECT bb.branch_code, count(1) AS trx_count FROM cash_transaction ct
            JOIN bank_branch bb ON bb.branch_code = ct.branch_code
            WHERE EXTRACT (MONTH FROM ct.trx_value_date) = ".$month." AND ct.trx_type = 'DEBIT'
            GROUP BY bb.branch_code
        ) ct ON ct.branch_code = st.branch_code
        WHERE EXTRACT (MONTH FROM st.trx_value_date) = ".$month." AND trx_type = 'DEBIT'
        GROUP BY bk.branch_name ,bk.branch_code,ct.trx_count ORDER BY count(1) DESC LIMIT 5

    ";

    // Get Records from the table
    $execuFREQDebitQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuFREQDebitQuery)) {
        $freq_ctr_debit[] = array((int)$row['ctr_count']); 
        $freq_utr_debit[] = array((int)$row['utr_count']); 
        $freq_brc_debit[] = array($row['branch_name']); 
    }

    $transaction = "
        SELECT bk.branch_code,bk.branch_name,count(st.*) AS utr_count, ct.trx_count AS ctr_count FROM suspicious_transaction st
        JOIN bank_branch bk ON bk.branch_code = st.branch_code
        LEFT JOIN (
            SELECT bb.branch_code, count(1) AS trx_count FROM cash_transaction ct
            JOIN bank_branch bb ON bb.branch_code = ct.branch_code
            WHERE EXTRACT (MONTH FROM ct.trx_value_date) = ".$month." AND ct.trx_type = 'CREDIT'
            GROUP BY bb.branch_code
        ) ct ON ct.branch_code = st.branch_code
        WHERE EXTRACT (MONTH FROM st.trx_value_date) = ".$month." AND trx_type = 'CREDIT'
        GROUP BY bk.branch_name ,bk.branch_code,ct.trx_count ORDER BY count(1) DESC LIMIT 5

    ";

    // Get Records from the table
    $execuFREQCreditQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuFREQCreditQuery)) {
        $freq_ctr_credit[] = array((int)$row['ctr_count']); 
        $freq_utr_credit[] = array((int)$row['utr_count']); 
        $freq_brc_credit[] = array($row['branch_name']); 
    }

     $transaction = "
        WITH ctr AS (
        SELECT u.branch_code, count(1) as cust_count FROM(
            SELECT ct.branch_code, customer_id, trx_value_date from cash_transaction ct
            JOIN bank_branch bk ON bk.branch_code = ct.branch_code
            WHERE EXTRACT(MONTH FROM trx_value_date) = '4' AND trx_type = 'DEBIT'
            GROUP BY ct.branch_code,customer_id,trx_value_date) AS u
        JOIN basic_customer bc ON u.customer_id = bc.customer_id
        WHERE bc.customer_type_id = '10'
        GROUP BY u.branch_code
        ORDER BY count(1) DESC),
        utr AS (
        SELECT branch_code, count(1) as cust_count FROM(
            SELECT bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id FROM suspicious_transaction st
            JOIN basic_account ba ON  ba.account_number= st.account_number
            JOIN basic_customer bc ON bc.customer_id = ba.customer_id
            WHERE EXTRACT(MONTH FROM st.trx_value_date) = '4' AND st.trx_type = 'DEBIT'
            GROUP BY bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id
            ) AS t
        WHERE customer_type_id = '10'
        GROUP BY branch_code
        ORDER BY count(1) DESC LIMIT 5
        )

        SELECT bk.branch_code,bk.branch_name,utr.cust_count AS utr_cust_count, ctr.cust_count AS ctr_cust_count FROM ctr
        LEFT JOIN utr ON utr.branch_code = ctr.branch_code
        LEFT JOIN bank_branch bk ON ctr.branch_code = bk.branch_code
        LIMIT 5
    ";

    // Get Records from the table
    $execuCustCorpDBQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuCustCorpDBQuery)) {
        $transaction_cust_corp_utr[] = array((int)$row['utr_cust_count']); 
        $transaction_cust_corp_ctr[] = array((int)$row['ctr_cust_count']); 
        $transaction_cust_corp_brc_name[] = array($row['branch_name']); 
    }

    $transaction = "
        WITH ctr AS (
        SELECT u.branch_code, count(1) as cust_count FROM(
            SELECT ct.branch_code, customer_id, trx_value_date from cash_transaction ct
            JOIN bank_branch bk ON bk.branch_code = ct.branch_code
            WHERE EXTRACT(MONTH FROM trx_value_date) = '4' AND trx_type = 'DEBIT'
            GROUP BY ct.branch_code,customer_id,trx_value_date) AS u
        JOIN basic_customer bc ON u.customer_id = bc.customer_id
        WHERE bc.customer_type_id = '9'
        GROUP BY u.branch_code
        ORDER BY count(1) DESC),
        utr AS (
        SELECT branch_code, count(1) as cust_count FROM(
            SELECT bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id FROM suspicious_transaction st
            JOIN basic_account ba ON  ba.account_number= st.account_number
            JOIN basic_customer bc ON bc.customer_id = ba.customer_id
            WHERE EXTRACT(MONTH FROM st.trx_value_date) = '4' AND st.trx_type = 'DEBIT'
            GROUP BY bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id 
            ) AS t 
        WHERE customer_type_id = '9'
        GROUP BY branch_code
        ORDER BY count(1) DESC LIMIT 5
        )

        SELECT utr.branch_code,bk.branch_name,utr.cust_count AS utr_cust_count, ctr.cust_count AS ctr_cust_count FROM utr
        JOIN ctr ON utr.branch_code = ctr.branch_code 
        JOIN bank_branch bk ON utr.branch_code = bk.branch_code
        LIMIT 5 
    ";

    // Get Records from the table
    $execuCustCorpDBQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuCustCorpDBQuery)) {
        $transaction_cust_ind_utr[] = array((int)$row['utr_cust_count']); 
        $transaction_cust_ind_ctr[] = array((int)$row['ctr_cust_count']); 
        $transaction_cust_ind_brc_name[] = array($row['branch_name']); 
    }

    $transaction = "
        WITH ctr AS (
        SELECT u.branch_code, count(1) as cust_count FROM(
            SELECT ct.branch_code, customer_id, trx_value_date from cash_transaction ct
            JOIN bank_branch bk ON bk.branch_code = ct.branch_code
            WHERE EXTRACT(MONTH FROM trx_value_date) = '4' AND trx_type = 'CREDIT'
            GROUP BY ct.branch_code,customer_id,trx_value_date) AS u
        JOIN basic_customer bc ON u.customer_id = bc.customer_id
        WHERE bc.customer_type_id = '10'
        GROUP BY u.branch_code
        ORDER BY count(1) DESC),
        utr AS (
        SELECT branch_code, count(1) as cust_count FROM(
            SELECT bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id FROM suspicious_transaction st
            JOIN basic_account ba ON  ba.account_number= st.account_number
            JOIN basic_customer bc ON bc.customer_id = ba.customer_id
            WHERE EXTRACT(MONTH FROM st.trx_value_date) = '4' AND st.trx_type = 'CREDIT'
            GROUP BY bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id
            ) AS t
        WHERE customer_type_id = '10'
        GROUP BY branch_code
        ORDER BY count(1) DESC LIMIT 5
        )

        SELECT ctr.branch_code,bk.branch_name,ctr.cust_count AS utr_cust_count, ctr.cust_count AS ctr_cust_count FROM ctr
        LEFT JOIN utr ON ctr.branch_code = ctr.branch_code
        LEFT JOIN bank_branch bk ON ctr.branch_code = bk.branch_code
        LIMIT 5
    ";

    // Get Records from the table
    $execuCustCorpDBCredQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuCustCorpDBCredQuery)) {
        $transaction_cust_corp_utr_cred[] = array((int)$row['utr_cust_count']); 
        $transaction_cust_corp_ctr_cred[] = array((int)$row['ctr_cust_count']); 
        $transaction_cust_corp_brc_name_cred[] = array($row['branch_name']); 
    }

    $transaction = "
        WITH ctr AS (
        SELECT u.branch_code, count(1) as cust_count FROM(
            SELECT ct.branch_code, customer_id, trx_value_date from cash_transaction ct
            JOIN bank_branch bk ON bk.branch_code = ct.branch_code
            WHERE EXTRACT(MONTH FROM trx_value_date) = '4' AND trx_type = 'CREDIT'
            GROUP BY ct.branch_code,customer_id,trx_value_date) AS u
        JOIN basic_customer bc ON u.customer_id = bc.customer_id
        WHERE bc.customer_type_id = '9'
        GROUP BY u.branch_code
        ORDER BY count(1) DESC),
        utr AS (
        SELECT branch_code, count(1) as cust_count FROM(
            SELECT bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id FROM suspicious_transaction st
            JOIN basic_account ba ON  ba.account_number= st.account_number
            JOIN basic_customer bc ON bc.customer_id = ba.customer_id
            WHERE EXTRACT(MONTH FROM st.trx_value_date) = '4' AND st.trx_type = 'CREDIT'
            GROUP BY bc.branch_code, bc.customer_id, st.trx_value_date, bc.customer_type_id
            ) AS t
        WHERE customer_type_id = '9'
        GROUP BY branch_code
        ORDER BY count(1) DESC LIMIT 5
        )

        SELECT ctr.branch_code,bk.branch_name,utr.cust_count AS utr_cust_count, ctr.cust_count AS ctr_cust_count FROM ctr
        LEFT JOIN utr ON utr.branch_code = ctr.branch_code
        LEFT JOIN bank_branch bk ON ctr.branch_code = bk.branch_code
        LIMIT 5
    ";

    // Get Records from the table
    $execuCustIndDBCredQuery = pg_query($transaction);
    while ($row = pg_fetch_array($execuCustIndDBCredQuery)) {
        $transaction_cust_ind_utr_cred[] = array((int)$row['utr_cust_count']); 
        $transaction_cust_ind_ctr_cred[] = array((int)$row['ctr_cust_count']); 
        $transaction_cust_ind_brc_name_cred[] = array($row['branch_name']); 
    }


?>
<script src="https://code.highcharts.com/highcharts.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() {
    Highcharts.setOptions({
        global: {
            useUTC: false,
            
        },
        lang: {
          decimalPoint: ',',
          thousandsSep: '.'
        },
        colors: ['#006300', '#FF9000', '#D95819', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
    });

    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,
            plotShadow: false
        },
        title: {
            text: 'Risk Type of Customer'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },credits: {
            enabled: false
        },
        series: [{
            type: 'pie',
            name: 'Risk Type',
            data: <?php echo json_encode(isset($risk_output) ? $risk_output : null);?>
        }]
    });

    $('#nationality').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,
            plotShadow: false
        },credits: {
            enabled: false
        },
        title: {
            text: 'Customer Nationality'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Nationality',
            data: <?php echo json_encode(isset($nationality_output) ? $nationality_output : null);?>
        }]
    });


    $('#custtype').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 1,
            plotShadow: false
        },
        title: {
            text: 'Customer Type'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },credits: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y}',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Customer Type',
            data: <?php echo json_encode(isset($custtype_output) ? $custtype_output : null);?>
        }]
    });

    <?php 

        $fr = isset($transaction_month) ? $transaction_month : null;
     ?>

    $('#transaction').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            text: 'Customer Total Transaction Amount , Grouped by Transaction Type & Customer Type'
        },

        xAxis: {
            categories: <?php echo json_encode($fr); ?>
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Amount'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + Highcharts.numberFormat(this.y) + '<br/>' +
                    'Total: ' + Highcharts.numberFormat(this.point.stackTotal);
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },credits: {
            enabled: false
        },
        series: [{
            name: 'Corporate Debit',
            data: <?php echo json_encode(isset($transaction_corporate_db_output) ? $transaction_corporate_db_output : null) ?>,
            stack: 'Debit'
        },{
            name: 'Corporate Credit',
            data: <?php echo json_encode(isset($transaction_corporate_cr_output) ? $transaction_corporate_cr_output : null) ?>,
            stack: 'Credit'
        },{
            name: 'Personal Debit',
            data: <?php echo json_encode(isset($transaction_personal_db_output) ? $transaction_personal_db_output : null) ?>,
            stack: 'Debit'
        },{
            name: 'Personal Credit',
            data: <?php echo json_encode(isset($transaction_personal_cr_output)? $transaction_personal_cr_output : null) ?>,
            stack: 'Credit'
        }]
    });


    $('#ctr1').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Monthly Cash Transaction April 2017'
        },
        subtitle: {
            text: 'BTN'    
        },
        xAxis: {
            categories: <?php echo json_encode($ctr_day) ?>
        },
        yAxis: {
            title: {
                text: 'Amount (IDR)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br />'
        },credits: {
            enabled: false
        },

        series: [{
            name: 'Debit',
            data: <?php echo json_encode(isset($transaction_ctr_debit) ? $transaction_ctr_debit : null) ?>, color:'#F97A26'
        }, {
            name: 'Credit',
            data: <?php echo json_encode(isset($transaction_ctr_credit) ? $transaction_ctr_credit : null) ?>, color:'#60A9B9'
        }]
    });

    $('#utr2').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Monthly Unusual Transaction April 2017'
        },
        subtitle: {
            text: 'BTN'    
        },
        xAxis: {
            categories: <?php echo json_encode($ctr_day) ?>
        },
        yAxis: {
            title: {
                text: 'Amount (IDR)'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br />'
        },credits: {
            enabled: false
        },

        series: [{
            name: 'Debt',
            data: <?php echo json_encode(isset($transaction_utr_debit) ? $transaction_utr_debit : null) ?>, color:'#F97A26'
        }, {
            name: 'Credit',
            data: <?php echo json_encode(isset($transaction_utr_credit) ? $transaction_utr_credit : null) ?>, color:'#60A9B9'
        }]
    });

    $('#ctr_utr_debit_count').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            // text: 'CTR & UTR Frequensi Transaksi Debit 5 Cabang Terbesar'
            //text: '5 Branch with the Largest Transaction Frequencies for CTR & UTR'
            text: 'Top 5 Transaction Frequencies for CTR & UTR'
			
        },

        xAxis: {
            categories: <?php echo json_encode(isset($freq_brc_debit) ? $freq_brc_debit : null); ?>
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Frequencies'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },credits: {
            enabled: false
        },
        series: [{
            name: 'CTR Debit',
            data: <?php echo json_encode(isset($freq_ctr_debit) ? $freq_ctr_debit : null) ?>,
        },{
            name: 'UTR Debit',
            data: <?php echo json_encode(isset($freq_utr_debit) ? $freq_utr_debit : null) ?>,
        }]
    });

    $('#ctr_utr_credit_count').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            // text: 'CTR & UTR Frequensi Transaksi Kredit 5 Cabang Terbesar'
            text: '5 Branch with the Largest Credit Transaction Frequencies for CTR & UTR'
        },

        xAxis: {
            categories: <?php echo json_encode(isset($freq_brc_credit) ? $freq_brc_credit : null); ?>
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Frequent'
            }
        },credits: {
            enabled: false
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },credits: {
            enabled: false
        },
        series: [{
            name: 'CTR Credit',
            data: <?php echo json_encode(isset($freq_ctr_credit) ? $freq_ctr_credit : null) ?>,
        },{
            name: 'UTR Credit',
            data: <?php echo json_encode(isset($freq_utr_credit) ? $freq_utr_credit : null) ?>,
        }]
    });

    $('#corp_cust').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            // text: 'CTR & UTR Customer Terbanyak Transaksi Debit 5 Cabang Terbesar'
            //text: '5 Branch with the Largest Debt Transaction Amount for CTR & UTR'
			text: 'Top 5 Debt Transaction Amount for CTR & UTR'

        },

        xAxis: {
            categories: <?php echo json_encode(isset($transaction_cust_corp_brc_name) ? $transaction_cust_corp_brc_name : null); ?>
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Total Customer'
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },credits: {
            enabled: false
        },
        series: [{
            name: 'Personal CTR',
            data: <?php echo json_encode(isset($transaction_cust_ind_ctr) ? $transaction_cust_ind_ctr : null) ?>,
            stack: 'CTR'
        },{
            name: 'Corporate CTR',
            data: <?php echo json_encode(isset($transaction_cust_corp_ctr) ? $transaction_cust_corp_ctr : null) ?>,
            stack: 'CTR'
        },{
            name: 'Personal UTR',
            data: <?php echo json_encode(isset($transaction_cust_ind_utr) ? $transaction_cust_ind_utr : null) ?>,
            stack: 'UTR'
        },{
            name: 'Corporate UTR',
            data: <?php echo json_encode(isset($transaction_cust_corp_utr) ? $transaction_cust_corp_utr : null) ?>,
            stack: 'UTR'
        }]
    });

    $('#corp_cust_cred').highcharts({

        chart: {
            type: 'column'
        },

        title: {
            // text: 'CTR & UTR Customer Terbanyak Transkasi Credit 5 Cabang Terbesar'
            text: '5 Branch with the Largest Credit Transaction Amount for CTR & UTR'
        },

        xAxis: {
            categories: <?php echo json_encode(isset($transaction_cust_corp_brc_name) ? $transaction_cust_corp_brc_name : null); ?>
        },

        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Total Customer'
            }
        },credits: {
            enabled: false
        },
        plotOptions: {
            column: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.x + '</b><br/>' +
                    this.series.name + ': ' + this.y + '<br/>';
            }
        },credits: {
            enabled: false
        },
        series: [{
            name: 'Personal CTR',
            data: <?php echo json_encode(isset($transaction_cust_ind_ctr_cred) ? $transaction_cust_ind_ctr_cred : null) ?>,
            stack: 'CTR'
        },{
            name: 'Corporate CTR',
            data: <?php echo json_encode(isset($transaction_cust_corp_ctr_cred) ? $transaction_cust_corp_ctr_cred : null) ?>,
            stack: 'CTR'
        },{
            name: 'Personal UTR',
            data: <?php echo json_encode(isset($transaction_cust_ind_utr_cred) ? $transaction_cust_ind_utr_cred : null) ?>,
            stack: 'UTR'
        },{
            name: 'Corporate UTR',
            data: <?php echo json_encode(isset($transaction_cust_corp_utr_cred) ? $transaction_cust_corp_utr_cred : null) ?>,
            stack: 'UTR'
        }]
    });
});

</script>

<?php $this->RenderBegin() ?>

<section class="content-header">
    <h1>Dashboard</h1>
</section>
<br>
<div class="box box-primary">
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-6">
                <div id="container"></div>
            </div>
            <!-- <div class="col-md-4">
                <div id="nationality"></div>
            </div> -->
            <div class="col-md-6">
                <div id="custtype"></div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-12">
                <!-- <div id="transaction"></div> -->
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-12">
                <div id="ctr1"></div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-12">
                <div id="utr2"></div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-6">
                <div id="ctr_utr_debit_count"></div>
            </div>
            <div class="col-md-6">
                <div id="ctr_utr_credit_count"></div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="dataTables_wrapper form-inline">
            <div class="col-md-6">
                <div id="corp_cust"></div>
            </div>
            <div class="col-md-6">
                <div id="corp_cust_cred"></div>
            </div>
        </div>
    </div>
</div>



<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>