<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

class LoginForm extends QForm {

    protected $txtUsername;
    protected $txtPassword;
    protected $btnLogin;
    protected $objUser;
    protected $userId;
    protected $paramLockCount;
    protected $paramExpiredCount;
    protected $paramSessionLogin;


    protected function Form_Create() {
        $this->objDefaultWaitIcon = new QWaitIcon($this);
        $this->txtUsername = new QTextBox($this);
        $this->txtUsername->Required = true;
        $this->txtUsername->Placeholder = 'Username';
        $this->txtUsername->CssClass = 'form-control';

        $this->txtPassword = new QTextBox($this);
        $this->txtPassword->Required = true;
        $this->txtPassword->Placeholder = 'Password';
        $this->txtPassword->TextMode = QTextMode::Password;
        $this->txtPassword->CssClass = 'form-control';

        $this->btnLogin = new QButton($this);
        $this->btnLogin->Text = QApplication::Translate('Sign In');
        $this->btnLogin->AddAction(new QClickEvent(), new QAjaxAction('btnLogin_Click'));
        $this->btnLogin->AddAction(new QEnterKeyEvent(), new QAjaxAction('btnLogin_Click'));
        $this->btnLogin->PrimaryButton = true;
        $this->btnLogin->CssClass = 'btn btn-primary btn-block btn-flat';
        $this->btnLogin->CausesValidation = true;
//        $this->btnLogin->AddAction(new QClickEvent(), new QJavaScriptAction('myFunction();'));


        $objParamLock = ApuParameter::LoadByParamTypeAndMappingCode('APPLICATION_PARAM','LOCKED_COUNT');
        $this->paramLockCount = $objParamLock->ParamValue;

        $objParamExpireCount = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM','EXPIRED_PASSWORD');
        $this->paramExpiredCount = $objParamExpireCount->ParamValue;

        $objSessionLogin = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM','TIMEOUT_SAME_LOGIN');
        $this->paramSessionLogin = $objSessionLogin->ParamValue;

        System::NavigationControl($this->txtUsername,$this->txtPassword);
        System::NavigationControl($this->txtPassword,$this->btnLogin);
        //$_SESSION[__SESSION_LOGIN__] = session_id();
    }


    protected function Form_Validate() {
        $blnToReturn = true;
        $this->objUser = Users::loadByUserName($this->txtUsername->Text);

        if(!$this->objUser){
            QApplication::DisplayAlert('Username Not Found');
            $blnToReturn = false;
        }

        if(isset($this->objUser) && $this->objUser->ExpiredCount < $this->paramExpiredCount)
        {

            if ($this->txtPassword->Text == '' || $this->txtUsername->Text == '') {
                QApplication::DisplayAlert('Username and Password Is Required');
                $this->txtPassword->Text = '';
                $this->txtUsername->Text = '';
                $blnToReturn = false;
            }elseif($this->objUser->Password != System::getHash($this->txtPassword->Text)){
                $this->objUser->LockedCount += 1;
                $this->objUser->Save();
                if($this->objUser->LockedCount == $this->paramLockCount){
                    $this->objUser->IsLocked = TRUE;
                    $this->objUser->Save();
                    QApplication::DisplayAlert('Password is Invalid and User has been blocked. Contact your administrator ! Thank You.');
                }else{
                    QApplication::DisplayAlert('Password is Invalid');
                }
                $this->txtPassword->Text = '';
                $blnToReturn = false;
            }elseif ($this->objUser->IsLoggedIn == TRUE) {
                QApplication::DisplayAlert('Cannot Login ! problem : 1. This account is in use ! 2. Someone use your account and close this application without logout ! Please contact your administrator !');
                $this->txtPassword->Text = '';
            }elseif ($this->objUser->LastLogin == NULL) {
                $_SESSION[__USER_LOGIN__] = $this->objUser->UserId;
                QApplication::Redirect(__SUBDIRECTORY__.'/src/login/changePassword.php');
            }
        }elseif(isset($this->objUser) && $this->objUser->ExpiredCount >= $this->paramExpiredCount){
            $_SESSION[__USER_LOGIN__] = $this->objUser->UserId; 
            QApplication::Redirect(__SUBDIRECTORY__.'/src/login/changePassword.php');
        }else{
            $blnToReturn = false;
            QApplication::DisplayAlert("Cannot Login ! Problem Is Username and Password Is Required !");
            $this->txtPassword->Text = '';
            $this->txtUsername->Text = '';
        }

        $blnFocused = false;

        foreach ($this->GetErrorControls() as $objControl) {
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            $objControl->Blink();
        }

        return $blnToReturn;
    }

    protected function btnLogin_Click($strFormId, $strControlId, $strParameter) {
        $global_delimiter = TSetting::LoadBySettingCode('CU001');
        $_SESSION['__GLOBAL_DELIMITER__'] = isset($global_delimiter) ? $global_delimiter->DefaultValue : ';' ;

        if(isset($this->objUser)){
            if($this->objUser->IsUserActive == false){
                QApplication::DisplayAlert('User is InActive. Contact your administrator ! Thank You.');
            }else{

                $strQuery = sprintf("
                                SELECT (DATE_PART('day', NOW() - '%s') * 24 + DATE_PART('hour', NOW() - '%s')) * 60 + DATE_PART('minute', NOW() - '%s'::timestamp) AS timediff
                                FROM users
                                WHERE user_id = %u",
                    date_format($this->objUser->LastLogin, 'Y-m-d H:i:s'),
                    date_format($this->objUser->LastLogin, 'Y-m-d H:i:s'),
                    date_format($this->objUser->LastLogin, 'Y-m-d H:i:s'),
                    $this->objUser->UserId);

                $objDatabase = QApplication::$Database[1];
                $objDbResult = $objDatabase->Query($strQuery);

                while ($mixRow = $objDbResult->FetchArray()) { $x = $mixRow['timediff']; }

                $wait = 1; # minute
                $wait = $this->paramSessionLogin;

                $objUser = Users::loadByUserName($this->txtUsername->Text);
                $objRole = RoleUser::Load($this->objUser->RoleId);
                $objConfiguration = TSetting::LoadByRoleLevel($objRole->RoleLevel);


                $y = 0;

                if ($objUser->IsLoggedIn == true) {
                    if ($x > $wait) {
                        $y = 1;
                    }else{
                        $y = 0;
                        // $dlg = QDialog::Alert('Apa yang akan anda dilakukan?', ['Login','Batal']);
                        QApplication::DisplayAlert(sprintf('Silahkan Tunggu %s menit!', $wait - $x));
                    }
                }else{
                    $y = 1;
                }
                if($y == 1){
                    date_default_timezone_set('Asia/Jakarta');

                    $_SESSION[__USER_LOGIN__] = $objUser->UserId;
                    $_SESSION[__USERNAME_LOGIN__] = $objUser->Username;
                    $_SESSION['__ROLE_CONFIGURATION__'] = $objConfiguration;


                    if($objUser->LastLogin){
                        # USER LOGIN !
                        $_SESSION[__USER_TIPE__] = $objUser->RoleId;
                        $_SESSION[__ROLE_NAME__] = $objUser->RoleName;
                        $_SESSION[__USER_ROLE__] = $objUser->RoleLevel;
                        $_SESSION[__USER_BRANCH__] = $objUser->BranchCode;
                        $_SESSION[__BRANCH_NAME__] = $objUser->BranchName;
                        $_SESSION[__STAGING__] = NULL;
                        $objUser->IsLoggedIn = true;
                        $objUser->LockedCount = 0;
                        $objUser->LockedSession = session_id();
                        $objUser->LastLogin = QDateTime::Now();
                        $objUser->Save();

                        # AUDIT TRAIL !
                        // $log = new LogActivity();
                        // $log->SaveIsLogin();

                        QApplication::Redirect(__SUBDIRECTORY__.'/home.php');
                    }else{
                        QApplication::Redirect(__SUBDIRECTORY__.'/src/login/changePassword.php');
                    }
                }
            }
        }
    }
}
LoginForm::Run('LoginForm');
?>
