<!DOCTYPE html>
<html>
<head>
    <title>Regulatory Solution</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="<?php _p(__APP_IMAGE_ASSETS__); ?>/logo-title2.png">
    <meta charset="UTF-8" />
    <meta name="description" content="Regulatory Solution">

    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/app.min.css");</style>
    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/font-awesome.min.css");</style>
    <style type="text/css">@import url("<?php _p(__APP_CSS_ASSETS__); ?>/bootstrap.min.css");</style>

    <!-- JQUERY -->
    <script type="text/javascript" src="<?php _p(__APP_JQUERY_ASSETS__)?>/jQuery-2.1.4.min.js"></script>

    <!-- JavaScript-->
    <script type="text/javascript" src="<?php _p(__APP_JS_ASSETS__)?>/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php _p(__APP_JS_ASSETS__)?>/app.min.js"></script>

    <style type="text/css" media="screen">

        .login-box{
        }

        .login-box-body{
            padding: 15px 15px 15px 15px;
            /*text-align: center;*/
        }

        .login-box-body img{
            width: 100%;
        }
    </style>

</head>

<body class="login-page">
<?php
//phpinfo();
?>
<?php $this->RenderBegin() ?>
    <div class="login-box">
        <div class="login-box-body">
            <div class="login-logo">
                <a href="#" class="logo"><img src="<?php _p(__APP_IMAGE_ASSETS__) ?>/login-logo.png" /> </a>
            </div>
            <p class="login-box-msg"><h4>Change Password</h4></p>
            <div class="form-group has-feedback">
                <?php $this->txtNewPassword->Render() ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php $this->txtPassword->Render() ?>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <?php $this->lblPwdWarning->Render() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?php
                $this->btnChange->Render() ;
                $this->objDefaultWaitIcon->Render();
                ?>
            </div>
        </div>
    </div>
</body>
<?php $this->RenderEnd() ?>

<script type = "text/javascript" >
    function changeHashOnLoad() {
        window.location.href += '#';
        setTimeout('changeHashAgain()', '50');
    }

    function changeHashAgain() {
        window.location.href += '1';
    }

    var storedHash = window.location.hash;
    window.setInterval(function () {
        if (window.location.hash != storedHash) {
            window.location.hash = storedHash;
        }
    }, 50);
    window.onload=changeHashOnLoad;
</script>

</html>
