<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

class ChangePasswordForm extends QForm {

    protected $txtNewPassword;
    protected $txtPassword;
    protected $txtUserId;
    protected $btnChange;
    protected $objUser;
    protected $objPasswordHistory;
    protected $lblPwdWarning;

    protected function Form_Create() {
        $this->objDefaultWaitIcon = new QWaitIcon($this);
        $this->txtNewPassword = new QTextBox($this);
        $this->txtNewPassword->Required = true;
        $this->txtNewPassword->Placeholder = 'New Password';
        $this->txtNewPassword->TextMode = QTextMode::Password;
        $this->txtNewPassword->CssClass = 'form-control';

        $this->txtPassword = new QTextBox($this);
        $this->txtPassword->Required = true;
        $this->txtPassword->Placeholder = 'Confirmation';
        $this->txtPassword->TextMode = QTextMode::Password;
        $this->txtPassword->CssClass = 'form-control';

        $this->lblPwdWarning = new QLabel($this);
        $this->lblPwdWarning->Text = 'Password should be a series of alphabetic, numeric , uppercase , lowercase and characters special !';
        $this->lblPwdWarning->Visible = false;

        $this->btnChange = new QButton($this);
        $this->btnChange->Text = QApplication::Translate('Change and Continue');
        $this->btnChange->AddAction(new QClickEvent(), new QAjaxAction('btnLogin_Click'));
        $this->btnChange->AddAction(new QEnterKeyEvent(), new QAjaxAction('btnLogin_Click'));
        $this->btnChange->PrimaryButton = true;
        $this->btnChange->CssClass = 'btn btn-primary btn-block btn-flat';
        $this->btnChange->CausesValidation = true;

        System::NavigationControl($this->txtNewPassword,$this->txtPassword);
        System::NavigationControl($this->txtPassword,$this->btnChange);
    }


    protected function Form_Validate() {
        $blnToReturn = true;

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            $objControl->Blink();
        }

        if(!preg_match("/^.*(?=.{8,})(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\W]+).*$/", $this->txtNewPassword->Text)) {
            $this->lblPwdWarning->Visible = true;
            $this->txtPassword->Text = '';
            $blnToReturn = false;
        }else{
            $this->lblPwdWarning->Visible = false;
            $blnToReturn = true;
        }

        return $blnToReturn;
    }

    protected function btnLogin_Click($strFormId, $strControlId, $strParameter) {
        if($this->txtNewPassword->Text != $this->txtPassword->Text){
            QApplication::DisplayAlert('Confirmation Password and New Password are not Same');
            $this->txtNewPassword->Text = '';
        }else{
            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            $objRole = RoleUser::Load($objUser->RoleId);
            $objConfiguration = TSetting::LoadByRoleLevel($objRole->RoleLevel);
            $_SESSION[__USERNAME_LOGIN__] = $objUser->Username;
            $_SESSION['__ROLE_CONFIGURATION__'] = $objConfiguration;
            $_SESSION[__USER_TIPE__] = $objUser->RoleId;
            $_SESSION[__ROLE_NAME__] = $objUser->RoleName;
            $_SESSION[__USER_ROLE__] = $objUser->RoleLevel;
            $_SESSION[__USER_BRANCH__] = $objUser->BranchCode;
            $_SESSION[__BRANCH_NAME__] = $objUser->BranchName;
            $_SESSION[__STAGING__] = NULL;

            # USER PASSWORD HISTORY !
            $objUserHistory = new UsersHistoryPassword();
            $objUserHistory->UserId= QApplication::PathInfo(0);
            $objUserHistory->UserIdHistory= System::GetId();
            $objUserHistory->UserId = $objUser->UserId;
            $objUserHistory->LastPassword = $objUser->Password = System::getHash($this->txtNewPassword->Text);
            $param = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM','PASSWORD_COUNT');
            $objUserHistory->CountPassword= $param->ParamValue;
            $objUserHistory->IndexUser = 1;
            $objUserHistory->Save();

            # SAVE NEW PASSWORD !
            $objUser->Password = System::getHash($this->txtNewPassword->Text);
            $objUser->ExpiredCount = 0;
            $objUser->IsLoggedIn = true;
            $objUser->LockedCount = 0;
            $objUser->LockedSession = session_id();
            $objUser->LastLogin = QDateTime::Now();
            $objUser->Save();

            # AUDIT TRAIL ! 
            $log = new LogActivity();
            $log->SaveChangePassword();
            QApplication::Redirect(__SUBDIRECTORY__.'/home.php');
        }
    }
}
ChangePasswordForm::Run('ChangePasswordForm');
?>
