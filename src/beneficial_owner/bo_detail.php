<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

    class BoSuspectListForm extends QForm {
        protected $dtgBoSuspect;
        protected $dtgOffshoreSuspect;
        protected $dtgPanamaSuspect;
        protected $dtgParadiseSuspect;
        protected $dtgBermudaSuspect;
        protected $objLinkProxy;
        protected $lstAutoComplete;
        protected $lstAutoComplete2;
        protected $textSearch1;
        protected $textSearch2;
        protected $lstParam1;
        protected $lstParam2;
        protected $btnFilter;
        protected $datePicker1;
        protected $datePicker2;


    protected function Form_Run() { 
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();

        $this->dtgBoSuspect = new BoSuspectDataGrid($this);

        $this->dtgBoSuspect->Paginator = new QPaginator($this->dtgBoSuspect);
        $this->dtgBoSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
        $this->dtgBoSuspect->ShowFilter = false;
        $this->dtgBoSuspect->ShowSearch = false;
        $this->dtgBoSuspect->Width = '100%';

        $this->objLinkProxy = new QControlProxy($this);
        $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

        $this->dtgBoSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
        $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["data_code_target"] ?>','HtmlEntities=false','Width=50'));
        $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Full Name','<?= $_ITEM["bo_name_target"] ?>','HtmlEntities=false','Width=600'));
        $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Last Update','<?= $_ITEM["last_update"] ?>','HtmlEntities=false','Width=30'));
        $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM["bo_suspect_id"]) ?>','HtmlEntities=false','Width=10'));

        $this->dtgBoSuspect->SetDataBinder('PageBinder', $this);

        $this->dtgOffshoreSuspect = new OffshoreSuspectDataGrid($this);

        $this->dtgOffshoreSuspect->Paginator = new QPaginator($this->dtgOffshoreSuspect);
        $this->dtgOffshoreSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
        $this->dtgOffshoreSuspect->ShowFilter = false;
        $this->dtgOffshoreSuspect->ShowSearch = false;
        $this->dtgOffshoreSuspect->Width = '100%';

        $this->dtgOffshoreSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
        $this->dtgOffshoreSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["off_data_code_source"] ?>','HtmlEntities=false','Width=50'));
        $this->dtgOffshoreSuspect->AddColumn(new QDataGridColumn('Full Name','<?= $_ITEM["off_name_source"] ?>','HtmlEntities=false','Width=600'));
        $this->dtgOffshoreSuspect->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM["off_suspect_id"]) ?>','HtmlEntities=false','Width=10'));
        

        $this->dtgOffshoreSuspect->SetDataBinder('PageBinderOffshore', $this);

        $this->dtgPanamaSuspect = new OffshoreSuspectDataGrid($this);

        $this->dtgPanamaSuspect->Paginator = new QPaginator($this->dtgPanamaSuspect);
        $this->dtgPanamaSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
        $this->dtgPanamaSuspect->ShowFilter = false;
        $this->dtgPanamaSuspect->ShowSearch = false;
        $this->dtgPanamaSuspect->Width = '100%';

        $this->dtgPanamaSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
        $this->dtgPanamaSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["off_data_code_source"] ?>','HtmlEntities=false','Width=50'));
        $this->dtgPanamaSuspect->AddColumn(new QDataGridColumn('Full Name','<?= $_ITEM["off_name_source"] ?>','HtmlEntities=false','Width=600'));
        $this->dtgPanamaSuspect->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM["off_suspect_id"]) ?>','HtmlEntities=false','Width=10'));
        

        $this->dtgPanamaSuspect->SetDataBinder('PageBinderOffshore', $this);

        $this->dtgParadiseSuspect = new OffshoreSuspectDataGrid($this);

        $this->dtgParadiseSuspect->Paginator = new QPaginator($this->dtgParadiseSuspect);
        $this->dtgParadiseSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
        $this->dtgParadiseSuspect->ShowFilter = false;
        $this->dtgParadiseSuspect->ShowSearch = false;
        $this->dtgParadiseSuspect->Width = '100%';

        $this->dtgParadiseSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
        $this->dtgParadiseSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["off_data_code_source"] ?>','HtmlEntities=false','Width=50'));
        $this->dtgParadiseSuspect->AddColumn(new QDataGridColumn('Full Name','<?= $_ITEM["off_name_source"] ?>','HtmlEntities=false','Width=600'));
        $this->dtgParadiseSuspect->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM["off_suspect_id"]) ?>','HtmlEntities=false','Width=10'));
        

        $this->dtgParadiseSuspect->SetDataBinder('PageBinderOffshore', $this);

        $this->dtgBermudaSuspect = new OffshoreSuspectDataGrid($this);

        $this->dtgBermudaSuspect->Paginator = new QPaginator($this->dtgBermudaSuspect);
        $this->dtgBermudaSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
        $this->dtgBermudaSuspect->ShowFilter = false;
        $this->dtgBermudaSuspect->ShowSearch = false;
        $this->dtgBermudaSuspect->Width = '100%';

        $this->dtgBermudaSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
        $this->dtgBermudaSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["off_data_code_source"] ?>','HtmlEntities=false','Width=50'));
        $this->dtgBermudaSuspect->AddColumn(new QDataGridColumn('Full Name','<?= $_ITEM["off_name_source"] ?>','HtmlEntities=false','Width=600'));
        $this->dtgBermudaSuspect->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM["off_suspect_id"]) ?>','HtmlEntities=false','Width=10'));
        

        $this->dtgBermudaSuspect->SetDataBinder('PageBinderOffshore', $this);

        $this->textSearch1 = new QTextBox($this);
        $this->textSearch1->hide();
        $this->textSearch1->ActionParameter = 'changeParam2';
        $this->textSearch1->AddAction(new QEnterKeyEvent(), new QAjaxAction('pageAction'));
        $this->textSearch1->hide();

        $this->textSearch2 = new QTextBox($this);
        $this->textSearch2->hide();
        $this->textSearch2->Width = '250';
        $this->textSearch2->hide();

        $this->datePicker1 = new QDateRangePicker($this);
        $this->datePicker1->Input = new QTextBox($this);
        $this->datePicker1->Input->Name = "Mulai";
        $this->datePicker1->CloseOnSelect= true;
        $this->datePicker1->JqDateFormat = 'yy-mm-dd';
        $this->datePicker1->Input->Hide();

        $this->datePicker2 = new QDateRangePicker($this);
        $this->datePicker2->Input = new QTextBox($this);
        $this->datePicker2->Input->Name = "Mulai";
        $this->datePicker2->CloseOnSelect= true;
        $this->datePicker2->JqDateFormat = 'yy-mm-dd';
        $this->datePicker2->Input->Hide();

        $this->lstParam1 = new QListBox($this);
        $this->lstParam1->Name = 'Param 1';
        $this->lstParam1->Width = '160';
        $this->lstParam1->AddItem('-Any-');
        $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
        if($objUser->Role->RoleName != 'BSH'){
            $this->lstParam1->AddItem('Branch','bankBranch');
        }
        // $this->lstParam1->AddItem('CIF','cif');
        $this->lstParam1->AddItem('Customer Name','custName');
        $this->lstParam1->AddItem('Tanggal','tanggal');
        // $this->lstParam1->AddItem('No Rekening','accNumber');
        // $this->lstParam1->AddItem('Report Status','reportStatus');
        $this->lstParam1->ActionParameter = 'changeParam1';
        $this->lstParam1->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

        $this->lstParam2 = new QListBox($this);
        $this->lstParam2->Name = 'Param 2';
        $this->lstParam2->Width = '160';
        $this->lstParam2->AddItem('-Any-');
        if($objUser->Role->RoleName != 'BSH'){
            $this->lstParam2->AddItem('Branch','bankBranch');
        }
        // $this->lstParam2->AddItem('CIF','cif');
        $this->lstParam2->AddItem('Customer Name','custName');
        $this->lstParam2->AddItem('Tanggal','tanggal');
        // $this->lstParam2->AddItem('No Rekening','accNumber');
        // $this->lstParam2->AddItem('Report Status','reportStatus');
        $this->lstParam2->ActionParameter = 'changeParam3';
        $this->lstParam2->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

        $this->btnFilter = new QButton($this);
        $this->btnFilter->Text = QApplication::Translate('Filter');
        $this->btnFilter->CssClass = 'btn btn-primary';
        $this->btnFilter->ActionParameter = 'changeParam2';
        $this->btnFilter->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));
        $this->btnFilter->CausesValidation = true;

        $this->lstAutoComplete = new QSelect2ListBox($this);
        $this->lstAutoComplete->Width = '250';
        $this->lstAutoComplete->AddItem('- Select One -', null);


        $this->lstAutoComplete2 = new QSelect2ListBox($this);
        $this->lstAutoComplete2->Width = '250';
        $this->lstAutoComplete2->AddItem('- Select One -', null);

    }

    public function rowActionButton($record)
    {
        $strReturn = null;
        $strReturn .= sprintf('<a href="%s" class="btn-sm btn-warning" title="Detail BO"><span class="fa fa-pencil"></span></a>',__SOURCE__ . '/beneficial_owner/bo_form.php/'.$record);

        return $strReturn;
    }

    public function PageBinder(){

        $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
        
        $val1 = null; $val2 = null;

        $param1 = $this->lstParam1->SelectedValue;
        $param2 = $this->lstParam2->SelectedValue;

        $textfield = ['first_name', 'customer_id', 'account_number'];
        $date = ['trx_value_date'];
        $select = ['customer_type_id','branch_code','bank_type','report_status'];

        if (in_array($param1,$textfield)){
            $val1 = $this->textSearch1->Text;
        }elseif (in_array($param1,$date)){
            if ($this->datePicker1->Input->Text) {
                    $exval1 = explode(' - ',$this->datePicker1->Input->Text);
                    if (count($exval1) > 1) {
                        $val1[0] = $exval1[0]; 
                        $val1[1] = $exval1[1]; 
                    }else{
                        $val1[0] = $exval1[0]; 
                        $val1[1] = $exval1[0];
                    }
                }else{
                    $val1 = NULL;
                }
        }elseif (in_array($param1,$select)) {
            $val1 = $this->lstAutoComplete->SelectedValue; 
        }

        if (in_array($param2,$textfield)){
            $val2 = $this->textSearch2->Text; 
        }elseif (in_array($param2,$date)){
            if ($this->datePicker2->Input->Text) {
                    $exval2 = explode(' - ',$this->datePicker2->Input->Text);
                    if (count($exval2) > 1) {
                        $val2[0] = $exval2[0]; 
                        $val2[1] = $exval2[1]; 
                    }else{
                        $val2[0] = $exval2[0]; 
                        $val2[1] = $exval2[0];
                    } 
                }else{
                    $val2 = NULL;
                }
        }elseif (in_array($param2,$select)) {
            $val2 = $this->lstAutoComplete2->SelectedValue; 
        }

        $y1 = ''; $y2 = ''; $q1 = ''; $q2 = '';

        switch ($param1) {
            case 'account_number':   $x1 = 'account_number';      $y1 = 'free_text';      break;
            case 'branch_code':      $x1 = 'branch_code';         $y1 = 'free_text';      break;
            case 'first_name':       $x1 = 'UPPER(first_name)';   $y1 = 'free_text';      break;
            case 'customer_id':      $x1 = 'customer_id';         $y1 = 'fixed_text';     break;
            case 'trx_value_date':   $x1 = 'trx_value_date';      $y1 = 'date_range';     break;
            case 'customer_type_id': $x1 = 'customer_type_id';    $y1 = 'fixed_value';    break;
            case 'bank_type':        $x1 = 'bank_type';           $y1 = 'fixed_value';    break;
            case 'report_status':    $x1 = 'report_status';       $y1 = 'fixed_value';    break;
        }

        switch ($param2) {
            case 'account_number':   $x2 = 'account_number';      $y2 = 'free_text';      break;
            case 'branch_code':      $x2 = 'branch_code';         $y2 = 'free_text';      break;
            case 'first_name':       $x2 = 'UPPER(first_name)';   $y2 = 'free_text';      break;
            case 'customer_id':      $x2 = 'customer_id';         $y2 = 'fixed_text';     break;
            case 'trx_value_date':   $x2 = 'trx_value_date';      $y2 = 'date_range';     break;
            case 'customer_type_id': $x2 = 'customer_type_id';    $y2 = 'fixed_value';    break;
            case 'bank_type':        $x2 = 'bank_type';           $y2 = 'fixed_value';    break;
            case 'report_status':    $x2 = 'report_status';       $y2 = 'fixed_value';    break;
        }

        switch ($y1) {
            case 'free_text'    : $q1 = "and ".$x1." like '%".strtoupper(trim($val1))."%'";        break;
            case 'date_range'   : $q1 = "and ".$x1." between '".$val1[0]."' and '".$val1[1]."'";   break;
            case 'fixed_text'   : $q1 = "and ".$x1." = '".$val1."'";                               break;
            case 'fixed_value'  : $q1 = "and ".$x1." = '".$val1."'";                               break;
        }

        switch ($y2) {
            case 'free_text'    : $q2 = "and ".$x2." like '%".strtoupper(trim($val2))."%'";        break;
            case 'date_range'   : $q2 = "and ".$x2." between '".$val2[0]."' and '".$val2[1]."'";   break;
            case 'fixed_text'   : $q2 = "and ".$x2." = '".$val2."'";                               break;
            case 'fixed_value'  : $q2 = "and ".$x2." = '".$val2."'";                               break;
        }

        $condition = "";

        if      ($val1 != NULL && $val2 != NULL) { $condition .= " ".$q1." ".$q2;    }
        elseif  ($val1 != NULL && $val2 == NULL) { $condition .= " ".$q1;            }
        elseif  ($val1 == NULL && $val2 != NULL) { $condition .= " ".$q2;            }
        else                                     { $condition .= "";                 }

        $intOffset = ($this->dtgBoSuspect->PageNumber - 1) * $this->dtgBoSuspect->ItemsPerPage;   

        $sql = "SELECT bo_suspect_id, bo_name_target, parent_code, data_code_target, last_update 
                FROM bo_suspect 
                WHERE parent_code = '".QApplication::pathinfo(0)."' ".$condition." AND approval_status = TRUE ";

        $sqlCount = "SELECT COUNT(1) FROM (".$sql.") AS X ";

        $sql .=" LIMIT 10 OFFSET ".$intOffset;

        $objDatabase = QApplication::$Database[1];
        $objDbResult = $objDatabase->Query($sql);

        $dataCount = $objDatabase->Query($sqlCount);

        $count = 0;
        while ($cRow = $dataCount->FetchArray()) {
            $count = $cRow[0];
        };

        $array = array();
        while ($mixRow = $objDbResult->FetchArray()) {
            array_push($array, $mixRow);
        }

        $this->dtgBoSuspect->DataSource = $array;
        $this->dtgBoSuspect->TotalItemCount = $count;
        $this->dtgBoSuspect->UseAjax = TRUE;
    }

    public function PageBinderOffshore(){

        $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
        
        $val1 = null; $val2 = null;

        $param1 = $this->lstParam1->SelectedValue;
        $param2 = $this->lstParam2->SelectedValue;

        $textfield = ['first_name', 'customer_id', 'account_number'];
        $date = ['trx_value_date'];
        $select = ['customer_type_id','branch_code','bank_type','report_status'];

        if (in_array($param1,$textfield)){
            $val1 = $this->textSearch1->Text;
        }elseif (in_array($param1,$date)){
            if ($this->datePicker1->Input->Text) {
                    $exval1 = explode(' - ',$this->datePicker1->Input->Text);
                    if (count($exval1) > 1) {
                        $val1[0] = $exval1[0]; 
                        $val1[1] = $exval1[1]; 
                    }else{
                        $val1[0] = $exval1[0]; 
                        $val1[1] = $exval1[0];
                    }
                }else{
                    $val1 = NULL;
                }
        }elseif (in_array($param1,$select)) {
            $val1 = $this->lstAutoComplete->SelectedValue; 
        }

        if (in_array($param2,$textfield)){
            $val2 = $this->textSearch2->Text; 
        }elseif (in_array($param2,$date)){
            if ($this->datePicker2->Input->Text) {
                    $exval2 = explode(' - ',$this->datePicker2->Input->Text);
                    if (count($exval2) > 1) {
                        $val2[0] = $exval2[0]; 
                        $val2[1] = $exval2[1]; 
                    }else{
                        $val2[0] = $exval2[0]; 
                        $val2[1] = $exval2[0];
                    } 
                }else{
                    $val2 = NULL;
                }
        }elseif (in_array($param2,$select)) {
            $val2 = $this->lstAutoComplete2->SelectedValue; 
        }

        $y1 = ''; $y2 = ''; $q1 = ''; $q2 = '';

        switch ($param1) {
            case 'account_number':   $x1 = 'account_number';      $y1 = 'free_text';      break;
            case 'branch_code':      $x1 = 'branch_code';         $y1 = 'free_text';      break;
            case 'first_name':       $x1 = 'UPPER(first_name)';   $y1 = 'free_text';      break;
            case 'customer_id':      $x1 = 'customer_id';         $y1 = 'fixed_text';     break;
            case 'trx_value_date':   $x1 = 'trx_value_date';      $y1 = 'date_range';     break;
            case 'customer_type_id': $x1 = 'customer_type_id';    $y1 = 'fixed_value';    break;
            case 'bank_type':        $x1 = 'bank_type';           $y1 = 'fixed_value';    break;
            case 'report_status':    $x1 = 'report_status';       $y1 = 'fixed_value';    break;
        }

        switch ($param2) {
            case 'account_number':   $x2 = 'account_number';      $y2 = 'free_text';      break;
            case 'branch_code':      $x2 = 'branch_code';         $y2 = 'free_text';      break;
            case 'first_name':       $x2 = 'UPPER(first_name)';   $y2 = 'free_text';      break;
            case 'customer_id':      $x2 = 'customer_id';         $y2 = 'fixed_text';     break;
            case 'trx_value_date':   $x2 = 'trx_value_date';      $y2 = 'date_range';     break;
            case 'customer_type_id': $x2 = 'customer_type_id';    $y2 = 'fixed_value';    break;
            case 'bank_type':        $x2 = 'bank_type';           $y2 = 'fixed_value';    break;
            case 'report_status':    $x2 = 'report_status';       $y2 = 'fixed_value';    break;
        }

        switch ($y1) {
            case 'free_text'    : $q1 = "and ".$x1." like '%".strtoupper(trim($val1))."%'";        break;
            case 'date_range'   : $q1 = "and ".$x1." between '".$val1[0]."' and '".$val1[1]."'";   break;
            case 'fixed_text'   : $q1 = "and ".$x1." = '".$val1."'";                               break;
            case 'fixed_value'  : $q1 = "and ".$x1." = '".$val1."'";                               break;
        }

        switch ($y2) {
            case 'free_text'    : $q2 = "and ".$x2." like '%".strtoupper(trim($val2))."%'";        break;
            case 'date_range'   : $q2 = "and ".$x2." between '".$val2[0]."' and '".$val2[1]."'";   break;
            case 'fixed_text'   : $q2 = "and ".$x2." = '".$val2."'";                               break;
            case 'fixed_value'  : $q2 = "and ".$x2." = '".$val2."'";                               break;
        }

        $condition = "";

        if      ($val1 != NULL && $val2 != NULL) { $condition .= " ".$q1." ".$q2;    }
        elseif  ($val1 != NULL && $val2 == NULL) { $condition .= " ".$q1;            }
        elseif  ($val1 == NULL && $val2 != NULL) { $condition .= " ".$q2;            }
        else                                     { $condition .= "";                 }

        $intOffset = ($this->dtgOffshoreSuspect->PageNumber - 1) * $this->dtgOffshoreSuspect->ItemsPerPage;   

        $sql = "SELECT off_suspect_id, off_name_source, off_data_code_source
                FROM offshore_suspect ";
 // WHERE parent_code = '".QApplication::pathinfo(0)."' ".$condition;
        $sqlCount = "SELECT COUNT(1) FROM (".$sql.") AS X ";

        $sql .=" LIMIT 10 OFFSET ".$intOffset;

        $objDatabase = QApplication::$Database[1];
        $objDbResult = $objDatabase->Query($sql);

        $dataCount = $objDatabase->Query($sqlCount);

        $count = 0;
        while ($cRow = $dataCount->FetchArray()) {
            $count = $cRow[0];
        };

        $array = array();
        while ($mixRow = $objDbResult->FetchArray()) {
            array_push($array, $mixRow);
        }

        $this->dtgOffshoreSuspect->DataSource = $array;
        $this->dtgOffshoreSuspect->TotalItemCount = $count;
        $this->dtgOffshoreSuspect->UseAjax = TRUE;
    }

    protected function pageAction($strFormId, $strControlId, $strParameter) {
        if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
        else $section = $strParameter;

        switch($section)
        {
            case'del':
                $obj = DimWic::Load($param);
                $obj->Delete();
                QApplication::DisplayAlert($obj->NamaNasabah. ' has been Removed');
                $this->dtgBoSuspect->Refresh();
                break;

            case 'change':
                $this->dtgBoSuspect->Refresh();
                break;
            case 'changeParam1':
                if($this->lstParam1->SelectedValue == 'tanggal'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->hide();
                    $this->textSearch1->hide();
                    $this->datePicker1->Input->unhide();
                }elseif($this->lstParam1->SelectedValue == 'cif'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->hide();
                    $this->textSearch1->unhide();
                    $this->textSearch1->Text = '';
                    $this->datePicker1->Input->hide();    
                }elseif($this->lstParam1->SelectedValue == 'custName' or $this->lstParam1->SelectedValue == 'accNumber'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->hide();
                    $this->textSearch1->unhide();
                    $this->textSearch1->Text = '';  
                    $this->datePicker1->Input->hide();  
                }elseif($this->lstParam1->SelectedValue == 'reportStatus'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('- Any -');
                    $this->lstAutoComplete->AddItem('Belum Dikerjakan','BLANK');
                    $this->lstAutoComplete->AddItem('Dikecualikan','1');
                    $this->lstAutoComplete->AddItem('Dilaporkan','2');
                    $this->textSearch1->hide();
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->Text = '';
                    $this->datePicker1->Input->hide();
                }elseif($this->lstParam1->SelectedValue == 'bankBranch'){
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->lstAutoComplete->AddItem('-Any-');

                    if($arrBranch = BankBranch::LoadAll()){
                        foreach($arrBranch as $bankBranch){
                            $this->lstAutoComplete->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                        }
                    }
                    $this->lstAutoComplete->unhide();
                    $this->textSearch1->hide();
                    $this->datePicker1->Input->hide();
                }else{
                    $this->lstAutoComplete->RemoveAllItems();
                    $this->textSearch1->Text = '';
                    $this->datePicker1->Input->Text = '';
                    $this->lstAutoComplete->AddItem('-Any-');
                    $this->dtgBoSuspect->Refresh();
                }
            break;
            case 'changeParam2':
                    $this->dtgBoSuspect->Refresh();
                break;
            case 'changeParam3':
                if($this->lstParam2->SelectedValue == 'tanggal'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->hide();
                    $this->textSearch2->hide();
                    $this->datePicker2->Input->unhide();
                }elseif($this->lstParam2->SelectedValue == 'custName' or $this->lstParam1->SelectedValue == 'accNumber' or $this->lstParam2->SelectedValue == 'cif'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->hide();
                    $this->textSearch2->unhide();
                    $this->textSearch2->Text = '';
                    $this->datePicker2->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'reportStatus'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('- Any -');
                    $this->lstAutoComplete2->AddItem('Belum Dikerjakan','BLANK');
                    $this->lstAutoComplete2->AddItem('Dikecualikan','1');
                    $this->lstAutoComplete2->AddItem('Dilaporkan','2');
                    $this->textSearch2->hide();
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->Text = '';
                    $this->datePicker2->Input->hide();
                }elseif($this->lstParam2->SelectedValue == 'bankBranch'){
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');

                    if($arrBranch = BankBranch::LoadAll()){
                        foreach($arrBranch as $bankBranch){
                            $this->lstAutoComplete2->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                        }
                    }
                    $this->lstAutoComplete2->unhide();
                    $this->textSearch2->hide();
                    $this->datePicker2->Input->hide();
                }else{
                    $this->lstAutoComplete2->RemoveAllItems();
                    $this->lstAutoComplete2->AddItem('-Any-');
                    $this->datePicker2->Input->Text = '';
                    $this->dtgBoSuspect->Refresh();
                    $this->textSearch2->Text = '';
                }
            break;
        }
    }

    public function getBranchName($id){
        $objBranch = BankBranch::QuerySingle(
            QQ::Equal(QQN::BankBranch()->BranchCode,$id)
        );

        if($objBranch){
            return $objBranch->BranchName;
        }else{
            return null;
        }
    }

    public function getBranchID($id){
        $objBranch = BankBranch::QuerySingle(
            QQ::Equal(QQN::BankBranch()->BankBranchId,$id)
        );

        if($objBranch){
            return $objBranch->BranchCode;
        }else{
            return null;
        }
    }

    public function getCustomerType($id){
        switch ($id) {
            case 'R': $x = 'Personal';  break;
            default:  $x = 'Corporate'; break;
        }
        return $x;
    }
}
BoSuspectListForm::Run('BoSuspectListForm');
?>