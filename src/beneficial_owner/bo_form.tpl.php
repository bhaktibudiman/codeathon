<?php
$strPageTitle = QApplication::Translate('Cash Transaction Detail');
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-xs-6">
            <div class="box">
                <div class="box-header with-border">
                    <h3>Beneficial Owner</h3>
                </div>
                <div class="box-body ">
                    <div class="form-group">
                        <?php isset($this->txtDataCodeSource) ? $this->txtDataCodeSource->RenderWithBootstrapInGroup(3,9) : null; ?>
                    </div>
                    <div class="form-group">
                        <?php isset($this->txtBoNameSource) ? $this->txtBoNameSource->RenderWithBootstrapInGroup(3,9) : null; ?>
                    </div>
                    <div class="form-group">
                        <?php isset($this->txtDataCodeTarget) ? $this->txtDataCodeTarget->RenderWithBootstrapInGroup(3,9) : null; ?>
                    </div>
                    <div class="form-group">
                        <?php isset($this->txtBoNameTarget) ? $this->txtBoNameTarget->RenderWithBootstrapInGroup(3,9) : null; ?>
                    </div>
                    <div class="form-group" style="padding: right">
                        <?php isset($this->btnCancel) ? $this->btnCancel->Render() : null; ?>
                        <?php //isset($this->btnApprove) ? $this->btnApprove->Render() : null; ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#myTab a").click(function(e){
                e.preventDefault();
                $(this).tab('show');
            });

            $('textarea').alphanumericOnly();
        });
    </script>
<?php $this->RenderEnd() ?>
<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>
