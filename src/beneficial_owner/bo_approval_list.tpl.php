<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

<section class="content-header">
    <h1>Beneficial Owner Approval</h1>
</section>
<br>
<div class="box box-primary">
        <div class="box-body">
            <div>
                <div class="col-md-2">
                    <label>Param 1</label> 
                        <?php 
                            $this->lstParam1->Render(); 
                        ?>
                </div>
                <div class="col-md-3">
                    <label>Pencarian</label> 
                        <?php 
                            $this->lstAutoComplete->Render(); 
                            $this->textSearch1->Render(); 
                            $this->datePicker1->Input->Render();
                        ?>
                </div>

                <div class="col-md-2">
                    <label>Param 2</label> 
                    <?php 
                        $this->lstParam2->Render();     
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Pencarian</label> 
                        <?php 
                            $this->lstAutoComplete2->Render();
                            $this->textSearch2->Render(); 
                            $this->datePicker2->Input->Render();
                            $this->datePicker1->Render();  
                            $this->datePicker2->Render();  
                        ?>
                </div>
                <div class="col-md-1">
                    <label>.</label> 
                    <?php $this->btnFilter->Render(); ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php $this->dtgBoSuspect->Render(); ?>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>
