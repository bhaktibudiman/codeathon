<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

    <section class="content-header">
        <?php 
            if (QApplication::PathInfo(0)) {
                echo "<h1>List Detail Suspect Screening</h1>";
            }else{
                echo "<h1>NEGATIVE LIST</h1>";
            }
        ?>
    </section>

    <div class="box box-primary">
        <div class="box-body">
            <div>
                <div class="col-md-3">
                    <label>Type</label> 
                        <?php 
                            $this->lstParam1->Render(); 
                        ?>
                </div>

                <div class="col-md-3">
                    <label>Suspect Name</label> 
                    <?php 
                        $this->textSearch2->Render(); 
                    ?>
                </div>
                <div class="col-md-3">
                    <label>Description</label> 
                        <?php 
                            $this->textSearch3->Render(); 
                        ?>
                </div>
                <div class="col-md-3">
                    <label>Suspect Number</label> 
                        <?php 
                            $this->textSearch1->Render(); 
                        ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php $this->dtgDetailSuspectScreening->Render(); ?>
        </div>
        <div class="box-footer">
            <div class="form-actions">
                <!-- <a href="javascript:" onclick="history.go(-1);" class="btn btn-warning" title="Back"><span class="fa fa-arrow-circle-left"></span> Back</a> -->
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>