<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

	class HeaderUploadSuspectEditForm extends QForm {
        // Local instance of the HeaderUploadSuspectMetaControl
        /**
         * @var HeaderUploadSuspectMetaControlGen mctHeaderUploadSuspect
         */
        protected $mctHeaderUploadSuspect;
        protected $lblUploadId;
        protected $txtFilename;
        protected $chkUploadStatus;
        protected $chkScreeningStatus;
        protected $calCreatedDate;
        protected $txtCreatedBy;
        protected $txtUrlStaging;

        /**
         * @var QButton Save
         */
        protected $btnSave;
        /**
         * @var QButton Delete
         */
        protected $btnDelete;
        /**
         * @var QButton Cancel
         */
        protected $btnCancel;


    //		protected function Form_Exit() {}
    //		protected function Form_Load() {}
    //		protected function Form_PreRender() {}

        protected function Form_Run() {
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->mctHeaderUploadSuspect = HeaderUploadSuspectMetaControl::CreateFromPathInfo($this);

            $this->lblUploadId = $this->mctHeaderUploadSuspect->lblUploadId_Create();
            $this->txtFilename = $this->mctHeaderUploadSuspect->txtFilename_Create();
            $this->txtFilename->Enabled = false;
            $this->chkUploadStatus = $this->mctHeaderUploadSuspect->chkUploadStatus_Create();
            $this->chkUploadStatus->Enabled = false;
            $this->chkScreeningStatus = $this->mctHeaderUploadSuspect->chkScreeningStatus_Create();
            $this->chkScreeningStatus->Enabled = false;
            $this->calCreatedDate = $this->mctHeaderUploadSuspect->calCreatedDate_Create();
            $this->txtCreatedBy = $this->mctHeaderUploadSuspect->txtCreatedBy_Create();
            $this->txtUrlStaging = $this->mctHeaderUploadSuspect->txtUrlStaging_Create();

            $this->btnSave = new QButton($this);
            $this->btnSave->Text = QApplication::Translate('Save');
            $this->btnSave->AddAction(new QClickEvent(), new QAjaxAction('btnSave_Click'));
            $this->btnSave->CssClass = 'btn btn-primary';
            $this->btnSave->CausesValidation = true;

            $this->btnCancel = new QButton($this);
            $this->btnCancel->Text = QApplication::Translate('Cancel');
            $this->btnCancel->CssClass = 'btn btn-warning';
            $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));

            $this->btnDelete = new QButton($this);
            $this->btnDelete->Text = QApplication::Translate('Delete');
            $this->btnDelete->CssClass = 'btn btn-danger';
            $this->btnDelete->AddAction(new QClickEvent(), new QConfirmAction(sprintf(QApplication::Translate('Are you SURE you want to DELETE this %s?'), QApplication::Translate('HeaderUploadSuspect'))));
            $this->btnDelete->AddAction(new QClickEvent(), new QAjaxAction('btnDelete_Click'));
            $this->btnDelete->Visible = $this->mctHeaderUploadSuspect->EditMode;
        }

        /**
         * This Form_Validate event handler allows you to specify any custom Form Validation rules.
         * It will also Blink() on all invalid controls, as well as Focus() on the top-most invalid control.
         */
        protected function Form_Validate() {
            // By default, we report the result of validation from the parent
            $blnToReturn = parent::Form_Validate();

            // TODO: Be sure to set $blnToReturn to false if any custom validation fails!

            $blnFocused = false;
            foreach ($this->GetErrorControls() as $objControl) {
                if (!$blnFocused) {
                    $objControl->Focus();
                    $blnFocused = true;
                }

                $objControl->Blink();
            }

            return $blnToReturn;
        }

        protected function btnSave_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Save" processing to the HeaderUploadSuspectMetaControl
            $this->mctHeaderUploadSuspect->SaveHeaderUploadSuspect();
            $this->RedirectToListPage();
        }

        protected function btnDelete_Click($strFormId, $strControlId, $strParameter) {
            // Delegate "Delete" processing to the HeaderUploadSuspectMetaControl
            $this->mctHeaderUploadSuspect->DeleteHeaderUploadSuspect();
            $this->RedirectToListPage();
        }

        protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
            $this->RedirectToListPage();
        }

        protected function RedirectToListPage() {
            QApplication::Redirect(__SOURCE__.'/apu/suspect_screening/head_list.php');
        }
	}

	// Go ahead and run this form object to render the page and its event handlers, implicitly using
	// header_upload_suspect_edit.tpl.php as the included HTML template file
	HeaderUploadSuspectEditForm::Run('HeaderUploadSuspectEditForm');
?>