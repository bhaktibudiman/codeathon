<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

	class HeaderUploadSuspectListForm extends QForm {

        protected $dtgSuspectScreening;
        protected $objLinkProxy;
        protected $hideButton;

		protected function Form_Run() {
			parent::Form_Run();
			QApplication::CheckRemoteAdmin();
            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);

            if($objUser->Role->RoleName == 'CS' || $objUser->Role->RoleName == 'CUSTOMER_SERVICE'){
                $this->hideButton = TRUE;
            }else{
                $this->hideButton = FALSE;
            }
		}

		protected function Form_Create() {
            parent::Form_Create();

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));

            $this->dtgSuspectScreening = new HeaderUploadSuspectDataGrid($this);
            $this->dtgSuspectScreening->Width = '100%';

            $this->dtgSuspectScreening->Paginator = new QPaginator($this->dtgSuspectScreening);
            $this->dtgSuspectScreening->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;

            $this->dtgSuspectScreening->ShowFilter = false;
            $this->dtgSuspectScreening->ShowFilterButton = false;
            $this->dtgSuspectScreening->ShowFilterResetButton = false;

            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Actions','<?= $_FORM->rowActionButton($_ITEM)?>','HtmlEntities=false','Width=100'));
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Filename','<?= $_ITEM->Filename ?>','HtmlEntities=false', 'Width=200'));
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Suspect Type', '<?= $_FORM->getSuspectType($_ITEM->ParameterId) ?>'));
            /*$this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Upload Status', '<?= $_FORM->getBoolean($_ITEM->UploadStatus) ?>','HtmlEntities=false', 'Width=200'));*/
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Screening Status', '<?= $_FORM->getBoolean($_ITEM->ScreeningStatus) ?>'));
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('URL Staging', '<?= $_ITEM->UrlStaging ?>'));
            $this->dtgSuspectScreening->AddColumn(new QDataGridColumn('Created Date', '<?= $_ITEM->CreatedDate ?>'));

            $this->dtgSuspectScreening->SetDataBinder('pageBinder');
        }

        protected function pageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            if($search = $this->dtgSuspectScreening->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::HeaderUploadSuspect()->Filename, $search)
                ));
            }

            $this->dtgSuspectScreening->MetaDataBinderCustom(new HeaderUploadSuspect(), QQ::AndCondition($objCondition));
        }

        public function getUser($id) {
            $objUser = Users::QuerySingle(
                QQ::Equal(QQN::Users()->UserId, $id)
            );
            $username = $objUser->Username;
            return $username;
        }

        function getSuspectType($param){
            $objApuParameter = ApuParameter::QuerySingle(QQ::Equal(QQN::ApuParameter()->ParameterId, $param));
            return (isset($objApuParameter) ? $objApuParameter->ParamDesc : NULL);
        }

        public function getBoolean($bool) {
            switch($bool){
                case TRUE:
                    $boolString = "SUCCESS";
                    break;
                case FALSE:
                    $boolString = "HASN'T EXECUTED";
                    break;
            }
            return $boolString;
        }

        public function rowActionButton($objRecord)
        {
            $strReturn = '';

            if($this->hideButton==false){
                $strReturn .= sprintf('<a href="%s" class="btn-sm btn-danger" title="Delete" onclick="return confirm(\'Delete File %s ?\')"><span class="fa fa-trash"></span></a>', __SOURCE__ . '/apu/suspect_screening/delete_list.php/?uploadid='.$objRecord->UploadId, $objRecord->Filename);
            }
                
		    $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            if($objUser->Role->RoleName != 'BSH'){
                $strReturn .= sprintf('&nbsp;<a href="%s" class="btn-sm btn-primary" title="Start Screening" onclick="return confirm(\'Start Screening For %s ?\')"><span class="fa fa-power-off"></span></a>', __SOURCE__ . '/service/'.$objRecord->UrlStaging.'/?parameterid='.$objRecord->ParameterId.'&uploadid='.$objRecord->UploadId,$objRecord->Filename);
                // $strReturn .= sprintf('<a href="%s" class="btn-sm btn-primary" title="Start Screening" onclick="return confirm(\'Start Screening For %s ?\')"><span class="fa fa-power-off"></span></a>',$this->objLinkProxy->RenderAsHref('run_'.$objRecord->UrlStaging,false),$objRecord->Filename);
                $_SESSION['prm_id'] = $objRecord->ParameterId;
            }

            return $strReturn;
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            //$mystring = substr($strParameter, strrpos($strParameter, "_") + 1);

            switch($section)
            {
                case'run':
                    $_SESSION[__STAGING__] = $param;
                    QApplication::Redirect(__SOURCE__.'/apu/suspect_screening/head_list.php');
                    break;
            }
        }
	}

	// Go ahead and run this form object to generate the page and event handlers, implicitly using
	// header_upload_suspect_list.tpl.php as the included HTML template file
	HeaderUploadSuspectListForm::Run('HeaderUploadSuspectListForm');
?>