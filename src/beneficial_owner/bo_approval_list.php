    <?php
        $dir = dirname(__FILE__);
        $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
        require($prePath.'/prepend.inc.php');

    	class BoSuspectListForm extends QForm {
            protected $dtgBoSuspect;
            protected $objLinkProxy;
            protected $lstAutoComplete;
            protected $lstAutoComplete2;
            protected $textSearch1;
            protected $textSearch2;
            protected $lstParam1;
            protected $lstParam2;
            protected $btnFilter;
            protected $datePicker1;
            protected $datePicker2;


        protected function Form_Run() { 
            parent::Form_Run();
        }

        protected function Form_Create() {
            parent::Form_Create();

            $this->dtgBoSuspect = new BoSuspectDataGrid($this);

            $this->dtgBoSuspect->Paginator = new QPaginator($this->dtgBoSuspect);
            // $this->dtgBoSuspect->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
            $this->dtgBoSuspect->ItemsPerPage = 20;
            $this->dtgBoSuspect->ShowFilter = false;
            $this->dtgBoSuspect->ShowSearch = false;
            $this->dtgBoSuspect->Width = '100%';

            $this->objLinkProxy = new QControlProxy($this);
            $this->objLinkProxy->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));


            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Data Source','<?= $_ITEM["data_code_source"] ?>','HtmlEntities=false','Width=20'));
            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Nama','<?= $_ITEM["bo_name_source"] ?>','HtmlEntities=false','Width=400'));
            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Last Update','<?= $_ITEM["last_update"] ?>','HtmlEntities=false','Width=30'));
            /*$this->dtgBoSuspect->AddColumn(new QDataGridColumn('Source','<?= $_ITEM->DataCodeSource ?>','HtmlEntities=false','Width=50'));
            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Last Update','<?= $_ITEM->LastUpdate ?>','HtmlEntities=false','Width=50'));*/
            $this->dtgBoSuspect->AddColumn(new QDataGridColumn('Approve','<?= $_FORM->rowActionButton($_ITEM["bo_suspect_id"]) ?>','HtmlEntities=false','Width=10'));
            

            $this->dtgBoSuspect->SetDataBinder('PageBinder', $this);

            $this->textSearch1 = new QTextBox($this);
            $this->textSearch1->hide();
            $this->textSearch1->ActionParameter = 'changeParam2';
            $this->textSearch1->AddAction(new QEnterKeyEvent(), new QAjaxAction('pageAction'));
            $this->textSearch1->hide();

            $this->textSearch2 = new QTextBox($this);
            $this->textSearch2->hide();
            $this->textSearch2->Width = '250';
            $this->textSearch2->hide();

            $this->datePicker1 = new QDateRangePicker($this);
            $this->datePicker1->Input = new QTextBox($this);
            $this->datePicker1->Input->Name = "Mulai";
            $this->datePicker1->CloseOnSelect= true;
            $this->datePicker1->JqDateFormat = 'yy-mm-dd';
            $this->datePicker1->Input->Hide();

            $this->datePicker2 = new QDateRangePicker($this);
            $this->datePicker2->Input = new QTextBox($this);
            $this->datePicker2->Input->Name = "Mulai";
            $this->datePicker2->CloseOnSelect= true;
            $this->datePicker2->JqDateFormat = 'yy-mm-dd';
            $this->datePicker2->Input->Hide();

            $this->lstParam1 = new QListBox($this);
            $this->lstParam1->Name = 'Param 1';
            $this->lstParam1->Width = '160';
            $this->lstParam1->AddItem('-Any-');
            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            if($objUser->Role->RoleName != 'BSH'){
                // $this->lstParam1->AddItem('Branch','bankBranch');
            }
            // $this->lstParam1->AddItem('CIF','cif');
            $this->lstParam1->AddItem('BO Name','bo_name_source');
            // $this->lstParam1->AddItem('Tanggal','tanggal');
            // $this->lstParam1->AddItem('No Rekening','accNumber');
            // $this->lstParam1->AddItem('Report Status','reportStatus');
            $this->lstParam1->ActionParameter = 'changeParam1';
            $this->lstParam1->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

            $this->lstParam2 = new QListBox($this);
            $this->lstParam2->Name = 'Param 2';
            $this->lstParam2->Width = '160';
            $this->lstParam2->AddItem('-Any-');
            if($objUser->Role->RoleName != 'BSH'){
                // $this->lstParam2->AddItem('Branch','bankBranch');
            }
            // $this->lstParam2->AddItem('CIF','cif');
            $this->lstParam2->AddItem('BO Name','bo_name_source');
            // $this->lstParam2->AddItem('Tanggal','tanggal');
            // $this->lstParam2->AddItem('No Rekening','accNumber');
            // $this->lstParam2->AddItem('Report Status','reportStatus');
            $this->lstParam2->ActionParameter = 'changeParam3';
            $this->lstParam2->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

            $this->btnFilter = new QButton($this);
            $this->btnFilter->Text = QApplication::Translate('Filter');
            $this->btnFilter->CssClass = 'btn btn-primary';
            $this->btnFilter->ActionParameter = 'changeParam2';
            $this->btnFilter->AddAction(new QClickEvent(), new QAjaxAction('pageAction'));
            $this->btnFilter->CausesValidation = true;

            $this->lstAutoComplete = new QSelect2ListBox($this);
            $this->lstAutoComplete->Width = '250';
            $this->lstAutoComplete->AddItem('- Select One -', null);

            $this->lstAutoComplete2 = new QSelect2ListBox($this);
            $this->lstAutoComplete2->Width = '250';
            $this->lstAutoComplete2->AddItem('- Select One -', null);
        }

        public function rowActionButton($record)
        {
            $strReturn = null;
            $strReturn .= sprintf('<a href="%s" class="btn-sm btn-warning" title="Detail BO"><span class="fa fa-plus-circle"></span></a>',
                __SOURCE__ . '/beneficial_owner/bo_approve.php?id='.$record);

            return $strReturn;
        }

        public function PageBinder(){

            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            
            $val1 = null; $val2 = null;

            $param1 = $this->lstParam1->SelectedValue;
            $param2 = $this->lstParam2->SelectedValue;

            $textfield = ['bo_name_source'];
            $date = [];
            $select = [];

            if (in_array($param1,$textfield)){
                $val1 = $this->textSearch1->Text;
            }elseif (in_array($param1,$date)){
                if ($this->datePicker1->Input->Text) {
                        $exval1 = explode(' - ',$this->datePicker1->Input->Text);
                        if (count($exval1) > 1) {
                            $val1[0] = $exval1[0]; 
                            $val1[1] = $exval1[1]; 
                        }else{
                            $val1[0] = $exval1[0]; 
                            $val1[1] = $exval1[0];
                        }
                    }else{
                        $val1 = NULL;
                    }
            }elseif (in_array($param1,$select)) {
                $val1 = $this->lstAutoComplete->SelectedValue; 
            }

            if (in_array($param2,$textfield)){
                $val2 = $this->textSearch2->Text; 
            }elseif (in_array($param2,$date)){
                if ($this->datePicker2->Input->Text) {
                        $exval2 = explode(' - ',$this->datePicker2->Input->Text);
                        if (count($exval2) > 1) {
                            $val2[0] = $exval2[0]; 
                            $val2[1] = $exval2[1]; 
                        }else{
                            $val2[0] = $exval2[0]; 
                            $val2[1] = $exval2[0];
                        } 
                    }else{
                        $val2 = NULL;
                    }
            }elseif (in_array($param2,$select)) {
                $val2 = $this->lstAutoComplete2->SelectedValue; 
            }

            $y1 = ''; $y2 = ''; $q1 = ''; $q2 = '';

            switch ($param1) {
                case 'bo_name_source':   $x1 = 'bo_name_source';      $y1 = 'free_text';      break;
                case 'branch_code':      $x1 = 'branch_code';         $y1 = 'free_text';      break;
                case 'first_name':       $x1 = 'UPPER(first_name)';   $y1 = 'free_text';      break;
                case 'customer_id':      $x1 = 'customer_id';         $y1 = 'fixed_text';     break;
                case 'trx_value_date':   $x1 = 'trx_value_date';      $y1 = 'date_range';     break;
                case 'customer_type_id': $x1 = 'customer_type_id';    $y1 = 'fixed_value';    break;
                case 'bank_type':        $x1 = 'bank_type';           $y1 = 'fixed_value';    break;
                case 'report_status':    $x1 = 'report_status';       $y1 = 'fixed_value';    break;
            }

            switch ($param2) {
                case 'bo_name_source':   $x2 = 'bo_name_source';      $y2 = 'free_text';      break;
                case 'branch_code':      $x2 = 'branch_code';         $y2 = 'free_text';      break;
                case 'first_name':       $x2 = 'UPPER(first_name)';   $y2 = 'free_text';      break;
                case 'customer_id':      $x2 = 'customer_id';         $y2 = 'fixed_text';     break;
                case 'trx_value_date':   $x2 = 'trx_value_date';      $y2 = 'date_range';     break;
                case 'customer_type_id': $x2 = 'customer_type_id';    $y2 = 'fixed_value';    break;
                case 'bank_type':        $x2 = 'bank_type';           $y2 = 'fixed_value';    break;
                case 'report_status':    $x2 = 'report_status';       $y2 = 'fixed_value';    break;
            }

            switch ($y1) {
                case 'free_text'    : $q1 = "and ".$x1." like '%".strtoupper(trim($val1))."%'";        break;
                case 'date_range'   : $q1 = "and ".$x1." between '".$val1[0]."' and '".$val1[1]."'";   break;
                case 'fixed_text'   : $q1 = "and ".$x1." = '".$val1."'";                               break;
                case 'fixed_value'  : $q1 = "and ".$x1." = '".$val1."'";                               break;
            }

            switch ($y2) {
                case 'free_text'    : $q2 = "and ".$x2." like '%".strtoupper(trim($val2))."%'";        break;
                case 'date_range'   : $q2 = "and ".$x2." between '".$val2[0]."' and '".$val2[1]."'";   break;
                case 'fixed_text'   : $q2 = "and ".$x2." = '".$val2."'";                               break;
                case 'fixed_value'  : $q2 = "and ".$x2." = '".$val2."'";                               break;
            }

            $condition = "";

            if      ($val1 != NULL && $val2 != NULL) { $condition .= " ".$q1." ".$q2;    }
            elseif  ($val1 != NULL && $val2 == NULL) { $condition .= " ".$q1;            }
            elseif  ($val1 == NULL && $val2 != NULL) { $condition .= " ".$q2;            }
            else                                     { $condition .= "";                 }

            $intOffset = ($this->dtgBoSuspect->PageNumber - 1) * $this->dtgBoSuspect->ItemsPerPage;   

            $sql = "SELECT bo_suspect_id,bo_name_source, parent_code, data_code_source, last_update 
                    FROM bo_suspect 
                    WHERE bo_suspect_id IS NOT NULL ".$condition. " AND approval_status != TRUE";

            $sqlCount = "SELECT COUNT(1) FROM (".$sql.") AS X ";

            $sql .=" LIMIT 10 OFFSET ".$intOffset;

            $objDatabase = QApplication::$Database[1];
            $objDbResult = $objDatabase->Query($sql);

            $dataCount = $objDatabase->Query($sqlCount);

            $count = 0;
            while ($cRow = $dataCount->FetchArray()) {
                $count = $cRow[0];
            };

            $array = array();
            while ($mixRow = $objDbResult->FetchArray()) {
                array_push($array, $mixRow);
            }

            $this->dtgBoSuspect->DataSource = $array;
            $this->dtgBoSuspect->TotalItemCount = $count;
            $this->dtgBoSuspect->UseAjax = TRUE;
        }

        /*public function PageBinder(){

            $objCondition = array();
            $objOptionalClauses = array();

            $param1 = $this->lstParam1->SelectedValue; 
            $param2 = $this->lstParam2->SelectedValue; 

            if ($param1 == 'custName' or $param1 == 'cif' or $param1 == 'accNumber'){
                $val1 = $this->textSearch1->Text; 
            }elseif ($param1 == 'tanggal'){
                $exval1 = explode(' - ',$this->datePicker1->Input->Text);
                if (count($exval1) > 1) {
                    $val1[0] = $exval1[0]; 
                    $val1[1] = $exval1[1]; 
                }else{
                    $val1[0] = $exval1[0]; 
                    $val1[1] = $exval1[0];
                }
            }else{
                $val1 = $this->lstAutoComplete->SelectedValue; 
            }

            if ($param2 == 'custName' or $param2 == 'cif' or $param2 == 'accNumber'){
                $val2 = $this->textSearch2->Text; 
            }elseif ($param2 == 'tanggal'){
                $exval2 = explode(' - ',$this->datePicker2->Input->Text);
                if (count($exval2) > 1) {
                    $val2[0] = $exval2[0]; 
                    $val2[1] = $exval2[1]; 
                }else{
                    $val2[0] = $exval2[0]; 
                    $val2[1] = $exval2[0];
                }
            }else{
                $val2 = $this->lstAutoComplete2->SelectedValue; 
            }

            $y1 = ''; $y2 = ''; $q1 = ''; $q2 = '';

            switch ($param1) {
                case 'bankBranch':      $x1 = QQN::DimWic()->Kodecabang;                        $y1 = 'fixed_value';    break;
                case 'custName':        $x1 = QQN::DimWic()->NamaNasabah;                       $y1 = 'free_text';      break;
                case 'tanggal':         $x1 = QQN::DimWic()->TglData;                           $y1 = 'date_range';     break;
            }

            switch ($param2) {
                case 'bankBranch':      $x2 = QQN::DimWic()->Kodecabang;                        $y2 = 'fixed_value';    break;
                case 'custName':        $x2 = QQN::DimWic()->NamaNasabah;                       $y2 = 'free_text';      break;
                case 'tanggal':         $x2 = QQN::DimWic()->TglData;                           $y2 = 'date_range';     break;
            }

            switch ($y1) {
                case 'free_text'    : $q1 = QQ::Like($x1, strtoupper('%'.$val1.'%'));                                           break;
                case 'date_range'   : $q1 = QQ::AndCondition(QQ::GreaterOrEqual($x1,$val1[0]),QQ::LessOrEqual($x1,$val1[1]));   break;
                case 'fixed_text'   : $q1 = QQ::Equal($x1, $val1);                                                              break;
                case 'with_null'    : $q1 = ($val1 == 'BLANK') ? QQ::isNull($x1) : QQ::Equal($x1, $val1);                       break;
                case 'fixed_value'  : $q1 = QQ::Equal($x1, $val1);                                                              break;
            }

            switch ($y2) {
                case 'free_text'    : $q2 = QQ::Like($x2, strtoupper('%'.$val2.'%'));                                           break;
                case 'date_range'   : $q2 = QQ::AndCondition(QQ::GreaterOrEqual($x2,$val2[0]),QQ::LessOrEqual($x2,$val2[1]));   break;
                case 'fixed_text'   : $q2 = QQ::Equal($x2, $val2);                                                              break;
                case 'with_null'    : $q2 = ($val2 == 'BLANK') ? QQ::isNull($x2) : QQ::Equal($x2, $val2);                       break;
                case 'fixed_value'  : $q2 = QQ::Equal($x2, $val2);                                                              break;
            }

            $condition = "";

            if     ($val1 != NULL && $val2 != NULL) { $condition = array_push($objCondition, $q1, $q2);     }
            elseif ($val1 != NULL && $val2 == NULL) { $condition = array_push($objCondition, $q1);          }
            elseif ($val1 == NULL && $val2 != NULL) { $condition = array_push($objCondition, $q2);          }
            else                                    { $condition = array_push($objCondition, QQ::All());    }
            
            $objUser = Users::Load($_SESSION[__USER_LOGIN__]);
            if ($objUser->Role->RoleLevel < 3) {    
                    array_push($objCondition, QQ::Equal(QQN::DimWic()->Kodecabang, $this->getBranchID($_SESSION[__USER_BRANCH__])));
            }

            $this->dtgBoSuspect->MetaDataBinderCustom(new BoSuspect(), QQ::AndCondition($objCondition), array(QQ::GroupBy(array(QQN::BoSuspect()->ParentCode)), QQ::OrderBy(QQN::BoSuspect()->BoNameSource, false)));
        }*/

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            switch($section)
            {
                case'del':
                    $obj = DimWic::Load($param);
                    $obj->Delete();
                    QApplication::DisplayAlert($obj->NamaNasabah. ' has been Removed');
                    $this->dtgBoSuspect->Refresh();
                    break;

                case 'change':
                    $this->dtgBoSuspect->Refresh();
                    break;
                case 'changeParam1':
                    if($this->lstParam1->SelectedValue == 'tanggal'){
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->lstAutoComplete->hide();
                        $this->textSearch1->hide();
                        $this->datePicker1->Input->unhide();
                    }elseif($this->lstParam1->SelectedValue == 'cif'){
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->lstAutoComplete->hide();
                        $this->textSearch1->unhide();
                        $this->textSearch1->Text = '';
                        $this->datePicker1->Input->hide();    
                    }elseif($this->lstParam1->SelectedValue == 'custName' or $this->lstParam1->SelectedValue == 'accNumber'){
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->lstAutoComplete->hide();
                        $this->textSearch1->unhide();
                        $this->textSearch1->Text = '';  
                        $this->datePicker1->Input->hide();  
                    }elseif($this->lstParam1->SelectedValue == 'reportStatus'){
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->lstAutoComplete->AddItem('- Any -');
                        $this->lstAutoComplete->AddItem('Belum Dikerjakan','BLANK');
                        $this->lstAutoComplete->AddItem('Dikecualikan','1');
                        $this->lstAutoComplete->AddItem('Dilaporkan','2');
                        $this->textSearch1->hide();
                        $this->lstAutoComplete->unhide();
                        $this->textSearch1->Text = '';
                        $this->datePicker1->Input->hide();
                    }elseif($this->lstParam1->SelectedValue == 'bankBranch'){
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->lstAutoComplete->AddItem('-Any-');

                        if($arrBranch = BankBranch::LoadAll()){
                            foreach($arrBranch as $bankBranch){
                                $this->lstAutoComplete->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                            }
                        }
                        $this->lstAutoComplete->unhide();
                        $this->textSearch1->hide();
                        $this->datePicker1->Input->hide();
                    }else{
                        $this->lstAutoComplete->RemoveAllItems();
                        $this->textSearch1->Text = '';
                        $this->datePicker1->Input->Text = '';
                        $this->lstAutoComplete->AddItem('-Any-');
                        $this->dtgBoSuspect->Refresh();
                    }
                break;
                case 'changeParam2':
                        $this->dtgBoSuspect->Refresh();
                    break;
                case 'changeParam3':
                    if($this->lstParam2->SelectedValue == 'tanggal'){
                        $this->lstAutoComplete2->RemoveAllItems();
                        $this->lstAutoComplete2->hide();
                        $this->textSearch2->hide();
                        $this->datePicker2->Input->unhide();
                    }elseif($this->lstParam2->SelectedValue == 'bo_name_source'){
                        $this->lstAutoComplete2->RemoveAllItems();
                        $this->lstAutoComplete2->hide();
                        $this->textSearch2->unhide();
                        $this->textSearch2->Text = '';
                        $this->datePicker2->Input->hide();
                    }elseif($this->lstParam2->SelectedValue == 'reportStatus'){
                        $this->lstAutoComplete2->RemoveAllItems();
                        $this->lstAutoComplete2->AddItem('- Any -');
                        $this->lstAutoComplete2->AddItem('Belum Dikerjakan','BLANK');
                        $this->lstAutoComplete2->AddItem('Dikecualikan','1');
                        $this->lstAutoComplete2->AddItem('Dilaporkan','2');
                        $this->textSearch2->hide();
                        $this->lstAutoComplete2->unhide();
                        $this->textSearch2->Text = '';
                        $this->datePicker2->Input->hide();
                    }elseif($this->lstParam2->SelectedValue == 'bankBranch'){
                        $this->lstAutoComplete2->RemoveAllItems();
                        $this->lstAutoComplete2->AddItem('-Any-');

                        if($arrBranch = BankBranch::LoadAll()){
                            foreach($arrBranch as $bankBranch){
                                $this->lstAutoComplete2->AddItem($bankBranch->BranchName, $bankBranch->BranchCode);
                            }
                        }
                        $this->lstAutoComplete2->unhide();
                        $this->textSearch2->hide();
                        $this->datePicker2->Input->hide();
                    }else{
                        $this->lstAutoComplete2->RemoveAllItems();
                        $this->lstAutoComplete2->AddItem('-Any-');
                        $this->datePicker2->Input->Text = '';
                        $this->dtgBoSuspect->Refresh();
                        $this->textSearch2->Text = '';
                    }
                break;
            }
        }
    }
    BoSuspectListForm::Run('BoSuspectListForm');
    ?>