<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

<section class="content-header">
</section>
<br>
<div class="box box-primary">
        <div class="box-body">
            <h4>Beneficial Owner</h4>
            <?php $this->dtgBoSuspect->Render(); ?>
        </div>
        <div class="box-body">
            <h4>Offshore Company</h4>
            <?php $this->dtgOffshoreSuspect->Render(); ?>
        </div>
         <div class="box-body">
            <h4>Panama's Data</h4>
            <?php $this->dtgPanamaSuspect->Render(); ?>
        </div>
        <div class="box-body">
            <h4>Paradise's Data</h4>
            <?php $this->dtgParadiseSuspect->Render(); ?>
        </div>
        <div class="box-body">
            <h4>Bermuda's Data</h4>
            <?php $this->dtgBermudaSuspect->Render(); ?>
        </div>
    </div>



<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>
