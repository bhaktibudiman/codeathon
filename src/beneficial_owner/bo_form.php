<?php
$dir = dirname(__FILE__);
$search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
require($prePath.'/prepend.inc.php');

class CashTransactionEditForm extends QForm {

    //Controls For MetaControl
    protected $mctCashTransaction;
    protected $objBoSuspect;
    protected $txtBoNameSource;
    protected $txtBoNameTarget;
    protected $txtDataCodeSource;
    protected $txtDataCodeTarget;

    protected $btnViewCustomerProfile;
    protected $btnReject;
    protected $btnApprove;
    protected $btnCancel;
    protected $btnSave;
    protected $btnSaveNote;

    protected function Form_Run() {
        parent::Form_Run();
    }

    protected function Form_Create() {
        parent::Form_Create();

        $this->objBoSuspect = BoSuspect::Load(QApplication::PathInfo(0));

        $this->txtBoNameSource = new QTextBox($this);
        $this->txtBoNameSource->CssClass = 'form-control';
        $this->txtBoNameSource->Name = 'BO Name Source';
        $this->txtBoNameSource->Text = $this->objBoSuspect->BoNameSource;

        $this->txtDataCodeSource = new QTextBox($this);
        $this->txtDataCodeSource->CssClass = 'form-control';
        $this->txtDataCodeSource->Name = 'Source Data Code';
        $this->txtDataCodeSource->Text = $this->objBoSuspect->DataCodeSource;

        $this->txtDataCodeTarget = new QTextBox($this);
        $this->txtDataCodeTarget->CssClass = 'form-control';
        $this->txtDataCodeTarget->Name = 'BO Name Source';
        $this->txtDataCodeTarget->Text = $this->objBoSuspect->DataCodeTarget;

        $this->txtBoNameTarget = new QTextBox($this);
        $this->txtBoNameTarget->CssClass = 'form-control';
        $this->txtBoNameTarget->Name = 'Target Data Code';
        $this->txtBoNameTarget->Text = $this->objBoSuspect->BoNameTarget;

        $this->btnCancel = new QButton($this);
        $this->btnCancel->Text = QApplication::Translate('Back');
        $this->btnCancel->AddAction(new QClickEvent(), new QAjaxAction('btnCancel_Click'));
        $this->btnCancel->CssClass = 'btn btn-warning';

        $this->btnApprove = new QButton($this);
        $this->btnApprove->Text = QApplication::Translate('Approve');
        $this->btnApprove->AddAction(new QClickEvent(), new QAjaxAction('btnApprove_Click'));
        $this->btnApprove->CssClass = 'btn btn-success';
        $this->btnApprove->CausesValidation = True;
    }

    protected function btnApprove_Click($strFormId, $strControlId, $strParameter) {

        try {
            $this->objBoSuspect = new NoteCashTransaction();
            $this->objBoSuspect->CashTransactionId = QApplication::PathInfo(0);
            $this->objBoSuspect->CreatedBy = $_SESSION[__USER_LOGIN__];
            $this->objBoSuspect->CreatedDate = QDateTime::Now();
            $this->objBoSuspect->ReportStatus = $this->rdDikecualikan->SelectedValue;

            $this->objBoSuspect->Save();

            QApplication::DisplayAlert("Process Success");
        } catch (Exception $e) {
            
        }
    }

    protected function btnReject_Click($strFormId, $strControlId, $strParameter) {
        
    }

    protected function Form_Validate() {
        $blnToReturn = true;

        if($this->rdDikecualikan->SelectedValue == ''){
            QApplication::DisplayAlert("Harap Pilih Salah Satu");
            $blnToReturn = true;
        }else if($this->rdDikecualikan->SelectedValue == ''){
            QApplication::DisplayAlert("Harap Pilih Salah Satu");
            $blnToReturn = true;
        }

        $blnFocused = false;
        foreach ($this->GetErrorControls() as $objControl) {
            // Set Focus to the top-most invalid control
            if (!$blnFocused) {
                $objControl->Focus();
                $blnFocused = true;
            }

            // Blink on ALL invalid controls
            $objControl->Blink();
        }

        return $blnToReturn;
    }

    protected function pageAction($strFormId, $strControlId, $strParameter) {
        $param = ApuParameter::LoadByParamTypeAndCode('APPLICATION_PARAM', 'WORKFLOW REJECT');
        $reject = $param->ParamValue;

        if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
        else $section = $strParameter;

        switch($section)
        {
            case 'saveAction' :
                $this->objNoteCTR = new NoteCashTransaction();

                $this->objNoteCTR->CashTransactionId = QApplication::PathInfo(0);
                $this->objNoteCTR->Note = $this->txtNoteCash->Text;

                $this->objNoteCTR->CreatedBy = $_SESSION[__USER_LOGIN__];
                $this->objNoteCTR->CreatedDate = QDateTime::Now();

                $this->objNoteCTR->Save();
                $this->dtgNoteCTR->Refresh();

                QApplication::DisplayAlert("Message Already Saved");


                $objCustomerCash = CashTransaction::Load(QApplication::PathInfo(0));
                $objCustomerCash->TransactionStatus = 'R';
                $objCustomerCash->WorkedStatus = $reject;
                $objCustomerCash->WorkedUser = $_SESSION[__USER_LOGIN__];
                $objCustomerCash->Save();

                $this->RedirectToListPage($_SESSION['chosenAct'],$_SESSION['chosenCif'],$_SESSION['chosenDate']);

                break;

            case 'rdPengecualian':
                if($this->rdDikecualikan->SelectedValue == 1){
                    $this->txtInformasiTransaksi->Required = True;
                    $this->txtCancelReason->Required = True;
                    $this->txtCancelReason->UnHide();
                    $this->txtInformasiTransaksi->UnHide();
                    $this->btnSave->UnHide();
                    $this->btnReject->Hide();
                }elseif($this->rdDikecualikan->SelectedValue == 3){
                    $this->txtInformasiTransaksi->Required = True;
                    $this->txtCancelReason->Required = True;
                    $this->txtCancelReason->UnHide();
                    $this->txtInformasiTransaksi->UnHide();
                    $this->btnSave->Hide();
                    $this->btnReject->UnHide();
                }else{
                    $this->txtCancelReason->Hide();
                    $this->txtInformasiTransaksi->Hide();
                    $this->txtInformasiTransaksi->Required = false;
                    $this->txtCancelReason->Required = false;
                    $this->btnSave->UnHide();
                    $this->btnReject->Hide();
                }
                break;
        }
    }

    protected function pageBinderNoteCashTransaction(){
        $this->dtgNoteCTR->DataSource =
            NoteCashTransaction::LoadArrayByCashTransactionId(QApplication::PathInfo(0));
    }

    protected function btnCancel_Click($strFormId, $strControlId, $strParameter) {
        QApplication::Redirect(__SOURCE__ . '/beneficial_owner/bo_list.php');
    }

    // Other Methods

    protected function RedirectToListPage($x = '',$y = '',$z = '') {
        QApplication::Redirect(__SOURCE__ . '/transaksi/cash/'.$x.'/'.$y.'/'.$z);
    }
}
CashTransactionEditForm::Run('CashTransactionEditForm');
?>
