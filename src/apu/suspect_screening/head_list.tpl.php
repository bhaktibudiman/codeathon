<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>

<?php $this->RenderBegin() ?>

    <section class="content-header">
        <h1>List Suspect Screening</h1>
    </section>

    <div class="box box-primary">
        <div class="box-body">
            <div class="dataTables_wrapper form-inline">
                <?php $this->dtgSuspectScreening->Render(); ?>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ . '/footer.inc.php'); ?>