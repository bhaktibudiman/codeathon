<?php
require(__CONFIGURATION__ . '/header.inc.php');
?>
<?php $this->RenderBegin() ?>
    <div class="row">
        <div class="col-md-8">
            <div class="box">
                <div class="box-header with-border">
                    <h1>Edit URL</h1>
                </div>
                <div class="box-body">
                    <div class="form-controls">
                        <?php $this->txtFilename->RenderWithBootstrap(2,6); ?>
                        <?php $this->chkUploadStatus->RenderWithBootstrap(2,6); ?>
                        <?php $this->chkScreeningStatus->RenderWithBootstrap(2,6); ?>
                        <?php $this->txtUrlStaging->RenderWithBootstrap(2,6); ?>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="form-actions">
                        <?php $this->btnSave->Render(); ?>
                        <?php $this->btnCancel->Render(); ?>
                        <?php $this->btnDelete->Render(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->RenderEnd() ?>

<?php require(__CONFIGURATION__ .'/footer.inc.php'); ?>