<?php
    $dir = dirname(__FILE__);
    $search = '/includes/configuration';$counter = 0;while($counter < 10){if($found=is_dir($dir.$search)){$prePath=realpath($dir.$search);break;}$counter++;$search = '/..'.$search;}
    require($prePath.'/prepend.inc.php');

	class DetailUploadSuspectListForm extends QForm {

        protected $dtgDetailSuspectScreening;
        protected $textSearch1;
        protected $textSearch2;
        protected $textSearch3;
        protected $lstParam1;
        protected $objSuspType;

		protected function Form_Run() {
			parent::Form_Run();
			QApplication::CheckRemoteAdmin();		    
		}

		protected function Form_Create() {

            $this->lstParam1 = new QSelect2ListBox($this);
            $this->lstParam1->Width = '250';
            $objSuspType = ApuParameter::LoadByParamTypeArray('SUSPECT_TYPE');
            $this->lstParam1->AddItem('- Select One -', null);
            foreach($objSuspType as $SuspType){
                $this->lstParam1->AddItem($SuspType->ParamDesc, $SuspType->ParameterId);
            }
            $this->lstParam1->ActionParameter = 'refresh';
            $this->lstParam1->AddAction(new QChangeEvent(), new QAjaxAction('pageAction'));

            $this->textSearch1 = new QTextBox($this);
            $this->textSearch1->ActionParameter = 'refresh';
            $this->textSearch1->AddAction(new QEnterKeyEvent(), new QAjaxAction('pageAction'));

            $this->textSearch2 = new QTextBox($this);
            $this->textSearch2->ActionParameter = 'refresh';
            $this->textSearch2->AddAction(new QEnterKeyEvent(), new QAjaxAction('pageAction'));

            $this->textSearch3 = new QTextBox($this);
            $this->textSearch3->ActionParameter = 'refresh';
            $this->textSearch3->AddAction(new QEnterKeyEvent(), new QAjaxAction('pageAction'));

            $this->dtgDetailSuspectScreening = new DetailUploadSuspectDataGrid($this);
            $this->dtgDetailSuspectScreening->Width = 1400;

            $this->dtgDetailSuspectScreening->Paginator = new QPaginator($this->dtgDetailSuspectScreening);
            $this->dtgDetailSuspectScreening->ItemsPerPage = __FORM_DRAFTS_FORM_LIST_ITEMS_PER_PAGE__;
            $this->dtgDetailSuspectScreening->ShowSearch = false;

            $this->dtgDetailSuspectScreening->ShowFilter = false;
            $this->dtgDetailSuspectScreening->ShowFilterButton = false;
            $this->dtgDetailSuspectScreening->ShowFilterResetButton = false;

            $this->dtgDetailSuspectScreening->AddColumn(new QDataGridColumn('No.','<?= $_CONTROL->CurrentNumber ?>.', 'Width=10'));
            $this->dtgDetailSuspectScreening->AddColumn(new QDataGridColumn('Type','<?= $_FORM->getParameter($_ITEM->ParameterId) ?>','HtmlEntities=false', 'Width=150'));
            $this->dtgDetailSuspectScreening->AddColumn(new QDataGridColumn('Suspect Name', '<?= $_FORM->getSuspName($_ITEM->IdDb) ?>', 'Width=300'));
            $this->dtgDetailSuspectScreening->AddColumn(new QDataGridColumn('Description', '<?= $_FORM->getSuspDesc($_ITEM->IdDb) ?>'), 'Width=300');
            $this->dtgDetailSuspectScreening->AddColumn(new QDataGridColumn('Suspect Number', '<?= $_FORM->getSuspNumber($_ITEM->IdDb) ?>','HtmlEntities=false', 'Width=100'));

            $this->dtgDetailSuspectScreening->SetDataBinder('pageBinder');
        }

        protected function pageAction($strFormId, $strControlId, $strParameter) {
            if(stristr($strParameter,'_')) list($section,$param) = @explode('_',$strParameter);
            else $section = $strParameter;

            switch($section)
            {
                case 'refresh':
                        $this->dtgDetailSuspectScreening->Refresh();
                    break;
            }
        }

        protected function pageBinder(){
            $objCondition = array();

            array_push($objCondition, QQ::All());

            $param_type     = $this->lstParam1->SelectedValue;
            $param_number   = $this->textSearch1->Text;
            $param_name     = $this->textSearch2->Text;
            $param_desc     = $this->textSearch3->Text;

            if ($param_type) {
                 array_push($objCondition, QQ::Equal(QQN::DetailUploadSuspect()->ParameterId,$param_type));
            }

            if ($param_name) {
                array_push($objCondition, 
                    QQ::OrCondition(
                        QQ::Like(QQN::DetailUploadSuspect()->Nama,'%'.strtoupper($param_name).'%'),
                        QQ::Like(QQN::DetailUploadSuspect()->FirstName,'%'.strtoupper($param_name).'%'),
                        QQ::Like(QQN::DetailUploadSuspect()->SecondName,'%'.strtoupper($param_name).'%')
                    )
                );
            }

            if ($param_desc) {
                 array_push($objCondition, QQ::Like(QQN::DetailUploadSuspect()->News1,'%'.strtoupper($param_desc).'%'));
            }

            if ($param_number) {
                 array_push($objCondition, QQ::Equal(QQN::DetailUploadSuspect()->Nomor, trim($param_number)));
            }

            if (QApplication::PathInfo(0)) {
                array_push($objCondition, QQ::Equal(QQN::DetailUploadSuspect()->UploadId,QApplication::PathInfo(0)));
            }

            /*if($search = $this->dtgDetailSuspectScreening->SearchText){
                array_push($objCondition, QQ::OrCondition(
                    QQ::Like(QQN::DetailUploadSuspect()->Nama, strtoupper($search)),
                    QQ::Like(QQN::DetailUploadSuspect()->FirstName, strtoupper($search)),
                    QQ::Like(QQN::DetailUploadSuspect()->SecondName, strtoupper($search))
                ));
            }*/

            $this->dtgDetailSuspectScreening->MetaDataBinderCustom(new DetailUploadSuspect(), QQ::AndCondition($objCondition));
        }

        public function getParameter($id) {
            $objParam = ApuParameter::QuerySingle(
                QQ::Equal(QQN::ApuParameter()->ParameterId, $id)
            );
            $desc = $objParam->ParamDesc;
            return $desc;
        }

        public function getSuspNumber($id) {
            $objSusp = DetailUploadSuspect::QuerySingle(
                QQ::Equal(QQN::DetailUploadSuspect()->IdDb, $id)
            );
            $returnString = ($objSusp->Nomor)?$objSusp->Nomor:$objSusp->IdTeroris;
            return $returnString;
        }

        public function getSuspName($id) {
            $objSusp = DetailUploadSuspect::QuerySingle(
                QQ::Equal(QQN::DetailUploadSuspect()->IdDb, $id)
            );
            $returnString = ($objSusp->Nama) ? $objSusp->Nama : $objSusp->SecondName;
            return $returnString;
        }

        public function getSuspDesc($id) {
            $objSusp = DetailUploadSuspect::QuerySingle(
                QQ::Equal(QQN::DetailUploadSuspect()->IdDb, $id)
            );
            $returnString = ($objSusp->News1)?$objSusp->News1:$objSusp->Narrative1;
            return $returnString;
        }

	}

	DetailUploadSuspectListForm::Run('DetailUploadSuspectListForm');
?>